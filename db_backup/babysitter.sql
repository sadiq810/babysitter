/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.6.21 : Database - babysetter
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `babysitter` */

DROP TABLE IF EXISTS `babysitter`;

CREATE TABLE `babysitter` (
  `bs_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `about` text,
  `address` text,
  `hourly_rate` varchar(50) DEFAULT NULL,
  `card_number` varchar(50) DEFAULT NULL,
  `initial_balance` varchar(255) DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `pin_code` varchar(20) DEFAULT NULL,
  `img` varchar(500) DEFAULT NULL,
  `approve_status` int(1) DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` date DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `accessToken` varchar(40) DEFAULT NULL,
  `refreshToken` varchar(40) DEFAULT NULL,
  `expiresIn` datetime DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `likes` varchar(20) DEFAULT NULL,
  `dislikes` varchar(20) DEFAULT NULL,
  `from_time` time DEFAULT NULL,
  `to_time` time DEFAULT NULL,
  `working_days` varchar(255) DEFAULT NULL,
  `utype` varchar(5) DEFAULT NULL COMMENT 'by default all users are parent, if utype set means it is also become a babysitter too',
  PRIMARY KEY (`bs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `babysitter` */

insert  into `babysitter`(`bs_id`,`fname`,`lname`,`email`,`username`,`password`,`phone`,`about`,`address`,`hourly_rate`,`card_number`,`initial_balance`,`expiration_date`,`pin_code`,`img`,`approve_status`,`created_at`,`created_by`,`modified_at`,`modified_by`,`status`,`accessToken`,`refreshToken`,`expiresIn`,`lat`,`long`,`likes`,`dislikes`,`from_time`,`to_time`,`working_days`,`utype`) values (1,'Khan','Saleem','saleem@yahoo.com','admin','21232f297a57a5a743894a0e4a801fc3','(123) 456-7890','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.\nEtiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc. ','795 Folsom Ave, Suite 600San Francisco, CA 94107','5','989898989',NULL,'2016-09-23','1234','uploads/145933450734055749bf7bf988f8eab8149d9bb007.png',1,'2016-03-28',1,'2016-04-05',1,1,'223ff629cb8bbb44ce4d19464cd20c3d32da32ca','fae94217ad5da356195fd5cbf615e8662802eb4e','2016-05-05 12:13:52','37.7821587','-122.4004835','2','12','14:04:00','14:04:00','Monday','b'),(2,'John','Doe','john@gmail.com','admin1','e00cf25ad42683b3df678c61f42c6bda','098987878','Here is details about me','here is address','20','9878676767-98',NULL,'2017-06-20','2345',NULL,1,'2016-04-05',NULL,'2016-04-05',2,1,'9cdb765d64907d22a6a790dac7b7ef2d287b970a','38f028f21207694cb4dcf92d4b62735846c3ba2d','2016-05-05 12:14:26','98.7676','-97.65634','3',NULL,'12:30:00','14:30:00','Monday-Tuesday-Wednesday','b');

/*Table structure for table `babysitter_activities` */

DROP TABLE IF EXISTS `babysitter_activities`;

CREATE TABLE `babysitter_activities` (
  `bsa_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `bs_id` int(11) DEFAULT NULL,
  `feedback` text,
  `desc` text,
  `clock_in` datetime DEFAULT NULL,
  `clock_out` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_completed` int(1) DEFAULT '0' COMMENT 'shows that task is completed, means clockin and clockout are set.',
  `is_paid` int(1) DEFAULT '0' COMMENT 'show that payment are done for this duration or not. 0 means not',
  `status` int(1) DEFAULT '1' COMMENT 'active or deactive',
  PRIMARY KEY (`bsa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `babysitter_activities` */

insert  into `babysitter_activities`(`bsa_id`,`job_id`,`bs_id`,`feedback`,`desc`,`clock_in`,`clock_out`,`created_at`,`created_by`,`is_completed`,`is_paid`,`status`) values (1,18,1,'Here is feedback','Here is optional description','2016-03-31 11:30:45','2016-03-31 11:43:05','2016-03-31 00:00:00',1,1,0,1),(2,19,2,'Optional Feedback',NULL,'2016-04-05 09:56:22','2016-04-05 09:58:50','2016-04-05 09:56:22',2,1,0,1);

/*Table structure for table `job_applications` */

DROP TABLE IF EXISTS `job_applications`;

CREATE TABLE `job_applications` (
  `ja_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) DEFAULT NULL,
  `bs_id` int(11) DEFAULT NULL,
  `details` text,
  `rate` varchar(50) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`ja_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `job_applications` */

insert  into `job_applications`(`ja_id`,`job_id`,`bs_id`,`details`,`rate`,`created_at`,`created_by`,`status`) values (1,19,1,'I am ready to handle this job',NULL,'2016-03-31',1,1),(2,19,2,'optional Detail goes here','17','2016-04-05',2,1);

/*Table structure for table `jobs` */

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `duration` varchar(100) DEFAULT NULL,
  `no_childern` varchar(20) DEFAULT NULL,
  `ages_childern` varchar(255) DEFAULT NULL,
  `child_qualities` varchar(500) DEFAULT NULL,
  `responsibilities` text,
  `location` text,
  `rate` varchar(500) DEFAULT NULL,
  `job_title` varchar(500) DEFAULT NULL,
  `job_description` text,
  `babysitter_id` int(11) DEFAULT NULL,
  `assignStatus` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `reviews` text COMMENT 'it stores parent''s reviews for assigned babysitter...',
  `punctuality` varchar(5) DEFAULT NULL,
  `attitude` varchar(5) DEFAULT NULL,
  `is_finished` int(1) DEFAULT '0' COMMENT 'shows if job finished or still in progress',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `jobs` */

insert  into `jobs`(`job_id`,`parent_id`,`start_date`,`start_time`,`duration`,`no_childern`,`ages_childern`,`child_qualities`,`responsibilities`,`location`,`rate`,`job_title`,`job_description`,`babysitter_id`,`assignStatus`,`created_at`,`created_by`,`status`,`reviews`,`punctuality`,`attitude`,`is_finished`) values (18,1,'2016-03-03','09:30:00','5hrs','1','15','school age experience','Here is the list of responsibilities','Peshawar Deans Trade Center','50','Here is the title of the job','Here is the long description of the job',1,1,'2016-03-31 00:00:00',1,1,'Here is review',NULL,NULL,1),(19,1,'2016-03-03','09:30:00','5hrs','1','15','school age experience','Here is the list of responsibilities','Peshawar Deans Trade Center','50','Here is the title of the job 2','Here is the long description of the job',2,1,'2016-03-31 00:00:00',1,1,'Here are reviews','7','7',0),(21,2,'2016-03-03','09:30:00','5hrs','1','15','school age experience','Here is the list of responsibilities','Peshawar Deans Trade Center','50','Here is the title of the job','Here is the long description of the job',2,1,'2016-04-05 00:00:00',NULL,1,NULL,'6','3',0);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `remarks` text,
  `nsource` varchar(15) DEFAULT NULL COMMENT 'for representing notification source.who create notification, p(parent),b(babysitter),admin',
  `nfor` varchar(50) DEFAULT NULL COMMENT 'for whome, means for admin, or babysitter b, for parent p',
  `from` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  `sentStatus` int(1) DEFAULT '0' COMMENT '0 shows that notification is not sent, 1 means sent',
  `is_read` int(1) DEFAULT '0' COMMENT '0 means not read, 1 means read',
  `created_at` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 means active, 0 means deactive..',
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `notifications` */

insert  into `notifications`(`notification_id`,`title`,`description`,`remarks`,`nsource`,`nfor`,`from`,`to`,`sentStatus`,`is_read`,`created_at`,`status`) values (1,'New User Registered!','New user is registered, Please check his/her details information and Activate Account!',NULL,'p','admin',2,NULL,0,0,'2016-04-05 08:39:55',1);

/*Table structure for table `oauth_access_tokens` */

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL COMMENT '获取资源的access_token',
  `client_id` varchar(80) NOT NULL COMMENT '开发者Appid',
  `user_id` varchar(255) DEFAULT NULL COMMENT '开发者用户id',
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '认证的时间date("Y-m-d H:i:s")',
  `scope` varchar(2000) DEFAULT NULL COMMENT '权限容器',
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_access_tokens` */

insert  into `oauth_access_tokens`(`access_token`,`client_id`,`user_id`,`expires`,`scope`) values ('0232dba0086c06c5352a849ab00091506e16477b','cs-003','admin','2016-04-29 10:42:03',NULL),('08f3d91b3375bff7af22edb4a1d350201d3fdcf5','cs-003','admin','2016-04-29 09:30:29',NULL),('0dd3c8395a18ed8b4d35ddfb362fe1776bf417a5','cs-003','admin','2016-04-29 09:40:11',NULL),('11011e3a27ccb6f9a8bd9e70f9de58e5541f9f8a','cs-003','admin','2016-04-29 10:07:55',NULL),('203889aa397347acc84f84a2b7fea23832d3cb50','cs-003','admin','2016-04-29 09:59:02',NULL),('216d646c738f0c688290fc6239dd8e967bd6fecf','cs-003','admin','2016-04-29 09:38:26',NULL),('223ff629cb8bbb44ce4d19464cd20c3d32da32ca','cs-003','admin','2016-05-05 12:13:52',NULL),('251e4ee758cdef4501370c3e96a0b6c6be5de0bf','cs-003','admin','2016-04-29 09:37:25',NULL),('270880733979c468627827a14a8f7dd0f50cb484','cs-003','admin','2016-04-29 10:07:38',NULL),('2973ac44c3d90b5775e8cfbd6d48c6487335008a','cs-003','admin','2016-04-29 09:59:47',NULL),('2ea99aa9c97f225c5d735e64fc475fad7946a52f','cs-003','admin','2016-05-05 12:13:09',NULL),('4b2b08b4f516d2022e3d2c75b453986832be9c5e','cs-003','admin','2016-04-29 09:39:38',NULL),('50059ce3df9a98eb6547b9af06e7fc40f4c80811','cs-003','admin','2016-04-29 10:43:29',NULL),('635438c1b6023b501a5ac1e9dd402397c98f0606','cs-003','admin','2016-04-29 09:41:56',NULL),('6673291a26f4f8a9c9898ea0e1abb15a189829cc','cs-003','admin','2016-04-29 09:43:53',NULL),('69b7f619dcfed87f27b5d7877c2a760089e91c96','cs-003','admin','2016-04-29 10:33:46',NULL),('6ccf463150af1775e25b30283588851f955e8181','cs-003','admin','2016-04-29 10:15:47',NULL),('74a41c791214a1e1d28bdef60df17d8b18467396','cs-003','admin','2016-05-05 06:57:27',NULL),('88ac571bf46ba9373ef8f59c714ddbbd88050108','cs-003','admin','2016-04-29 09:37:58',NULL),('946ae291877396658144acb8c3d6b8458b78bc78','cs-003','admin','2016-04-29 09:59:17',NULL),('9b45e738580e75338c0e141067f094a4298fdfcc','cs-003','admin','2016-05-05 12:10:34',NULL),('9c9e437bde3edfab5371df0f6b0376e29d1d3962','cs-003','admin1','2016-05-05 08:41:15',NULL),('9cdb765d64907d22a6a790dac7b7ef2d287b970a','cs-003','admin1','2016-05-05 12:14:26',NULL),('9f5694e9c23759505980607a0129bbec6e3011cc','cs-003','admin','2016-04-29 09:39:23',NULL),('9ff9cde7f22a69e3e965d2e2cdd8a7c73f0f4049','cs-003','admin','2016-04-29 10:01:45',NULL),('d026ac23595f9c411299d431a8bea94d331cfc06','cs-003','admin','2016-04-29 10:42:21',NULL),('d7e002a87d407dbced9be8046c1ca47765ea14cc','cs-003','admin','2016-04-29 09:38:40',NULL),('dc199f56c7cf7be71c69485d28292c1efbdd19a8','cs-003','admin','2016-04-29 10:00:40',NULL),('e5fc39c3f9eef280180a3bfef08da1fde186cbb6','cs-003','admin','2016-05-05 08:04:53',NULL),('ebdd6d67c164b73bc2a81d13efc2f49b766dd2f7','cs-003','admin','2016-05-04 12:01:51',NULL),('f5df9b1685f0f27c8aa39089d9728564403259d2','cs-003','admin','2016-04-29 09:29:55',NULL),('f6044719f8d7a16cd31cfa3d886a1e35f3fcd104','cs-003','admin','2016-04-29 09:57:38',NULL),('fe1f0695421d31a9e93433ec213d08f204c6f7c7','cs-003','admin','2016-04-29 09:26:55',NULL);

/*Table structure for table `oauth_authorization_codes` */

DROP TABLE IF EXISTS `oauth_authorization_codes`;

CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL COMMENT '通过Authorization 获取到的code，用于获取access_token',
  `client_id` varchar(80) NOT NULL COMMENT '开发者Appid',
  `user_id` varchar(255) DEFAULT NULL COMMENT '开发者用户id',
  `redirect_uri` varchar(2000) DEFAULT NULL COMMENT '认证后跳转的url',
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '认证的时间date("Y-m-d H:i:s")',
  `scope` varchar(2000) DEFAULT NULL COMMENT '权限容器',
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_authorization_codes` */

/*Table structure for table `oauth_clients` */

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL COMMENT '开发者AppId',
  `client_secret` varchar(80) NOT NULL COMMENT '开发者AppSecret',
  `redirect_uri` varchar(2000) NOT NULL COMMENT '认证后跳转的url',
  `grant_types` varchar(80) DEFAULT NULL COMMENT '认证的方式，client_credentials、password、refresh_token、authorization_code、authorization_access_token',
  `scope` varchar(100) DEFAULT NULL COMMENT '权限容器',
  `user_id` varchar(80) DEFAULT NULL COMMENT '开发者用户id',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_clients` */

insert  into `oauth_clients`(`client_id`,`client_secret`,`redirect_uri`,`grant_types`,`scope`,`user_id`) values ('cs-003','csadmin','http://homeway.me/','client_credentials password authorization_code refresh_token','file node userinfo cloud','xiaocao');

/*Table structure for table `oauth_jwt` */

DROP TABLE IF EXISTS `oauth_jwt`;

CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL COMMENT '开发者用户id',
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_jwt` */

/*Table structure for table `oauth_refresh_tokens` */

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL COMMENT '跟新access_token的token',
  `client_id` varchar(80) NOT NULL COMMENT '开发者AppId',
  `user_id` varchar(255) DEFAULT NULL COMMENT '开发者用户id',
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '认证的时间date("Y-m-d H:i:s")',
  `scope` varchar(2000) DEFAULT NULL COMMENT '权限容器',
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_refresh_tokens` */

insert  into `oauth_refresh_tokens`(`refresh_token`,`client_id`,`user_id`,`expires`,`scope`) values ('02f05ae0128745ea20c945a2ca669f806f9d80c1','cs-003','admin','2016-04-13 09:59:02',NULL),('131f897fc987ad8753a1afb128a80053e17eef7d','cs-003','admin','2016-04-13 10:07:55',NULL),('2517cdecbdb2ab56de91a449d327e58013db5be4','cs-003','admin','2016-04-13 09:59:17',NULL),('275edce5ad3e2cf9ab524fb355aeb30daf1f54a8','cs-003','admin','2016-04-13 10:33:46',NULL),('3618e9c18ea9e5854a2cd62592b343b780f8a523','cs-003','admin','2016-04-13 09:38:40',NULL),('38f028f21207694cb4dcf92d4b62735846c3ba2d','cs-003','admin1','2016-04-19 12:14:26',NULL),('3925b4f4aa302830fcce257db110b33d3db1293a','cs-003','admin','2016-04-13 09:37:25',NULL),('4caf5d64f5bb68d3483fc335ed83356167b9e0b4','cs-003','admin','2016-04-13 09:40:11',NULL),('4f4820bcae23b542ba93541f2516cb55716be9b0','cs-003','admin','2016-04-13 10:01:45',NULL),('4f55da58fcebcff700a45e29686804c864646fe6','cs-003','admin','2016-04-13 10:42:03',NULL),('53f5963c8e61518c392a526597c8fdd6659be564','cs-003','admin','2016-04-18 12:01:51',NULL),('611b93b625124cc71fc111d0164ede5b575c8817','cs-003','admin','2016-04-13 09:41:57',NULL),('673884046cdde579ba14c1eb1c53fc4eb59add42','cs-003','admin','2016-04-13 10:43:29',NULL),('6c4f5088e1e3185b3c8ba3bd1b2afd2414ee93e1','cs-003','admin','2016-04-19 12:13:09',NULL),('7732c18b63c2e5b44ad078ffd865e1ea2ff2c0f2','cs-003','admin','2016-04-13 09:39:23',NULL),('7eab1544a4daa3f456bde93814c6fe785e9ac56b','cs-003','admin','2016-04-19 08:04:53',NULL),('7fa12185e45d4f12fbd08d58e31fd0a004c070d3','cs-003','admin','2016-04-13 10:15:47',NULL),('815fb02eac4dd48260f4fab5e207df8de81d8094','cs-003','admin','2016-04-19 06:57:27',NULL),('8233f672d480afb5b8ce4d4ad2d91772cf060166','cs-003','admin','2016-04-13 09:37:58',NULL),('8236ece7c4101abaa67df063310e7ff9f5426aa8','cs-003','admin','2016-04-13 09:57:38',NULL),('88c0eaa8c12dcbf8e1e71da539ce10701bbdba24','cs-003','admin','2016-04-13 09:38:26',NULL),('8a09caa60746c0bac2ab1d72e910ed83a8c88d44','cs-003','admin1','2016-04-19 08:41:15',NULL),('97f4e31461fd81e0ced3db7fce5385b444583158','cs-003','admin','2016-04-13 09:29:55',NULL),('9dc6b23af54b8dd8d7df8f7fae45029b65d7f306','cs-003','admin','2016-04-13 10:07:38',NULL),('a99e4bc46a0e03909d53bedae6eb0f80e910258e','cs-003','admin','2016-04-13 09:43:53',NULL),('ae64d6bf791059b63384126450ca99dc5fcaaec1','cs-003','admin','2016-04-13 10:42:21',NULL),('b6d1d434ca73dc4e8c12669fcd8133f16cf59b36','cs-003','admin','2016-04-13 09:26:55',NULL),('d0cf58eabba32aa2e37daac6975c18df39f05765','cs-003','admin','2016-04-13 09:30:29',NULL),('e0f8207b246c0d6f7074d73e631e84c258a9d471','cs-003','admin','2016-04-13 09:59:47',NULL),('ea7d65b11f835d0f8a7dac31d1f4858b21b62c7d','cs-003','admin','2016-04-13 09:39:38',NULL),('f01f6bf88fdae619b507ce396e4c27439bdff7dc','cs-003','admin','2016-04-19 12:10:35',NULL),('f7c74449dd5128d973c1f15bb4da39849cdabeaa','cs-003','admin','2016-04-13 10:00:40',NULL),('fae94217ad5da356195fd5cbf615e8662802eb4e','cs-003','admin','2016-04-19 12:13:52',NULL);

/*Table structure for table `oauth_scopes` */

DROP TABLE IF EXISTS `oauth_scopes`;

CREATE TABLE `oauth_scopes` (
  `scope` text COMMENT '容器名字',
  `is_default` tinyint(1) DEFAULT NULL COMMENT '是否默认拥有，1=>是，0=>否'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_scopes` */

/*Table structure for table `oauth_users` */

DROP TABLE IF EXISTS `oauth_users`;

CREATE TABLE `oauth_users` (
  `username` varchar(255) NOT NULL COMMENT '内部时候使用的认证用户名',
  `password` varchar(2000) DEFAULT NULL COMMENT '内部时候使用的认证用户密码',
  `first_name` varchar(255) DEFAULT NULL COMMENT '内部时候使用',
  `last_name` varchar(255) DEFAULT NULL COMMENT '内部时候使用',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `oauth_users` */

/*Table structure for table `registered_devices` */

DROP TABLE IF EXISTS `registered_devices`;

CREATE TABLE `registered_devices` (
  `rd_id` int(11) NOT NULL AUTO_INCREMENT,
  `dev_id` varchar(500) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` varchar(5) DEFAULT NULL,
  `device_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`rd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `registered_devices` */

insert  into `registered_devices`(`rd_id`,`dev_id`,`user_id`,`user_type`,`device_type`) values (1,'988887',1,NULL,'Android');

/*Table structure for table `sys_users` */

DROP TABLE IF EXISTS `sys_users`;

CREATE TABLE `sys_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `address` text,
  `img` varchar(500) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` date DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `sys_users` */

insert  into `sys_users`(`user_id`,`fname`,`lname`,`email`,`username`,`password`,`phone`,`address`,`img`,`created_at`,`created_by`,`modified_at`,`modified_by`,`status`) values (1,'khan','gul','khan@gmail.com','admin','21232f297a57a5a743894a0e4a801fc3','0333978787','Here is the address',NULL,NULL,NULL,'2016-03-28',1,1),(2,'sadiq','khan','sadiq810@gmail.com','admin1','21232f297a57a5a743894a0e4a801fc3','0987878','here is address',NULL,'2016-03-28',1,NULL,NULL,1),(3,'testsdf','testsdf','test@gmail.comsdf','trestsdf','098f6bcd4621d373cade4e832627b4f6','099970234','here is address3243',NULL,'2016-03-28',1,'2016-03-28',1,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
