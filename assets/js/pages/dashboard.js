/*------------------------------------------------------------------
 [Dashboard Trigger Js]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :   http://aimmate.com
 Support     :   aimmateteam@gmail.com
 Primary use :	Dashboard
 -------------------------------------------------------------------*/

jQuery(document).ready(function($) {
    'use strict';

    var $windowWidth = $(window).width();
    if($windowWidth > 640){
        if(!detectIE()){
           // autoUpdateNumber();
        }
    }
    easyPeiChartWidget();
    //weatherIcon();
    socialShare();
    vectorMapWidget();
    flotChartStartPie();

    plotAccordingToChoicesDataSet(); //Series Toggle Widget chart Load
   // plotAccordingToChoicesToggle(); //Series Toggle Widget chart toggle button Trigger

    flot_real_time_chart_start();
    flot_real_time_chart_start_update(); //Real time update call

   // notificationCall();
    colorChanger();
});

/********* Easy Pie Chart Widget Start *********/
function easyPeiChartWidget(){
    'use strict';

    $('#pie-widget-1').easyPieChart({
        animate: 2000,
        barColor: $redActive,
        scaleColor: $redActive,
        lineWidth: 5,
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
            $(this.el).find('.pie-widget-count-1').text(Math.round(percent));
        }
    });
    $('#pie-widget-2').easyPieChart({
        animate: 2000,
        barColor: $lightGreen,
        scaleColor: $lightGreen,
        lineWidth: 5,
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
            $(this.el).find('.pie-widget-count-2').text(Math.round(percent));
        }
    });
    $('#pie-widget-3').easyPieChart({
        animate: 2000,
        barColor: $lightBlueActive,
        scaleColor: $lightBlueActive,
        easing: 'easeOutBounce',
        lineWidth: 5,
        onStep: function (from, to, percent) {
            $(this.el).find('.pie-widget-count-3').text(Math.round(percent));
        }
    });
    $('#pie-widget-4').easyPieChart({
        animate: 2000,
        barColor: $success,
        scaleColor: $success,
        easing: 'easeOutBounce',
        lineWidth: 5,
        onStep: function (from, to, percent) {
            $(this.el).find('.pie-widget-count-4').text(Math.round(percent));
        }
    });
    //update instance after 10 sec
    var $windowWidth = $(window).width();
 /*   if($windowWidth > 640){
        setInterval(function() {
            /!*var randomNumber = getRandomNumber();
            $('#pie-widget-1').data('easyPieChart').update(randomNumber);
            var randomNumber = getRandomNumber();
            $('#pie-widget-2').data('easyPieChart').update(randomNumber);
            var randomNumber = getRandomNumber();
            $('#pie-widget-3').data('easyPieChart').update(randomNumber);
            var randomNumber = getRandomNumber();
            $('#pie-widget-4').data('easyPieChart').update(randomNumber);*!/

        }, 10000);
    }*/
}

function  getRandomNumber(){
    'use strict';

    var number = 1 + Math.floor(Math.random() * 100);
    return number;
}

/********* Easy Pie Chart Widget End *********/

/********* Weather  Widget Icon Start *********/

function weatherIcon(){
    'use strict';

    var icons = new Skycons({"color": "#fff"}),
        list  = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
        ],
        i;

    for(i = list.length; i--; )
        icons.set(list[i], list[i]);

    icons.play();
}
/********* Weather  Widget Icon End  *********/

/********* Social Share overlay Start  *********/
function socialShare(){
    'use strict';

    $(".social-share").mouseenter(function(){
        // handle the mouseenter functionality
        $(this).addClass("overlay-hover");
    }).mouseleave(function(){
        // handle the mouseleave functionality
        $(this).removeClass("overlay-hover");
    });
}
/********* Social Share overlay End  *********/

/********* Vector Map Widget Start  *********/
function vectorMapWidget(){
    'use strict';

    var $mapBackgroundColor = '#fff';
    var $mapRegionStyle = $lightGreen;
    var $mapRegionStyleHover = '#58595B';
    var $mapMarkerColor = $isOnline;
    var $mapStrokeColor = $isOnline;
    var users = '';

    $.ajax({
        url:baseURL+"index.php/dashboard/get_users_forMap",
        type:"get",
        success:function(response){
            //users = JSON.parse(response);
            users = $.parseJSON(response);
            $.each(users, function(key,value){
                if(value.online == 0){
                    value.style = {fill: $isOffline,stroke:'none'};
                }//.... end of if() .....//

                if(value.busy){
                    value.style = {fill: $isBusy,stroke:'none'};
                }//.... end of if() ....//
            });
            $('#world_map').vectorMap({
                map: 'world_mill_en',
                normalizeFunction: 'polynomial',
                backgroundColor: $mapBackgroundColor,
                regionStyle: {
                    initial: {
                        fill: $mapRegionStyle
                    },
                    hover: {
                        fill: $mapRegionStyleHover
                    }
                },
                markerStyle: {
                    initial: {
                        fill: $mapMarkerColor,
                        stroke: $mapStrokeColor,
                        r:4
                    },
                    hover: {
                        stroke: '#f2f2f2',
                        "stroke-width": 1
                    }
                },
                markers:users
                /*markers: [
                 {latLng: [23.780, 90.419], name: 'Dhaka, Bangladesh'},
                 {"latLng":["46.809358","-96.9157318"],"name":"Jane Doe, 374 Magnolia Drive Asheville, NC 28803"},
                 {latLng: [40.71, -74.00], name: 'New York'},
                 {latLng: [34.05, -118.24], name: 'Los Angeles', style: {fill: $isBusy,stroke:'none'} },
                 {latLng: [41.87, -87.62], name: 'Chicago', style: {fill: $isBusy,stroke:'none'}},
                 {latLng: [29.76, -95.36], name: 'Houston'},
                 {latLng: [26.02, 50.55], name: 'Bahrain', style: {fill: $isIdle,stroke:'none'} },
                 {latLng: [19.082, 72.881], name: 'Mumbai,  India', style: {fill: $isOffline,stroke:'none'} },
                 {latLng: [55.749, 37.632], name: 'Russia, Moscow', style: {fill: $isBusy,stroke:'none'} },
                 {latLng: [51.629, -69.259], name: 'Rio Gallegos, Argentina'}
                 ]*/
            });

        }
    });

}
/********* Vector Map Widget End  *********/

/********* flot Chart Pie Widget Start  *********/
function flotChartStartPie(){
    'use strict';

    var pieData = [],
        series = Math.floor(Math.random() * 6) + 1;

    for (var i = 0; i < series; i++) {
        pieData[i] = {
            label: "Product - " + (i + 1),
            data: Math.floor(Math.random() * 100) + 1
        }
    }
    var $flotPieChart = $('#flotPieChart');
    var pieData = [
        {
            label:"Product - 1",
            data:43
        },{
            label:"Product - 2",
            data:19
        },{
            label:"Product - 3", data:89
        },{ label:"Product - 4", data:83
        }
    ]
    $.plot($flotPieChart, pieData, {
        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 3/4,
                    formatter: labelFormatter,
                    background: {
                        opacity: 0.5,
                        color: '#000'
                    }
                }
            }
        },
        legend: {
            show: false
        },
        colors: [$fillColor2, $lightBlueActive, $redActive, $blueActive, $brownActive, $greenActive]
    });
}
function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}
/********* flot Chart Pie Widget End  *********/


/******** Change color Widget Start ************/
function colorChanger(){
    'use strict';

    var $fullContent = $("body");
    $('.change-color-switch ul li ').click(function(){

        $fullContent.removeClass('black-color');
        $fullContent.removeClass('blue-color');
        $fullContent.removeClass('deep-blue-color');
        $fullContent.removeClass('red-color');
        $fullContent.removeClass('light-green-color');
        $fullContent.removeClass('default');
        $('.change-color-switch ul li ').removeClass('active');

        if($(this).hasClass('active')){

        } else{
            var className = $(this).attr('class');
            $fullContent.addClass(className);
            $(this).addClass('active');
        }
    });
}
/******** Change color Widget End ************/

/******** Notification On dashboard Start ************/
function notificationCall(){
    'use strict';

   /* $.amaran({
        content:{
            message:'New Mail Arrived',
            size:'4 new mail in inbox',
            file:'',
            icon:'fa fa-envelope-o'
        },
        theme:'default ok',
        position:'bottom right',
        inEffect:'slideRight',
        outEffect:'slideBottom',
        closeButton:true,
        delay: 5000
    });*/
    setTimeout(function(){
        $.amaran({
            content:{
                img:'assets/images/demo/avatar-80.png',
                user:'New Chat Message',
                message:'Hi, How are you ? please knock me when you arrived <i class="fa fa-smile-o"></i>'
            },
            theme:'user',
            position:'bottom left',
            inEffect:'slideRight',
            outEffect:'slideBottom',
            closeButton:true,
            delay: 5000
        });
       /* setTimeout(function(){
            $.amaran({
                content:{
                    message:'Can\'t deliver the product',
                    size:'32 Kg',
                    file:'H: 32 Road: 21, Chicago, NY 3210',
                    icon:'fa fa fa-truck'
                },
                theme:'default error',
                position:'top right',
                inEffect:'slideRight',
                outEffect:'slideTop',
                closeButton:true,
                delay: 5000
            });
        }, 5000)*/
    }, 5000);
}
/******** Notification On dashboard End ************/

/******** Auto Update Number dashboard Start ************/
function autoUpdateNumber(){
    'use strict';

    var countUpOptions = {
        useEasing : true, // toggle easing
        useGrouping : true, // 1,000,000 vs 1000000
        separator : ',', // character to use as a separator
        decimal : '.' // character to use as a decimal
    };

    var productUp = new countUp('product-up', 0, 39, 0, 9.5, countUpOptions);
    productUp.start();

    var incomeShow = new countUp('income-show', 0, 10299.30, 2, 9.5, countUpOptions);
    incomeShow.start();

    var userShow = new countUp('user-show', 0, 132129, 0, 5.5, countUpOptions);
    userShow.start();

    var deliverShow = new countUp('deliver-show', 0, 10490, 1, 6.5, countUpOptions);
    deliverShow.start();

    var downloadShow = new countUp('download-show', 0, 5340, 0, 9.5, countUpOptions);
    downloadShow.start();
    var saleView = new countUp('sale-view', 0, 2129, 0, 9.5, countUpOptions);
    saleView.start();
}
/******** Auto Update Number dashboard end ************/


/******** Flot Real time chart Start  ************/
var data = [0,0,0,0,0,0,0,0,0,0,0,0];
    totalPoints = 12;
function getRandomData() {
    'use strict';

    if (data.length > totalPoints)
        data = data.slice(1);


    $.ajax({
        url:baseURL+"index.php/dashboard/get_online_users",
        type:"post",
        success:function(response){
            //if(data[data.length-1] != response)
            data.push(parseInt(response));
            /*data.push(parseInt(response));
            if (data.length > 500)
                data.shift();*/
        }//.... end of success:
    });
    // Do a random walk

  /*  while (data.length < totalPoints) {

        var prev = data.length > 0 ? data[data.length - 1] : 50,
            y = prev + Math.random() * 10 - 5;

        if (y < 0) {
            y = 0;
        } else if (y > 100) {
            y = 99;
        }

        data.push(y);
    }*/

    // Zip the generated y values with the x values

    var res = [];
    for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
    }
    return res;
}

var plot;
function flot_real_time_chart_start(){
    'use strict';

    plot = $.plot("#realTimeChart", [ getRandomData() ], {
        series: {
            lines: {
                show: true,
                lineWidth:1,
                fill: true,
                fillColor: {

                    colors: [
                        {
                            opacity: 0.2
                        },
                        {
                            opacity: 0.1
                        }
                    ]
                }
            },
            shadowSize: 0
        },
        colors: ['#1FB5AD'],
        yaxis: {
            min: 0,
            max: 150
        },
        xaxis: {
            show: false
        },
        grid: {
            tickColor: $fillColor1,
            borderWidth: 0
        }
    });
}
function flot_real_time_chart_start_update(){
    'use strict';

    var updateInterval = 6000;

    plot.setData([getRandomData()]);

    // Since the axes don't change, we don't need to call plot.setupGrid()

    plot.draw();
    setTimeout(flot_real_time_chart_start_update, updateInterval);
}
/******** Flot Real time chart End  ************/


/******** Flot According To Choices chart Start  ************/
var choiceContainer;
var datasets;
var Tparent = [], Tbabysitter=[],Tincome=[];
function plotAccordingToChoicesDataSet(){
    'use strict';

    $.ajax({
        type:"POST",
        url:baseURL+"index.php/dashboard/get_yearly_report",
        success:function(respones){
            var data = $.parseJSON(respones);
            $.each(data.parents,function(key, value){
                Tparent.push([parseInt(value.year), value.percentage]);
            });//.... end of foreach() ....//

            $.each(data.babysitters,function(key, value){
                Tbabysitter.push([parseInt(value.year), value.percentage]);
            });//.... end of foreach() ....//

            $.each(data.income,function(key, value){
                Tincome.push([parseInt(value.year), value.percentage]);
            });//.... end of foreach() ....//
            populate_graph();
            plotAccordingToChoicesToggle();
        }
    });//....  end of ajax() ....//
}

function populate_graph(){
    'use strict';
    datasets = {
        "a": {
            label: "Parents",
            data: Tparent
        },
        "b": {
            label: "Income",
            data: Tincome
        },
        "c": {
            label: "Babysitters",
            data: Tbabysitter
        }
    };
    var i = 0;
    $.each(datasets, function (key, val) {
        val.color = i;
        ++i;
    });

// insert checkboxes
    choiceContainer = $("#choicesWidget");
    $.each(datasets, function (key, val) {
        choiceContainer.append("<li><input class='switchCheckBox' data-size='mini' type='checkbox' name='" + key +
            "' checked='checked' id='id" + key + "'></input>" +
            "<br/><label class='switch-label' for='id" + key + "'>"
            + val.label + "</label></li>");
    });

    plotAccordingToChoices();
}

function plotAccordingToChoices() {
    'use strict';

    var data = [];
    choiceContainer.find("input:checked").each(function () {
        var key = $(this).attr("name");
        if (key && datasets[key]) {
            data.push(datasets[key]);
        }
    });

    if (data.length > 0) {
        $.plot("#seriesToggleWidget", data, {
            highlightColor: $lightGreen,
            yaxis: {
                min: 0,
                show:true,
                color: '#E3DFD8'
            },
            xaxis: {
                tickDecimals: 0,
                show:true,
                color: '#E3DFD8'

            },
            colors: [$lightGreen, $redActive, $lightBlueActive, $greenActive],
            grid: {
                borderColor: '#E3DFD8'
            }
        });
    }
    $(".switchCheckBox").bootstrapSwitch();
}
function plotAccordingToChoicesToggle(){
    'use strict';
    $(".switchCheckBox").on('switchChange.bootstrapSwitch', function (event, state) {
        plotAccordingToChoices();
    });
}



/******** Flot According To Choices chart End  ************/
