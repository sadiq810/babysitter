<?php

class Dashboard extends MY_Controller
{
	/**
	 * Dashboard constructor.
	 */
    public function __construct()
    {
        parent::__construct();
    }//.... end of __construct() ....//

	/**
	 * Function for loading main dashboard....
	 */
	public function index()
	{
		$this->load->view('admin/dashboard/index');
	}//.... end of index() ....//
	
	/**
	 * Function for getting popup notifications....
	 */
	public function getNotification()
	{
		$notifications = $this->common_model->get_record('notifications',array("notification_id","title","description","nsource","from"), array("nfor"=> "admin",'sentStatus'=> 0));
		foreach($notifications as $notification){
			//.... change sent Status of the notification,
			$this->common_model->update_record("notifications",array('sentStatus'=> 1), array('notification_id'=> $notification->notification_id));
			//.... get img url if any....//
			if($notification->nsource == "p"){
				$table = "parent";
				$where = array('parent_id'=> $notification->from);
			}else{
				$table = "baby_setter";
				$where = array('bs_id'=> $notification->from);
			}
			$img = $this->common_model->get_record($table,array('img'), $where);
			$notification->img = $img[0]->img;
		}
		print json_encode($notifications);
	}//.... end of getNotification() .....//
	
	/**
	 * Function for loading all notifications....
	 */
	public function load_all_notifications()
	{
		$this->load->view('admin/dashboard/notifications');
	}//....  end of load_all_notifications() ....//
	
	/**
	 * Function for populating notification grid....
	 */
	public function get_notifications()
	{
		$this->db->order_by("notifications.from","DESC");
		$this->datatables->select('fname,lname,email,username,phone,address,upgrade_request as approve_request,utype,bs_id,notification_id')
				->from('babysitter')->where(array('nfor'=> "admin","notifications.status"=> 1))->join("notifications","babysitter.bs_id = notifications.from","inner")
				->add_column('Action', '<a href="'.base_url().'index.php/dashboard/delete_notification/$1" class="btn btn-xs btn-danger btnDelete"><i class=\'fa fa-minus\'></i></a>', 'notification_id')
				->add_column('approve', '<a href="'.base_url().'index.php/dashboard/approve_account/$1" class="btn btn-xs btn-success btnApprove"><i class=\'fa fa-check\'></i></a>', 'bs_id');
		/*add_column('Action','<a class="btn btn-danger delete" href="#">Delete</a>','bs_id');*/
		echo $this->datatables->generate();
	}//.... end of get_notifications() ....//
	
	/**
	 * Function for deleting notification ....
	 */
	public function delete_notification($id = '')
	{
		$this->common_model->update_record("notifications", array("status"=> 0), array('notification_id'=> $id));
		echo true;
	}//.... end of delete_notification() .....//
	
	/**
	 * function for approving babysitter request of the parent.....
	 */
	public function approve_account($id = '')
	{
		$this->common_model->update_record("babysitter", array("utype"=> 'b'), array('bs_id'=> $id));
		echo true;
	}//.... end of approve_account() .....//
	
	/**
	 * Function for getting Online Users for dashboard chart...
	 */
	public function get_online_users()
	{
		$users = $this->common_model->get_record("babysitter" ,array("bs_id") ,array("is_online"=> 1) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		echo $users;
	}//.... end of get_online_users() ....//
	
	/**
	 * function for getting all members
	 */
	public function get_all_members()
	{
		$users = $this->common_model->get_record("babysitter" ,array("bs_id") ,array("status"=> 1) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return $users;
	}//.... end of get_all_members() .....//
	
	/**
	 * function for getting total numbers of parents....
	 */
	public function get_all_parents()
	{
		$users = $this->common_model->get_record("babysitter" ,array("bs_id") ,array("status"=> 1,"utype"=> NULL) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return $users;
	}//.... end of get_all_parents() ...../
	
	/**
	 * function for getting total numbers of babysitter....
	 */
	public function get_all_babysitters()
	{
		$users = $this->common_model->get_record("babysitter" ,array("bs_id") ,array("status"=> 1 ,"utype"=> "b") ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return $users;
	}//.... end of get_all_babysitters() ....//
	
	/**
	 * Function for getting total number of all jobs.....
	 */
	public function get_all_jobs()
	{
		$jobs = $this->common_model->get_record("jobs" ,array("job_id") ,array("YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return $jobs;
	}//.... end of get_all_jobs() .....//
	
	/**
	 * Function for getting total of rejected jobs.....
	 */
	public function get_all_rejectedJobs()
	{
		$jobs = $this->common_model->get_record("jobs" ,array("job_id") ,array("is_accepted"=> 2,"YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return $jobs;
	}//..... end of get_all_rejectedJobs() ......//
	
	/**
	 * Function for getting total of completed jobs.....
	 */
	public function get_all_completedJobs()
	{
		$jobs = $this->common_model->get_record("jobs" ,array("job_id") ,array("is_accepted"=> 1,"is_finished"=> 1,"YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return $jobs;
	}//.... end of get_all_completedJobs() .....//

	/**
	 * Function for getting total of pending jobs...
	 */
	public function get_all_pendingJobs()
	{
		$jobs = $this->common_model->get_record("jobs" ,array("job_id") ,array("is_accepted"=> 0,"is_finished"=> 0,"status"=> 1, "YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return $jobs;
	}//.... end of get_all_pendingJobs() .....//
	
	/**
	 * Function for getting total of progress jobs.....
	 * 
	 */
	public function get_all_progressJobs()
	{
		$jobs = $this->common_model->get_record("jobs" ,array("job_id") ,array("is_accepted"=> 1,"is_finished"=> 0,"status"=> 1,"YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return $jobs;
	}//.... end of get_all_progressJobs() .....//

	/**
	 * Function for getting total income of the babysitters....
	 */
	public function get_total_income()
	{
		$jobs = $this->common_model->get_record("jobs" ,array("SUM(total_cost) as total") ,array("is_accepted"=> 1,"is_finished"=> 1) ,$orderby='' ,$groupby='', $limit='', $skip ='', "");
		return $jobs[0]->total;
	}//.... end of get_total_income() ....//

	/**
	 * Function for getting total of new jobs in this current month.....
	 */
	public function get_newJobs()
	{
		$jobs = $this->common_model->get_record("jobs" ,array("job_id") ,array("status"=> 1,"MONTH(created_at)"=> date("m"),"YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', True);
		return $jobs;
	}//.... end of get_newJobs() ....//
	
	/**
	 * Function for getting total of new users....
	 */
	public function get_newUsers()
	{
		$users = $this->common_model->get_record("babysitter" ,array("bs_id") ,array("status"=> 1,"MONTH(created_at)"=> date("m"),"YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', True);
		return $users;
	}//.... end of get_newUsers() .....//
	
	/**
	 * Function for getting total income of the current month.....
	 */
	public function get_income_current_month()
	{
		$jobs = $this->common_model->get_record("jobs" ,array("SUM(total_cost) as total") ,array("is_accepted"=> 1,"is_finished"=> 1,"YEAR(created_at)"=> date("Y"),"YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', "");
		return $jobs[0]->total;
	}//.... end of get_income_current_month() ....//

	/**
	 * Function for getting total of completed jobs in current month
	 */
	public function get_current_month_completedJobs()
	{
		$tjobs = $this->common_model->get_record("jobs" ,array("job_id") ,array("MONTH(created_at)"=> date("m"),"YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		$jobs = $this->common_model->get_record("jobs" ,array("job_id") ,array("is_accepted"=> 1,"is_finished"=> 1,"MONTH(created_at)"=> date("m"),"YEAR(created_at)"=> date("Y")) ,$orderby='' ,$groupby='', $limit='', $skip ='', TRUE);
		return array($jobs,$tjobs);
	}//.... end of get_current_month_completedJobs() ....//
	
	/**
	 * Function for getting users with status for dashboard vector map....
	 */
	public function get_users_forMap()
	{
		$users = $this->common_model->get_record("babysitter" ,array("bs_id","CONCAT(fname,' ',lname) as name", 'address','lat','long','is_online') ,array("status"=> 1) ,$orderby='' ,$groupby='', $limit='', $skip ='', "");
		$data = array();
		foreach($users as $key=> $user){
			$arr = array('latLng'=>array($user->lat,$user->long),'name'=> $user->name.", ".$user->address,"online"=> $user->is_online);
			//.... check user is busy or idle .....//
			$userStatus = $this->common_model->get_record("jobs" ,array("babysitter_id") ,array("babysitter_id"=> $user->bs_id,"is_accepted"=> 1, "is_finished"=> 0,"status"=> 1,"clock_in !="=> NULL,"clock_out"=> NULL, "created_at"=> date("Y-m-d")) ,$orderby='' ,$groupby='', $limit='', $skip ='', "");
			if(!empty($userStatus))
				$arr['busy'] = true;
			$data[] = $arr;
		}//.... end of foreach() ....//

		print json_encode($data);
	}//.... end of get_users_forMap() .....//

	/**
	 * Function for getting 6 months sales report....
	 */
	public function get_six_months_saleReport()
	{
		$columns = array("COUNT(total_cost) as total","created_at");
		$where = array("is_accepted"=> 1,"is_finished"=> 1,"YEAR(created_at)"=> date("Y"),"MONTH(created_at) <"=>date("m"));
		$groupby = array("MONTH(created_at)");
		$orderby = "MONTH(created_at) DESC";
		$income = $this->common_model->get_record("jobs" ,$columns ,$where ,$orderby ,$groupby, $limit= 6, $skip ='', "");
		//$totalMonths = (int) date('m', strtotime('-1 month'));
		//.... get total income in this year....//
		$totalIncomeInYear = $this->common_model->get_record("jobs" ,array("SUM(total_cost) as total") ,array("is_accepted"=> 1,"is_finished"=> 1,"YEAR(created_at)"=> date("Y"),"MONTH(created_at) <"=>date("m")) ,$orderby='' ,$groupby='', $limit='', $skip ='', "");
		//$averageIncome = (int) $totalIncomeInYear[0]->total/$totalMonths;
		
		foreach($income as $k){
			$k->month = date("M",strtotime($k->created_at));
			$totalinYear = ($totalIncomeInYear[0]->total == 0)? 1: $totalIncomeInYear[0]->total;
			$k->percentage =(int)($k->total*100/$totalinYear);
		}
		return (array_reverse($income));
	}//.... end of get_six_months_saleReport() ....//
	
	/**
	 * Function for getting yearly report of babysitters, parents, and income...... 
	 */
	public function get_yearly_report()
	{
		$columns = array("SUM(total_cost) as total","YEAR(created_at) as year");
		$where = array("is_accepted"=> 1,"is_finished"=> 1);
		$groupby = array("YEAR(created_at)");
		$orderby = "YEAR(created_at) DESC";
		$income = $this->common_model->get_record("jobs" ,$columns ,$where ,$orderby ,$groupby, $limit= 6, $skip ='', "");
		$totalIncome = 1;
		foreach($income as $k=> $val){
			$totalIncome += $val->total;
		}//.... end of foreach() ....//
		//... find out percentage of each year ....//
		foreach($income as $value){
			$value->percentage =(int) ($value->total *100/ $totalIncome);
		}
		$data['income'] = array_reverse($income);

		//..... Now Find Parents Percentage ....//
		$columns = array("COUNT(bs_id) as total","YEAR(created_at) as year");
		$where = array("status"=> 1);
		$groupby = array("YEAR(created_at)");
		$orderby = "YEAR(created_at) DESC";
		$parents = $this->common_model->get_record("babysitter" ,$columns ,$where ,$orderby ,$groupby, $limit= 6, $skip ='', "");

		$totalParents = 1;
		foreach($parents as $k=> $val){
			$totalParents += $val->total;
		}//.... end of foreach() ....//
		//... find out percentage of each year ....//
		foreach($parents as $value){
			$value->percentage =(int) ($value->total *100/ $totalParents);
		}
		$data['parents'] = array_reverse($parents);

		//.... Now find babysitters ....//
		$columns = array("COUNT(bs_id) as total","YEAR(created_at) as year");
		$where = array("status"=> 1,"utype"=> "b");
		$groupby = array("YEAR(created_at)");
		$orderby = "YEAR(created_at) DESC";
		$babysitters = $this->common_model->get_record("babysitter" ,$columns ,$where ,$orderby ,$groupby, $limit= 6, $skip ='', "");

		$totalBabysitters = 1;
		foreach($babysitters as $k=> $val){
			$totalBabysitters += $val->total;
		}//.... end of foreach() ....//
		//... find out percentage of each year ....//
		foreach($babysitters as $value){
			$value->percentage =(int) ($value->total *100/ $totalBabysitters);
		}
		$data['babysitters'] = array_reverse($babysitters);

		print json_encode($data);
	}//.... end of get_yearly_report() .....//

	/**
	 * Function for getting popular babysitters
	 */
	public function get_popular_babysitters()
	{
		$columns = array("bs_id","CONCAT(fname,' ',lname) as name","email","phone","likes","dislikes","COUNT(job_id) as project_done","SUM(attitude) as attitude",
				"SUM(punctuality) as punctuality");
		$where = array("babysitter.status"=> 1,"utype"=> "b","jobs.is_finished" => 1);
		$groupby = array("bs_id");
		$orderby = "project_done DESC";
		$joins = array(
				(0)=> array(
					"table"=> "jobs",
					'condition' => " jobs.babysitter_id = babysitter.bs_id",
					'jointype' => "inner"
				)
		);
		$this->db->group_by($groupby);
		$this->db->order_by($orderby);
		$babysitters = $this->common_model->get_join_record("babysitter", $columns,$joins, $where,$take=10,$skip='',$count='',$groupby,$orderby);

		return $babysitters;
	}//.... end of get_popular_babysitters() .....//
}//.... end of class....//