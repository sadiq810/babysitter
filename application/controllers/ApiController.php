<?php
/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 3/29/2016
 * Time: 3:14 PM
 */
class ApiController extends CI_Controller{

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library("Server", "server");
        $this->load->model("common_model");
        $this->load->model("user_model");
    }//.... end of __construct() ....//

    /**
     * Index method of the class.....//
     */
    public function index()
    {
        //$functionName=$_REQUEST['func_name'];
        //$params= $_REQUEST['paramsarray'];
        $functionName = $this->input->post_get("func_name");
        $params = $this->input->post_get("paramsarray");

        if(method_exists($this, $functionName)){
            $params = json_decode($params);
            $this->$functionName($params);
        }else{
            $data['data'] = "Invalid Function, Function not found!";
            $data['response'] = 0;
            print json_encode($data);
        }//.... end of if-else() ....//
    }//.... end of the index() ....//

    /**
     * function for logging in BabySitters.....
     */
    public function userLogin($params = '')
    {
        if(isset($params)){
            $user = $this->common_model->get_record("babysitter",array("*"),array('username' => $params->username, "password"=> md5($params->password),"approve_status"=> 1,'status'=> 1));
            if(!empty($user)){
                $username = $user[0]->username;
                $password = $user[0]->password;
                $result = $this->postCallOauth($username, $password);
                $result = json_decode($result);
                if(isset($result) && isset($result->access_token)){
                    $accessToken = $result->access_token;
                    $refreshToken = $result->refresh_token;
                    $expiresIn = date("Y-m-d H:i:s",time()+$result->expires_in);
                    $this->common_model->update_record("babysitter",array('accessToken'=> $accessToken,'refreshToken'=> $refreshToken,"expiresIn"=> $expiresIn,"is_online"=> 1),array('bs_id'=> $user[0]->bs_id));
                    $data['data'] = array(
                        'fname'             => $user[0]->fname,
                        'lname'             => $user[0]->lname,
                        'email'             => $user[0]->email,
                        'phone'             => $user[0]->phone,
                        'address'           =>$user[0]->address,
                        'about'             =>$user[0]->about,
                        'hourly_rate'       =>$user[0]->hourly_rate,
                        'from_time'         => $user[0]->from_time,
                        'to_time'           => $user[0]->to_time,
                        'working_days'      => explode("-",$user[0]->working_days),
                        'languages'         => explode("-",$user[0]->languages),
                        'img'               => (isset($user[0]->img) && $user[0]->img)? base_url().$user[0]->img:NULL,
                        'card_number'       => $user[0]->card_number,
                        'expiration_date'   =>$user[0]->expiration_date,
                        'utype'             => $user[0]->utype,
                        'accessToken'       => $accessToken,
                        'lat'               => $user[0]->lat,
                        'long'              => $user[0]->long,
                        'likes'             => $user[0]->likes,
                        'dislikes'          => $user[0]->dislikes
                    );

                    foreach($user as $babysitter){
                        $sumPunctuality = $this->common_model->get_record('jobs' ,array('SUM(punctuality) as punctuality') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', $count ='');
                        $totalPunctuality =(int) $this->common_model->get_record('jobs' ,array('punctuality') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', true);
                        $totalPunctuality = ($totalPunctuality <= 0)?1:$totalPunctuality;
                        $data['data']['punctuality'] =(int) $sumPunctuality[0]->punctuality/$totalPunctuality;

                        $sumAttitude = $this->common_model->get_record('jobs' ,array('SUM(attitude) as attitude') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', $count ='');
                        $totalAttitude =(int) $this->common_model->get_record('jobs' ,array('attitude') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', true);
                        $totalAttitude = ($totalAttitude<=0)?1:$totalAttitude;
                        $data['data']['attitude'] =(int) $sumAttitude[0]->attitude/$totalAttitude;
                    }//.... end of foreach() ....//

                    $data['response'] = 1;
                }else{
                    $data['data'] = "Access Token Error, Could not get access token";
                    $data['response'] = 0;
                }//.... end of if-else

            }else{
                $data['data'] = "Invalid User Credentials";
                $data['response'] = 0;
            }//.... end of if-else...//
        }else{
            $data['data'] = "Parameters are missing";
            $data['response'] = 0;
        }//.... end of if() .....//

        /*header('content-type:application/json');*/
        print json_encode($data);
    }//.... end of parentLogin() ....//

    /**
     * Function for user logout....
     */
    public function userLogOut($params)
    {
        if(isset($params) && $params->accessToken){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                $result =  $this->common_model->update_record("babysitter",array("is_online"=> 0),array("bs_id"=> $match[0]->bs_id));
                $data['data'] = "Sign Out Successfully !";
                $data['response'] = 1;
            }//.... end of inner if-else() ....//
        }else{
            $data['data'] = "Parameters Array OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        header('content-type:application/json');
        print json_encode($data);
    }//.... end of userLogOut() ....//

    /**
     * Function for User SignUp as a parent......//
     */
    public function userSignUp($params)
    {
        if(isset($params)){
            $where = array('username' => $params->username, 'status'=>1);
            $match = $this->user_model->checkExist_user("babysitter",'username', $where);
            $email = $this->user_model->checkExist_user("babysitter",'email', array('email' => $params->email, 'status'=>1));
            if(!empty($match) || !empty($email)) {
                $data['data'] = "Username OR Email already exist, please select different one";
                $data['response'] = 0;
            }else{
                $arr = array(
                    'fname'           => (isset($params->fname))?$params->fname:NULL,
                    'lname'           => (isset($params->lname))?$params->lname:NULL,
                    'email'           => (isset($params->email))?$params->email:NULL,
                    'username'        => (isset($params->username))?$params->username:NULL,
                    'phone'           => (isset($params->phone))?$params->phone:NULL,
                    'address'         => (isset($params->address))?$params->address:NULL,
                    'card_number'     => (isset($params->card_number))?$params->card_number:NULL,
                    'expiration_date' => (isset($params->expiration_date))? date('Y-m-d', strtotime($params->expiration_date)):NULL,
                    'pin_code'        => (isset($params->pin_code))? $params->pin_code:NULL,
                    'lat'             => (isset($params->lat))? $params->lat:NULL,
                    'long'            => (isset($params->long))? $params->long:NULL
                );

                if(!$params->lat && $params->address){
                    $latlong = $this->get_lat_long($params->address);
                    $arr['lat'] = $latlong[0];
                    $arr['long'] = $latlong[1];
                }//.... end of if() ...//

                if(isset($_FILES['img'])){
                    $path = time().$_FILES['img']['name'];
                    $path = 'uploads/'.$path;
                    $thumbsPath = 'uploads/thumbs/'.$path;
                    if(move_uploaded_file($_FILES['img']['tmp_name'],$path)) {
                        $res = $this->create_thumbs($path, $thumbsPath);
                        if($res == true)
                            $arr['img'] = $thumbsPath;
                        else
                            $arr['img'] = $path;
                    }//.... end of if() .....//
                };//.... end of file-uploading....//

                $digits = 4;
                $verificationCode =  rand(pow(10, $digits-1), pow(10, $digits)-1);
                $arr['verification_code'] = $verificationCode;
                $stts = $this->sendEmail($arr['email'], $verificationCode);

                $arr['password'] = md5($params->password);
                $arr['created_at'] = date('Y-m-d');
                $insertID =  $this->common_model->insert_record("babysitter",$arr);
                //.... now save notification for admin
                if($insertID){
                $this->common_model->insert_record("notifications",array("title"=>"New User Registered!","nfor"=>"admin","nsource"=> "p","from"=> $insertID,"description"=>"New user is registered, Please check his/her details information and Activate Account!",'created_at'=> date("Y-m-d H:i:s")));
                }//.... end of if() ....//

                $data['data'] = "Record Saved Successfully !";
                $data['response'] = 1;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters are missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        print json_encode($data);
    }//.... end of parentSignUp() ....//

    /**
     * Function for activation of parent-user...
     */
    public function activateUser($params)
    {
        if(isset($params)){
            $where = array('username' => $params->username, 'status'=> 1 ,'verification_code'=> $params->code,"approve_status"=> 0);
            $match = $this->user_model->checkExist_user("babysitter",array("*"), $where);
            if(empty($match)) {
                $data['data'] = "Sorry , record not found in our database....";
                $data['response'] = 0;
            }else{
                $arr = array(
                    "approve_status" => 1
                );
                $this->common_model->update_record("babysitter",$arr, array("bs_id"=> $match[0]->bs_id));
                $data['data'] = "Your Account Activated Successfully !";
                $data['response'] = 1;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters are missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        print json_encode($data);
    }//.... end of activateUser() .....//

    /**
     * function for sending emmail....
     */
    public function sendEmail($to, $verificationCode)
    {
        $this->load->library('email');
        $this->email->clear();
        $this->email->from('babysitter');
        $this->email->to("$to");
        $this->email->subject('Account Activation Code');
        $this->email->message('Your Account Activation Code: '.$verificationCode);
        $this->email->send();
        return;
    }//.... end of sendEmail() ....//

    /**
     * Function for parent to upgrade to babysitter account......//
     */
    public function userUpgrade($params)
    {
        if(isset($params)){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                $arr = array(
                    'about'             => (isset($params->about))?$params->about:NULL,
                    'hourly_rate'       => (isset($params->hourly_rate))? $params->hourly_rate:NULL,
                    'from_time'         => (isset($params->from_time))? $params->from_time:NULL,
                    'to_time'           => (isset($params->to_time))? $params->to_time:NULL,
                    'working_days'      => (isset($params->working_days))? implode("-",$params->working_days):NULL,
                    'languages'         => (isset($params->languages))? implode("-",$params->languages):NULL,
                    'modified_at'       => date("Y-m-d"),
                    'modified_by'       => $match[0]->bs_id,
                    "upgrade_request"   => 1
                );

                if(isset($params->fname))
                    $arr['fname'] = $params->fname;
                if(isset($params->lname))
                    $arr['lname'] = $params->lname;
                if(isset($params->email))
                    $arr['email'] = $params->email;
                if(isset($params->username))
                    $arr['username'] = $params->username;
                if(isset($params->phone))
                    $arr['phone'] = $params->phone;
                if(isset($params->address))
                    $arr['address'] = $params->address;
                if(isset($params->card_number))
                    $arr['card_number'] = $params->card_number;
                if(isset($params->pin_code))
                    $arr['pin_code'] = $params->pin_code;
                if(isset($params->expiration_date))
                    $arr['expiration_date'] = date('Y-m-d', strtotime($params->expiration_date));

                if(isset($_FILES['img'])){
                    $path = time().$_FILES['img']['name'];
                    $path = 'uploads/'.$path;
                    if(move_uploaded_file($_FILES['img']['tmp_name'],$path)) {
                        $arr['img'] = $path;
                    }
                };//.... end of file-uploading....//

                $insertID =  $this->common_model->update_record("babysitter",$arr, array('bs_id'=> $match[0]->bs_id));
                //.... now save notification for admin
                if($insertID){
                    $this->common_model->insert_record("notifications",array("title"=>"User Upgrade Request!","nfor"=>"admin","nsource"=> "p","from"=> $match[0]->bs_id,"description"=>"User Upgrade Request Received, Please check his/her details information and Activate Account!",'created_at'=> date("Y-m-d H:i:s")));
                }//.... end of if() ....//

                $data['data'] = "Record Saved Successfully !";
                $data['response'] = 1;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters are missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        header('content-type:application/json');
        print json_encode($data);
    }//.... end of babySitterSignUp() ....//

    /**
     * Function for babysitter Updating Profile......//
     */
    public function userUpdateProfile($params)
    {
        if(isset($params) && $params->accessToken){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{

                if(isset($params->username)) {
                    $arr['username'] = $params->username;
                    //.... check username if already taken..
                    $where = array('username' => $params->username, 'status' => 1, "bs_id !=" => $match[0]->bs_id);
                    $existUserName = $this->user_model->checkExist_user("babysitter", 'username', $where);
                    $email = $this->user_model->checkExist_user("babysitter",'email', array("bs_id !=" => $match[0]->bs_id,'email' => $params->email, 'status'=>1));
                }//.... end of if() ....//

                if (isset($existUserName) && !empty($existUserName) || isset($params->email) && !empty($email)) {
                    $data['data'] = "Username OR Email already exist, please select different one";
                    $data['response'] = 0;
                }else {
                        if(isset($params->fname))
                            $arr['fname'] = $params->fname;
                        if(isset($params->lname))
                            $arr['lname'] = $params->lname;
                        if(isset($params->email))
                            $arr['email'] = $params->email;
                        if(isset($params->phone))
                            $arr['phone'] = $params->phone;
                        if(isset($params->address)){
                            $arr['address'] = $params->address;
                                $latlong = $this->get_lat_long($params->address);
                                $arr['lat'] = $latlong[0];
                                $arr['long'] = $latlong[1];
                        }//.... end of if() ....//

                        if(isset($params->about))
                            $arr['about'] = $params->about;
                        if(isset($params->card_number))
                            $arr['card_number'] = $params->card_number;
                        if(isset($params->pin_code))
                            $arr['pin_code'] = $params->pin_code;
                        if(isset($params->expiration_date))
                            $arr['expiration_date'] = date('Y-m-d', strtotime($params->expiration_date));
                        if(isset($params->hourly_rate))
                            $arr['hourly_rate'] = $params->hourly_rate;
                        if(isset($params->password))
                        $arr['password'] = md5($params->password);

                        if(isset($params->from_time))
                            $arr['from_time'] = date("H:i:s", strtotime($params->from_time));
                        if(isset($params->to_time))
                            $arr['to_time'] = date("H:i:s", strtotime($params->to_time));
                        if(isset($params->working_days))
                            $arr['working_days'] = implode("-",$params->working_days);
                    if(isset($params->languages))
                            $arr['languages'] = implode("-",$params->languages);
                        if(isset($params->lat))
                            $arr['lat'] = $params->lat;
                        if(isset($params->long))
                            $arr['long'] = $params->long;

                        if(isset($_FILES['img'])){
                            $path = time().$_FILES['img']['name'];
                            $path = 'uploads/'.$path;
                            $thumbsPath = 'uploads/thumbs/'.$path;
                            if(move_uploaded_file($_FILES['img']['tmp_name'],$path)) {
                                $res = $this->create_thumbs($path, $thumbsPath);
                                if($res == true)
                                    $arr['img'] = $thumbsPath;
                                else
                                    $arr['img'] = $path;
                            }//.... end of if() ....//
                        };//.... end of file-uploading....//

                        $arr['modified_at'] = date('Y-m-d');
                        $arr['modified_by'] = $match[0]->bs_id;

                    $result =  $this->common_model->update_record("babysitter",$arr,array("bs_id"=> $match[0]->bs_id));
                    $columns = array("*");
                    $user = $this->common_model->get_record("babysitter", $columns,array("bs_id"=> $match[0]->bs_id));

                    $data['data'] = array(
                        'fname'             => $user[0]->fname,
                        'lname'             => $user[0]->lname,
                        'email'             => $user[0]->email,
                        'phone'             => $user[0]->phone,
                        'address'           =>$user[0]->address,
                        'about'             =>$user[0]->about,
                        'hourly_rate'       =>$user[0]->hourly_rate,
                        'from_time'         => $user[0]->from_time,
                        'to_time'           => $user[0]->to_time,
                        'working_days'      => explode("-",$user[0]->working_days),
                        'languages'         => explode("-",$user[0]->languages),
                        'img'               => (isset($user[0]->img) && $user[0]->img)?base_url().$user[0]->img:NULL,
                        'card_number'       => $user[0]->card_number,
                        'expiration_date'   =>$user[0]->expiration_date,
                        'utype'             => $user[0]->utype,
                        'accessToken'       => $params->accessToken,
                        'lat'               => $user[0]->lat,
                        'long'              => $user[0]->long,
                        'likes'             => $user[0]->likes,
                        'dislikes'          => $user[0]->dislikes
                    );

                    foreach($user as $babysitter){
                        $sumPunctuality = $this->common_model->get_record('jobs' ,array('SUM(punctuality) as punctuality') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', $count ='');
                        $totalPunctuality =(int) $this->common_model->get_record('jobs' ,array('punctuality') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', true);
                        $totalPunctuality = ($totalPunctuality <= 0)?1:$totalPunctuality;
                        $data['data']['punctuality'] =(int) $sumPunctuality[0]->punctuality/$totalPunctuality;

                        $sumAttitude = $this->common_model->get_record('jobs' ,array('SUM(attitude) as attitude') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', $count ='');
                        $totalAttitude =(int) $this->common_model->get_record('jobs' ,array('attitude') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', true);
                        $totalAttitude = ($totalAttitude<=0)?1:$totalAttitude;
                        $data['data']['attitude'] =(int) $sumAttitude[0]->attitude/$totalAttitude;
                    }//.... end of foreach() ....//

                    $data['response'] = 1;
                }
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        header('content-type:application/json');
        print json_encode($data);
    }//.... end of babySitterUpdateProfile() ....//

    /**
     * Function for changing password ......
     */
    public function changePassword($params)
    {
        if(isset($params->accessToken) && $params->accessToken){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                if(isset($params->oldPass) && isset($params->newPass)){

                    $babysitters = $this->common_model->get_record("babysitter", array("password"), array("bs_id"=> $match[0]->bs_id));
                    $oldPassword = md5($params->oldPass);
                    if($oldPassword == $babysitters[0]->password){
                        $this->common_model->update_record("babysitter", array("password"=> md5($params->newPass)), array("bs_id"=> $match[0]->bs_id));
                        $data['data'] = "Password changed successfully!";
                        $data['response'] = 1;
                    }else{
                        $data['data'] = "Old Password not matched!";
                        $data['response'] = 0;
                    }//.... end of inner if-else() .....//
                }else{
                    $data['data'] = "Old Password Or New Password Not Provided!";
                    $data['response'] = 0;
                }
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//
        print json_encode($data);
    }//.... end of changePassword() .....//

    /**
     * Function for Selecting Babysitters for a job, based on the time of the babysitter and parent search......
     */
    public function getBabysitterForJob($params)
    {
        if(isset($params->accessToken) && $params->accessToken){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{

                $where = array();
                if(isset($params->start_date))
                    $where['from_time >= '] = date("H:i:s",strtotime($params->start_date));
                if(isset($params->end_date))
                    $where["to_time <= "] = date("H:i:s", strtotime($params->end_date));

                $columns = array("bs_id as babysitter_id","fname","lname","email","phone","address","hourly_rate","img",'lat',"long",'likes',"dislikes","from_time",
                    "to_time","working_days","languages");

                if(isset($params->location))
                    $this->db->like("address",$params->location, "both");

                $babysitters = $this->common_model->get_record("babysitter", $columns, $where);

                foreach($babysitters as $b){
                    $b->working_days = explode("-",$b->working_days);
                    $b->languages = explode("-",$b->languages);
                    $b->img = ($b->img)?base_url().$b->img:NULL;
                }//.... end of foreach() ....//

                $data['data'] = $babysitters;
                $data['response'] = 1;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        /*header('content-type:application/json');*/
        print json_encode($data);
    }//.... End of createJob() .....//

    /**
     * Function for awarding a specific job to specific babysitter.......
     * Required fields in $params are.....
     * accessToken, utype, ApplicationID
     * ApplicationID is the id of the applications table
     */
    public function awardJobTo($params)
    {
        if(isset($params->accessToken) && $params->accessToken && $params->babysitter_id){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                //.... assign job to babysitter and inform him/her.

                $parentInfo = $this->common_model->get_record("babysitter",array("address","lat","long"), array('bs_id'=> $match[0]->bs_id));
                $babysitterInfo = $this->common_model->get_record("babysitter",array("hourly_rate"), array('bs_id'=> $params->babysitter_id));

                $columns = array(
                    "parent_id"     => $match[0]->bs_id,
                    "pstart_time"   => (isset($params->start_date))? date("Y-m-d H:i:s", strtotime($params->start_date)):NULL,
                    "pend_time"     =>  (isset($params->end_date))? date("Y-m-d H:i:s", strtotime($params->end_date)):NULL,
                    "babysitter_id" => $params->babysitter_id,
                    "location"      => $parentInfo[0]->address,
                    "lat"           => $parentInfo[0]->lat,
                    "long"          => $parentInfo[0]->long,
                    'hourly_cost'   => $babysitterInfo[0]->hourly_rate,
                    'created_at'    => date('Y-m-d'),
                    'created_by'    => $match[0]->bs_id
                );
                $stats = $this->common_model->insert_record('jobs',$columns);
                if($stats){
                    $notify = array(
                        'title'=> "New Job Request !",
                        'message'=> "You have got Job Request, Accept/Reject it",
                        'ticker' => "New Job Request !",
                        'user_type'=> 'b',
                        'uid' => $params->babysitter_id
                    );
                    $this->sendNotifications($notify);
                    $data['data'] = "Record Saved Successfully!";
                    $data['response'] = 1;
                }else{
                    $data['data'] = "Error Occurred, Your are not the publisher of this job.";
                    $data['response'] = 0;
                }//.... end of if() ....//

            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array, babysitter_id OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        print json_encode($data);
    }//.... end of awardJobTo() .....//

    /**
     * Function for getting list of jobs of babysitter......
     * required parameters. AccessToken
     */
    public function getJobs($params)
    {
        if(isset($params->accessToken) && $params->accessToken){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                $columns = array("job_id","pstart_time as start_date","pend_time as end_date","location","lat","long","is_finished","is_accepted");
                $this->db->order_by("is_finished");
                $jobs = $this->common_model->get_record("jobs",$columns,array("babysitter_id"=> $match[0]->bs_id,"is_accepted !=" => 2,"jobs.status"=> 1));
                foreach($jobs as $job){
                    if($job->is_finished == 1){
                        $job->jobStatus = "COMPLETED";
                    }elseif($job->is_accepted == 0){
                        $job->jobStatus = "PENDING";
                    }elseif($job->is_accepted == 1){
                        $job->jobStatus = "ACCEPTED";
                    }else{
                        $job->jobStatus = "REJECTED";
                    }
                    unset($job->is_accepted);
                    unset($job->is_finished);
                }//.... end of foreach() .....//

                $data['data'] = $jobs;
                $data['response'] = 1;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        print json_encode($data);
    }//.... end of getJobs() ....//

    /**
     * Function for saving data for babysitters , who Accept Or Reject a job.....
     * Required Parameters: AccessToken, job_id, status = 1 means accepted, 0 means rejected.
     */
    public function AcceptOrRejectJob($params)
    {
        if(isset($params->accessToken) && $params->accessToken && isset($params->job_id)){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                $parentInfo = $this->common_model->get_record("jobs",array("parent_id"),array("job_id"=> $params->job_id));

                $where = array(
                    'job_id' => $params->job_id,
                );
                $columns = array('feedback' => (isset($params->feedback))? $params->feedback:NULL);
                $status = (isset($params->status))?$params->status:1;
                if($status == 1){
                    $columns['is_accepted'] = 1;
                    $notify = array(
                        'title'=> "Your Job Request is Accepted",
                        'message'=> "Your Job Request is Accepted by babysitter.",
                        'ticker' => "Your job request accepted.",
                        'user_type'=> 'b',
                        'uid' => $parentInfo[0]->parent_id
                    );
                }else{
                    $notify = array(
                        'title'=> "Your Job Request Rejected",
                        'message'=> "Your Job Request is rejected by babysitter",
                        'ticker' => "Your Job Request Rejected!",
                        'user_type'=> 'b',
                        'uid' => $parentInfo[0]->parent_id
                    );
                    $columns['is_accepted'] = 2;
                }//.... end of if-else ....//

                $status = $this->common_model->update_record("jobs",$columns,$where);

                if($status){
                    $this->sendNotifications($notify);
                    $data['data'] = "Record Saved Successfully!";
                    $data['response'] = 1;
                }else{
                    $data['data'] = "Unable to save record. Please Try later.";
                    $data['response'] = 0;
                }//..... end of if-else....//

            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array, JobID OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        print json_encode($data);
    }//.... end of applyForJob() .....//

    /**
     * Function for getting jobs, published by a specific parent.....
     * It get jobs of the parents' published....
     */
    public function getMyJobs($params)
    {
        if(isset($params->accessToken) && $params->accessToken){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                $columns = array("job_id","jobs.pstart_time as start_date","jobs.pend_time as end_date","babysitter.address","babysitter.lat","babysitter.long","jobs.is_finished","jobs.is_accepted",
                    "CONCAT(fname,' ',lname) as name");
                $joins = array(
                    (0)=> array(
                        'table'=> "babysitter",
                        'condition'=> "jobs.babysitter_id = babysitter.bs_id",
                        'jointype' => "left"
                    )
                );
                $this->db->order_by("is_finished");
                $jobs = $this->common_model->get_join_record("jobs",$columns,$joins,array("parent_id"=> $match[0]->bs_id,"is_accepted !=" => 2,"jobs.status"=> 1));
                foreach($jobs as $job){
                    if($job->is_finished == 1){
                        $job->jobStatus = "COMPLETED";
                    }elseif($job->is_accepted == 0){
                        $job->jobStatus = "PENDING";
                    }elseif($job->is_accepted == 1){
                        $job->jobStatus = "PROGRESS";
                    }else{
                        $job->jobStatus = "REJECTED";
                    }
                    unset($job->is_accepted);
                    unset($job->is_finished);
                }//.... end of foreach() .....//

                $data['data'] = $jobs;
                $data['response'] = 1;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        header('content-type:application/json');
        print json_encode($data);
    }//..... end of getMyJobs() .....//

    /**
     * Function for getting Applicants for a specific job..........
     */
    public function getApplicants11111($params)
    {
        if(isset($params->accessToken) && $params->accessToken && $params->job_id){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                $columns = array(
                    "CONCAT(babysitter.fname,' ',babysitter.lname) as name","babysitter.bs_id","babysitter.email","babysitter.phone",
                    "babysitter.address","babysitter.hourly_rate","babysitter.from_time","babysitter.to_time","babysitter.working_days",
                    "babysitter.likes","babysitter.dislikes","babysitter.lat","babysitter.long","babysitter.about","babysitter.img",
                    "job_applications.ja_id as ApplicationID"
                );
                $joins = array(
                    (0)=> array(
                        'table' => 'job_applications',
                        'condition'=> 'babysitter.bs_id = job_applications.bs_id',
                        'jointype' => 'inner'
                    )
                );

                $where = array("job_applications.status"=> 1, "job_applications.job_id"=> $params->job_id);
                $applicants = $this->common_model->get_join_record("babysitter",$columns,$joins,$where);

                foreach($applicants as $babysitter){
                    if(isset($babysitter->img) && $babysitter->img){
                        $babysitter->img = base_url().$babysitter->img;
                    }//.... end of if() ....//

                    $babysitter->working_days = explode("-", $babysitter->working_days);

                    $sumPunctuality = $this->common_model->get_record('jobs' ,array('SUM(punctuality) as punctuality') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', $count ='');
                    $totalPunctuality = $this->common_model->get_record('jobs' ,array('punctuality') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', true);
                    $babysitter->punctuality =(int) $sumPunctuality[0]->punctuality/$totalPunctuality;

                    $sumAttitude = $this->common_model->get_record('jobs' ,array('SUM(attitude) as attitude') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', $count ='');
                    $totalAttitude = $this->common_model->get_record('jobs' ,array('attitude') ,array('babysitter_id'=> $babysitter->bs_id) ,$orderby='' ,$groupby='', $limit='', $skip ='', true);
                    $babysitter->attitude =(int) $sumAttitude[0]->attitude/$totalAttitude;

                    unset($babysitter->bs_id);
                }//.... end of foreach() ....//

                $data['data'] = $applicants;
                $data['response'] = 1;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array, JobID OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        header('content-type:application/json');
        print json_encode($data);
    }//.... end of getApplicants() .....//



    /**
     * Function for getting awarded jobs of the Babysitters.Or Jobs Assigned to a particular Babysitter....
     * Required values for $params are
     * accessToken, userType
     */
    public function getBabySitterJobs1111($params)
    {
        if(isset($params->accessToken) && $params->accessToken){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                $columns = array(
                    'jobs.job_id','jobs.start_date','jobs.start_time','jobs.duration','jobs.no_childern','jobs.ages_childern',
                    'jobs.child_qualities','jobs.responsibilities','jobs.location','jobs.rate','jobs.job_title',
                    'jobs.job_description','jobs.created_at as posted_on','jobs.status',
                    'CONCAT(babysitter.fname," ",babysitter.lname) as AssignedTo'
                );
                $joins = array(
                    (0)=> array(
                        'table' => 'babysitter',
                        'condition'=> 'babysitter.bs_id = jobs.babysitter_id',
                        'jointype' => 'inner'
                    )
                );
                $this->db->order_by("jobs.job_id");
                $jobs = $this->common_model->get_join_record("jobs",$columns,$joins,array("jobs.status"=> 1,"jobs.babysitter_id"=> $match[0]->bs_id));

                foreach($jobs as $job){
                    if($job->status == 1)
                        $job->JobStatus = "OPEN";
                    elseif($job->status == 0)
                        $job->JobStatus = "CLOSED";
                    unset($job->status);
                    //.... check clockin if active......
                    $job->clockIn = NULL;
                    $clockInStatus = $this->common_model->get_record("babysitter_activities",array("bsa_id","clock_in"), array("job_id"=> $job->job_id, "bs_id" => $match[0]->bs_id,'is_completed'=> 0));
                    if(!empty($clockInStatus)){
                        $job->clockIn = $clockInStatus[0]->clock_in;
                    }//.... end of if() ....//
                }//.... end of foreach() ....//

                $data['data'] = $jobs;
                $data['response'] = 1;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        header('content-type:application/json');
        print json_encode($data);
    }//.... end of getBabySitterJobs() ....//

    /**
     * function for storing babySitter ClockIn activity.....
     * * Required Fields in $params are..
     * AccessToken, job_id
     */
    public function babySitterClockIn($params)
    {
        if(isset($params->accessToken) && $params->accessToken && isset($params->job_id)){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                $columns = array(
                    "clock_in" => date('Y-m-d H:i:s'),
                );
                $status = $this->common_model->update_record("jobs",$columns, array("job_id" => $params->job_id));
                if($status){
                    $data['data'] = "Record Save Successfully!";
                    $data['response'] = 1;
                }else{
                    $data['data'] = "Error Occurred, Please Try Later.";
                    $data['response'] = 0;
                }
            }//.... end of inner if-else
        }else{
            $data['data'] = "Parameters Array, JobID OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        print json_encode($data);
    }//.... end of babySitterClockIn() .....//

    /**
     * Function for saving babysitter completed activities against a specific job....
     * Required Fields in $params are..
     * AccessToken, utype, job_id
     */
    public function babySitterClockOut($params)
    {
        if(isset($params->accessToken) && $params->accessToken && isset($params->job_id)){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{

                $jobDetails = $this->common_model->get_record("jobs",array("hourly_cost","clock_in"), array("job_id"=> $params->job_id));
                $hourlyCost =(int) (isset($jobDetails[0]->hourly_cost))?$jobDetails[0]->hourly_cost:1;


                $datetime1 = new DateTime($jobDetails[0]->clock_in);
                $clock_out = date('Y-m-d H:i:s');
                $datetime2 = new DateTime($clock_out);
                $difference = $datetime1->diff($datetime2);
                $hours =(float) $difference->format("%H.%I.%S");
                $totalHourse = $hours + ($difference->days*24);

                $columns = array(
                    "clock_out" => date('Y-m-d H:i:s',strtotime($clock_out)),
                    'is_finished'=> 1,
                    "total_cost" => $totalHourse * $hourlyCost,
                    "total_hours" => $totalHourse
                );

                $status = $this->common_model->update_record("jobs",$columns, array("job_id"=> $params->job_id));

                $data['data'] = "Record Save Successfully!";
                $data['response'] = 1;
                //.... get non-completed record of this job.....

            }//.... end of inner if-else ....//
        }else{
            $data['data'] = "Parameters Array, JobID OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        print json_encode($data);
    }//.... End of saveBabySitterActivity() ....//

    /**
     * Function for saving parent reviews for babySitters.....
     * Required Params.
     * AccessToken, utype, job_id....
     */
    public function addReviews($params)
    {
        if(isset($params->accessToken) && $params->accessToken && isset($params->job_id)){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                //.... Check whether job is assigned to someone or not.....
                $checkIfAssigned = $this->common_model->get_record("jobs",array("job_id"), array("job_id"=> $params->job_id, "parent_id" => $match[0]->bs_id,"babysitter_id !="=> NULL));

                if(empty($checkIfAssigned)){
                    $data['data'] = "You can't Add Review for this job, because it's not assigned to anyone. OR You didn't publish it.";
                    $data['response'] = 0;
                }else{
                    $columns = array(
                        "reviews"  => (isset($params->review))?$params->review:NULL,
                    );
                    $status = $this->common_model->update_record("jobs",$columns, array("job_id"=> $params->job_id));
                    if($status){
                        $data['data'] = "Record Save Successfully!";
                        $data['response'] = 1;
                    }else{
                        $data['data'] = "Error Occurred, Please Try Later.";
                        $data['response'] = 0;
                    }//.... end of if-else() ....//
                }//.... end of if-else() ....//
            }//.... end of inner if-else ....//
        }else{
            $data['data'] = "Parameters Array, job_id OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        header('content-type:application/json');
        print json_encode($data);
    }//.... end of addReviews() ....//

    /**
     * Function for rating a baby sitter using a specific job, babysitter may be rated as like or dislike.....
     * Required args in Params.
     * accessToken,attitude,punctuality, job_id, likes....
     * likes = 1 means like,
     * likes = -1 means dislike,
     *
     */
    public function rateBabySitter($params)
    {
        if(isset($params->accessToken) && $params->accessToken && $params->job_id){
            $match = $this->checkAndValidateUser($params->accessToken);
            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                //.... get babysitter of specific job.....
                $babysitter_id = $this->common_model->get_record("jobs",array("babysitter_id"), array("job_id"=> $params->job_id, "parent_id" => $match[0]->bs_id));

                if(empty($babysitter_id)){
                    $data['data'] = "You can't rate, because it's not assigned to anyone. OR You didn't publish it.";
                    $data['response'] = 0;
                }else{

                    $columns = array();
                    if(isset($params->attitude))
                        $columns['attitude'] = $params->attitude;
                    if(isset($params->punctuality))
                        $columns['punctuality'] = $params->punctuality;
                    if(!empty($columns))
                        $status = $this->common_model->update_record("jobs",$columns, array("job_id"=> $params->job_id));

                    if(isset($params->likes)){
                        $rating = $params->likes;
                        $babysitter = $this->common_model->get_record("babysitter",array("likes","dislikes"), array("bs_id"=> $babysitter_id[0]->babysitter_id));
                        if($rating == 1){
                            $column = array('likes'=> $babysitter[0]->likes+1);
                        }else{
                            $column = array('dislikes'=> $babysitter[0]->dislikes+1);
                        }
                        $status = $this->common_model->update_record("babysitter",$column, array("bs_id"=> $babysitter_id[0]->babysitter_id));
                    }//.... end of if() ....//

                    if($status){
                        $data['data'] = "Record Save Successfully!";
                        $data['response'] = 1;
                    }else{
                        $data['data'] = "Error Occurred, Please Try Later.";
                        $data['response'] = 0;
                    }//.... end of if-else() ....//
                }//.... end of if-else() ....//
            }//.... end of inner if-else ....//
        }else{
            $data['data'] = "Parameters Array, job_id OR Access Token missing";
            $data['response'] = 0;
        }//.... end of if-else() ....//

        header('content-type:application/json');
        print json_encode($data);
    }//.... end of rateBabySitter() ....//

    /**
     * Function for validating user by passing its accessToken and usertype.....
     */
    private function checkAndValidateUser($accessToken, $UType='')
    {
        $where = array('accessToken' => $accessToken, "status"=> 1);
        $match = $this->common_model->get_record("babysitter",array("bs_id","utype","refreshToken","expiresIn"), $where);
        if(!empty($match) && date("Y-m-d") > date("Y-m-d", strtotime($match[0]->expiresIn))){
            $data['data'] = "Access Token is Expired";
            $data['refreshToken'] = $match[0]->refreshToken;
            $data['response'] = 0;
            print json_encode($data);
            exit(1);
        }else{
            return $match;
        }//.... end of if-else() ....//
    }//.... end of checkAndValidateUser() ....//

    /**
     * Function for reNewing access Token....
     * Params Required: refreshToken
     */
    public function renewToken($params)
    {
        if(isset($params->refreshToken)){
            $where = array('refreshToken' => $params->refreshToken, "status"=> 1);
            $match = $this->common_model->get_record("babysitter",array("bs_id","accessToken","refreshToken","expiresIn"), $where);
            if(!empty($match)){
                $result = $this->postCallOauth("","",$params->refreshToken);
                $result = json_decode($result);
                if(isset($result) && isset($result->access_token)){
                    $accessToken = $result->access_token;
                    $refreshToken = $result->refresh_token;
                    $expiresIn = date("Y-m-d H:i:s",time()+$result->expires_in);
                    $this->common_model->update_record("babysitter",array('accessToken'=> $accessToken,'refreshToken'=> $refreshToken,"expiresIn"=> $expiresIn),array('bs_id'=> $match[0]->bs_id));
                    $data['data'] = array('accessToken'=> $accessToken);
                    $data['response'] = 1;
                }else{
                    $data['data'] = "Access Token Error, Could not renew access token";
                    $data['response'] = 0;
                }//.... end of if-else
            }else{
                $data['data'] = "Invalid Refresh Token";
                $data['response'] = 0;
            }//.... end of inner if-else
        }else{
            $data['data'] = "Refresh Token is not provided";
            $data['response'] = 0;
        }//.... end of if-else ....//

        print json_encode($data);
    }//.... end of renewToken() ....//

    /**
     * Function for getting all baby sitters and notify them about the job.....
     */
    private function getAndNotifyAllBabySitters1111($job_id = '')
    {
        $jobDetails = $this->common_model->get_record("jobs",array('job_title','job_description'),array("job_id"=> $job_id));
        $data = array(
            'job_id'=> $job_id,
            'title'=> $jobDetails[0]->job_title,
            'message'=> $jobDetails[0]->job_description,
            'ticker' => "New Job Posted.",
            'user_type'=> 'b'
        );

        $babysitters = $this->common_model->get_record('babysitter',array("bs_id as user_id"),array('approve_status'=> 1, 'status'=> 1,'utype'=> 'b'));
        foreach($babysitters as $babysitter){
            $data['uid'] = $babysitter->user_id;
            $this->sendNotifications($data);
        }
        return;
    }//.... end of getAndNotifyAllBabySitters() ....//

    /**
     * function for registering devices.....//
     */
    public function registerDevice($params)
    {
        if(isset($params->devid) && $params->accessToken && $params->devid && $params->devtype)
        {
            $where = array('accessToken' => $params->accessToken);
            $match = $this->common_model->get_record("babysitter",array("bs_id as user_id"), $where);

            if(empty($match))
            {
                $data['data'] = "Sorry, Invalid Access Token";
                $data['response'] = 0;
            }else{
                //.... check exist record.....//
                $existDevice = $this->common_model->get_record("registered_devices",array("rd_id"),array("user_id"=> $match[0]->user_id));
                $column = array('dev_id' => $params->devid, "user_id"=> $match[0]->user_id, "device_type"=> $params->devtype);
                if(empty($existDevice))
                    $result = $this->common_model->insert_record("registered_devices",$column);
                else
                    $result = $this->common_model->update_record("registered_devices",$column, array('rd_id'=> $existDevice[0]->rd_id));
                if($result)
                    $data = array('response'=> 1,'data'=> "Record Saved Successfully");
                else
                    $data = array('response'=> 0,'data'=> "Error Occurred, Please Try later...");
            }//.... end of inner if-else .....//

        }else{
            $data = array('response'=> 0,'data'=> "Device ID ,Device type OR AccessToken is missing..");
        }//.... end of outer if-else ....//

        header('content-type:application/json');
        print json_encode($data);
    }//.... end of registerDevice() ....//

    /**
     * This make a post call to OAuth Library to get Access Token.....
     */
    public function postCallOauth($username, $password,$refreshToken = "")
    {
        if($refreshToken){
            $url = base_url().'index.php/apiController/refreshToken';
            $data_string = "grant_type=refresh_token&refresh_token=$refreshToken&client_id=cs-003&client_secret=csadmin";
        }else{
            $url = base_url().'index.php/apiController/get_accessToken';
            $data_string = "grant_type=password&username=$username&password=$password&client_id=cs-003&client_secret=csadmin";
        }//.... end of if-else.....//
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }//.... end of postCallOauth() .....//

    /**
     * @param $dataobj
     * @return bool
     * Function for sending Notifications to end users......
     */
    private function sendNotifications($dataobj)
    {
        $uid = $dataobj['uid'];
        //print_r($dataobj);
        $message =  $dataobj['ticker'];
        $regArray = array();
        $iosArray = array();
        $apiKey = "AIzaSyBph-O_Fe5Ma_pbVRfnT4Ftx2TQ7cYmvmA";
        $RegID = $this->common_model->get_record('registered_devices',array('*'),array('user_id'=> $uid));

        foreach($RegID as $id)
        {
            if($id->device_type == 'android')
                $regArray[] = $id->dev_id;
            else
                $iosArray[] = $id->dev_id;
        }

        if(count($regArray) > 0){

            $url = 'https://android.googleapis.com/gcm/send';
            $fields = array(
                'registration_ids' => $regArray,
                'data' => array("title"=>$dataobj['title'] , "message" => $dataobj['message'], "ticker"=>$dataobj['ticker'] ),
            );
            $headers = array(
                'Authorization: key=' . $apiKey,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            //curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //     curl_setopt($ch, CURLOPT_POST, true);
            //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $fields ));

            // Execute post
            $result = curl_exec($ch);

            // Close connection
            curl_close($ch);
            //echo $result;
        }

        if(count($iosArray) > 0){
            foreach($iosArray as $device){
                // Put your device token here (without spaces):
                $deviceToken = $device;

                // Put your private key's passphrase here:
                $passphrase = 'RouteMe';

                // Put your alert message here:
                $message = $dataobj['message'];

                ////////////////////////////////////////////////////////////////////////////////

                $ctx = stream_context_create();
                stream_context_set_option($ctx, 'ssl', 'local_cert', 'assets/ck.pem');
                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                // Open a connection to the APNS server
                $fp = stream_socket_client(
                    'ssl://gateway.sandbox.push.apple.com:2195', $err,
                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

                if (!$fp)
                    return false;

                //echo 'Connected to APNS' . PHP_EOL;

                // Create the payload body
                $body['aps'] = array(
                    'alert' => array("title"=>$dataobj['title'],"body"=>$dataobj['ticker']),
                    'sound' => 'bingbong.aiff'
                );
                $body['message'] = $message;
                // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));

                /*if (!$result)
                    echo 'Message not delivered' . PHP_EOL;
                else
                    echo 'Message successfully delivered' . PHP_EOL;
                */
                // Close the connection to the server
                fclose($fp);
            }
        }
    }//.... End of sendNotifications() ....//

    /**
     * Function for getting Access Token....
     */
    public function get_accessToken()
    {
        $this->server->password_credentials();
    }//..... end of get_token() ....//

    /**
     * Function for refreshing Token, using refresh_token
     */
    public function refreshToken()
    {
        $this->server->refresh_token();
    }//.... end of refreshToken() ....//

    /**
     * Function for getting Latitude and Longtitude of the address...
     */
    public function get_lat_long($address)
    {
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key=AIzaSyCj-CYOUDIH7bWsgEHRuWI2vbwn4R9jhkM&sensor=false');
        $geo = json_decode($geo, true);
        $latitude = (isset($geo['results'][0]['geometry']['location']['lat']))?$geo['results'][0]['geometry']['location']['lat']:null;
        $longitude = (isset($geo['results'][0]['geometry']['location']['lng']))?$geo['results'][0]['geometry']['location']['lng']:null;
        return array($latitude,$longitude);
    }//.... End of function() ....//

    /**
     * Function for creating thumbs of the images....
     */
    protected function create_thumbs($path="", $thumbsPath="")
    {
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $path,
            'new_image' => $thumbsPath,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 100,
            'height' => 100
        );
        $this->load->library('image_lib', $config_manip);
        if($this->image_lib->resize()){
            $this->image_lib->clear();
            return true;
        }else{
            return false;
        }//.... end of if-else() .....//
    }//.... end of create_thumbs() ....//
}//.... end of class ....//