<?php

class Babysitter extends MY_Controller
{
	private $table = "babysitter";
	 /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/babysitter';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->model("user_model");
    }//... end of __construct() ...//

	/**
	 * Function for loading form for adding new record....
	 */
	function load_form()
	{
		$this->load->view('admin/babysitter/add');
	}//.... end of load_form() .....//

	/**
	 * function for loading all list of baby sitters....
	 */
	function list_record()
	{
		$this->load->view('admin/babysitter/babysitterslist');
	}//.... end of list_record() .....//

	/**
	 * function for approving parent....
	 */
	public function approve_babbysitter($bs_id = '')
	{
		$where = array('bs_id' => $bs_id);
		$columns = array('utype' => 'b');
		$status = $this->common_model->update_record($this->table, $columns, $where);
		echo $status;
	}//.... end of approve_parent() ....//

	/**
	 * function for editing a baby sitter...
	 */
	public function edit($bs_id = '')
	{
		$where = array('bs_id' => $bs_id);
		$column = array('*');
		$data["record"] = $this->common_model->get_record($this->table ,$column ,$where);
		$this->load->view('admin/babysitter/add',$data);
	}//.... end of edit() ....//

	/**
	 * function for deleting a baby sitter
	 */
	public function delete($bs_id = "")
	{
		$where = array('bs_id' => $bs_id);
		$columns = array('status' => 0);
		$status = $this->user_model->delete_record($this->table, $columns, $where);
		redirect("babysitters");
	}//.... end of delete() ....//

	/**
	 * Function for loading details page of a babysitter....
	 */
	public function view_details($babysitter_id = '')
	{
		$data['babysitter'] = $this->common_model->get_record($this->table,array("bs_id","fname","lname","address","phone","lat","long"," hourly_rate as sitter_cost","about","img","likes","dislikes",'working_days','languages',"from_time","to_time"), array("bs_id"=> $babysitter_id));
		if(!$data['babysitter'][0]->lat){
			$latLong = $this->get_lat_long($data['babysitter'][0]->address);
			$data['babysitter'][0]->lat = $latLong[0];
			$data['babysitter'][0]->long = $latLong[1];
			$this->common_model->update_record($this->table,array('lat'=> $latLong[0],'long'=> $latLong[1]),array('bs_id'=> $data['babysitter'][0]->bs_id));
		}//.... end of if() ....//

		$joins = array(
				(0)=> array(
					'table'=> 'jobs',
					'condition'=> 'jobs.parent_id = babysitter.bs_id',
					'jointype'=> 'inner'
				)
		);
		$where = array('jobs.babysitter_id'=> $babysitter_id,"is_finished"=> 1);
		$orderby = "";
		$this->db->order_by('jobs.job_id',"desc");
		$this->db->group_by('jobs.parent_id');
		$data['clients'] = $this->common_model->get_join_record($this->table,array('fname',"lname","img"),$joins, $where,$take=12,$skip='',$count='',$groupby='',$orderby);
		$data['jobs'] = $this->common_model->get_join_record("babysitter",array('reviews','fname',"lname","pstart_time","pend_time","total_cost","total_hours","is_paid"), $joins,array('babysitter_id'=> $babysitter_id,"is_finished"=> 1));
		$data['totalEarnings'] = $this->common_model->get_join_record("jobs",array("sum(total_cost) as totalEarnings"), $joins='',array('babysitter_id'=> $babysitter_id,"is_finished"=> 1));
		$data['balance'] = $this->common_model->get_join_record("jobs",array("sum(total_cost) as balance"), $joins='',array('babysitter_id'=> $babysitter_id,"is_finished"=> 1, "is_paid"=> 0));
		$data['totalHours'] = $this->common_model->get_join_record("jobs",array("sum(total_hours) as tHours"), $joins='',array('babysitter_id'=> $babysitter_id,"is_finished"=> 1));
		$status = $this->common_model->get_join_record("jobs",array("punctuality","attitude"), $joins='',array('babysitter_id'=> $babysitter_id,"is_finished"=> 1));
		$total = (count($status))?count($status):1;
		$punctuality = 0;
		$attitude = 0;
		foreach ($status as $item) {
			$punctuality += $item->punctuality;
			$attitude  += $item->attitude;
		}//.... end of foreach() ....//

		$data['punctuality'] = ($punctuality/$total );
		$data['attitude'] = ($attitude/$total );

		$data['CompletedJobs'] = $this->common_model->get_record("jobs",array("job_id"), array('babysitter_id'=> $babysitter_id,'is_finished'=>1),$orderby='' ,$groupby='', $limit='', $skip ='', $count = TRUE);
		$this->load->view("admin/babysitter/babysitterdetailpage",$data);
	}//.... end of view_details() ....//

	/**
	 * function for saving record ....//
	 */
	public function save_record()
	{
		$bs_id 	= $this->input->post("bs_id");
		$fname 		= $this->input->post("fname");
		$lname 		= $this->input->post("lname");
		$email 		= $this->input->post("email");
		$username 	= $this->input->post("username");
		$password 	= $this->input->post("password");
		$phone 		= $this->input->post("phone");
		$address 	= $this->input->post("address");
		$hrate	 	= $this->input->post("hrate");
		$card_number= $this->input->post("card_number");
		$expired 	= $this->input->post("expire_date");
		$aboutme 	= $this->input->post("aboutme");
		$pin_code 	= $this->input->post("pin_code");

		$availableOn = $this->input->post("availableOn");
		$to = $this->input->post("to");
		$from = $this->input->post("from");
		$availableOn = implode("-",$availableOn);

		$data = array(
				'fname'    => $fname,
				'lname'    => $lname,
				'email'    => $email,
				'username' => $username,
				'phone'	   => $phone,
				'address'  => $address,
				'hourly_rate'=> $hrate,
				'card_number'=> $card_number,
				'expiration_date'=> date("Y-m-d", strtotime($expired)),
				'about'	=> $aboutme,
				'pin_code'	=> $pin_code,
				'from_time'=> date("H:m:s", strtotime($from)),
				'to_time'=> date("H:m:s", strtotime($to)),
				'working_days'=> $availableOn,
				'utype'		=> 'b',
				'upgrade_request'=> 1
		);

		if(isset($_FILES['img'])){
			$path = time().$_FILES['img']['name'];
			$thumbsPath = 'uploads/thumbs/'.$path;
			$path = 'uploads/'.$path;
			if(! move_uploaded_file($_FILES['img']['tmp_name'],$path))
			{
				echo 'error';
				return false;
			}else {
				$res = $this->create_thumbs($path, $thumbsPath);
				if($res == true)
					$data['img'] = $thumbsPath;
				else
					$data['img'] = $path;
			}//.... end of if-else
		};

		$where = array('username' => $username, 'status'=>1);
		$where2 = array('email' => $email, 'status'=>1);
		if($bs_id){
			$where['bs_id !='] = $bs_id;
			$where2['bs_id !='] = $bs_id;
		}
		$match = $this->user_model->checkExist_user($this->table,'username', $where);
		$email = $this->user_model->checkExist_user("babysitter",'email', $where2);
		if(!empty($match))
		{
			echo 'exist';
			exit();
		}elseif(!empty($email)){
			echo 'emailExist';
			exit(1);
		}//.... end of if-else() ....//

		if($bs_id){
			$data['modified_at'] = date('Y-m-d');
			$data['modified_by'] = $this->session->userdata('user_id');
			$Status =  $this->common_model->update_record($this->table,$data,array('bs_id'=> $bs_id));
		}else{
			$data['password'] = md5($password);
			$data['created_at'] = date('Y-m-d');
			$data['created_by'] = $this->session->userdata('user_id');
			$Status =  $this->common_model->insert_record($this->table,$data);
			$insertID = $Status;
			if($insertID){
				$this->common_model->insert_record("notifications",array("title"=>"New User is Registered!","nfor"=>"admin","nsource"=> "b","from"=> $insertID,"description"=>"New user is registered, Please check his/her details information and Activate Account!",'created_at'=> date("Y-m-d H:i:s")));
			}//.... end of if() ....//
		}
		echo $Status;
	}//.... end of save_record() ....//

	/**
	 * Function for getting list of babysitters to populate grid....
	 */
	public function get_babysitters()
	{
		$this->datatables->select('bs_id,fname,lname,email,username,phone,address,upgrade_request as approve_request,utype')
				->from($this->table)->where(array('status != '=> 0, "upgrade_request"=> 1))
				->add_column('edit', '<a href="babysitter/edit/$1" class="btn btn-xs btn-warning"><i class=\'fa fa-pencil-square-o\'></i></a>', 'bs_id')
				->add_column('Action', '<a href="babysitter/delete/$1" class="btn btn-xs btn-danger"><i class=\'fa fa-minus\'></i></a>', 'bs_id')
				->add_column('approve', '<a href="'.base_url().'babysitters/approve/$1" class="btn btn-xs btn-success btnApprove"><i class=\'fa fa-check\'></i></a>', 'bs_id')
				->add_column('view','<a href="'.base_url().'babysitters/view/$1" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>','bs_id');

				/*add_column('Action','<a class="btn btn-danger delete" href="#">Delete</a>','bs_id');*/
		echo $this->datatables->generate();
	}//.... end of get_babysitters() ....//

}//.... end of class...