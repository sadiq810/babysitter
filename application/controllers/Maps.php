<?php

class Maps extends MY_Controller {

	/**
	 * Maps constructor.
	 */
    public function __construct()
    {
        parent::__construct();
    }//.... end of __construct() ....//

	/**
	 * Function for loading map...
	 */
	public function google_map()
	{
		$this->load->view('admin/maps/googlemap');
	}//.... end of google_map() ....//

	/**
	 * Function for getting all available jobs for populating google map.....
	 */
	public function get_allJobs()
	{
		$joins = array(
				(0)=> array(
					'table'		=> "babysitter",
					"condition" => "babysitter.bs_id = jobs.parent_id",
					"jointype"	=> "inner"
				)
		);
		$jobs = $this->common_model->get_join_record("jobs",array("job_id","location","jobs.lat","jobs.long","pstart_time as start_date"," pend_time as end_date","CONCAT(fname,' ',lname) as title"),$joins, array("is_finished"=> 0, "jobs.status"=> 1));
		foreach ($jobs as $job) {
			if(!$job->lat && !$job->long && $job->lat == null){
				$latLong = $this->get_lat_long($job->location);
				$job->lat = $latLong[0];
				$job->long = $latLong[1];
				$this->common_model->update_record("jobs",array('lat'=> $latLong[0],'long'=> $latLong[1]),array('job_id'=> $job->job_id));
			}//.... end of if() ....//
		}//.... end of foreach() ....//

		print json_encode($jobs);
	}//.... end of get_allJobs() .....//
	
	function vectormap()
	{
		$this->load->view('admin/maps/vectormap');
	}
	
	
}//.... end of class ....//