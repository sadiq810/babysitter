<?php
/**
 * Created by PhpStorm.
 * User: Spider
 * Date: 10/20/2014
 * Time: 10:02 PM
 */
class Login_controller extends MY_Controller{

    /**
     * constructor of the class Mobile_cotroller.
     */
    public function __construct(){
        parent::__construct();

    }//--- End of __construct() ---//

    /**
     * index function starts here
     */
    public function index()
    {
        if($this->session->userdata('is_logged_in') === TRUE)
        {
            $data['main_content'] = 'admin/table';
            $this->load->view('includes/template', $data);
        }
        else{
            $this->login();
            /*$this->load->view('home');*/
        }
    }//--- End of index() ---//

    /**
     * login function()
     */
    protected function  login()
    {
        //$this->load->view('login');
        $this->load->view('includes/login');
    }//--- End of function login() ---//

    /**
     * function for log out the user
     */
    public function logout()
    {
        $this->session->sess_destroy();
        redirect("/");
    }//--- End of function logout() ---//

    /**
     * function for validation of user
     */
    public function validate_user()
    {
        $this->form_validation->set_rules('username','User Name','required|trim');
        $this->form_validation->set_rules('password','Password','required|trim|md5');
        if($this->form_validation->run()){
            $user = $this->input->post('username');
            $user = $user == false ? '': $user;
            $pass = $this->input->post('password');
            $pass = $pass == false ? '': $pass;
            $data = $this->login_model->validate_credentials($user,$pass,'sys_users');
            if(empty($data))
            {
                $this->login();
            }
            else{
                $userdata = array(
                    'user_id'      => $data[0]->user_id,
                    'username'    => $data[0]->username,
                    'fname'        => $data[0]->fname,
                    'lname'        => $data[0]->lname,
                    'is_logged_in'=> TRUE,
                );
                $this->session->set_userdata($userdata);
                redirect('/');
            }

        }//End of outer if()
        else{
            $this->index();
        }

    }//--- End of function validate_user() ---//


    /**
     * function for emailing the forgot password to the user.
     */
    public function email_forgot_password()
    {
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $rec = $this->login_model->get_record('rms_user' ,array('Email','UserID') ,array('UserName'=>$username,'Email'=>$email));

        if(!empty($rec)){
            $password = $this->login_model->get_record('rms_user_cred_plain' ,array('UserCred') ,array('UserID'=>$rec[0]->UserID));
            $password=$password[0]->UserCred;

            $body = "<html>\n";
            $body .= "<body style=\"font-family:'Source Sans Pro', Arial, Tahoma, sans-serif; font-size:12px; color:#333;padding-top:10px;\">\n";
            $body .='<span style="font-weight:bold;padding:5px;">User Name:</span>'. $username."<br>".'
			        <span style="font-weight:bold;padding:5px;"> Password :</span>'.$password."<br>".'
					 <br /><br>';
            $body .= "</body>\n";
            $body .= "</html>\n";
            $config = array(
                'mailtype' => 'html',
                'newline' => '\r\n',
                'charset' => 'utf-8' //default charset
            );
            $this->load->library('email',$config);

            $this->email->from('rms@passwordmanager.com');
            $this->email->to($email);
            $this->email->subject('Your Forgotten Password');
            $this->email->message($body);
            if($this->email->send()){
                $data['message'] = 'Your Password is sent to '.$email.' ,Please Check your Email..';
            }else{
                $data['message'] = 'Password Could not be sent, Error Occured,Please Try Later..';
            }
        }else{
            $data['message'] = 'Your Email or User Name is Incorrect, Please try with Correct Information';
        }
        $this->load->view('login',$data);
    }//--- end of function email_forgot_password() ---//


}//--- End of Mobile_controller class ---//