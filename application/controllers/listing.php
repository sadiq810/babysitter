<?php

class Taskslist extends MY_Controller {
	 /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/list';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        /*$this->load->model('dashboard_model');
		
        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }*/
    }
	
    /**
    * Check if the user is logged in, if he's not, 
    * send him to the login page
    * @return void
    */	
	function lists()
	{
		/*if($this->session->userdata('is_logged_in')){
			redirect(site_url("admin") . '/dashboard');
        }else{
        	$this->load->view('admin/login');	
        }*/
		
		$data["main_content"] = "admin/list/taskslist";
		$this->load->view('includes/template', $data);
		
					
		
	}
}
?>