<?php
/**
 * Created by PhpStorm.
 * User: sadiq
 * Date: 4/8/2016
 * Time: 4:13 PM
 */
class Calender extends MY_Controller{

    /**
     * Calender constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }//.... end of __Construct() .....//

    /**
     * Function for loading Calender view....//
     */
    public function index()
    {
        $columns = array("job_id","pstart_time","pend_time","clock_out","is_finished","is_accepted");
        $this->db->order_by("is_finished");
        $jobs = $this->common_model->get_record("jobs",$columns,array("jobs.status"=> 1));
        foreach($jobs as $job){
            if($job->is_finished == 1){
                $job->jobStatus = "JOB COMPLETED";
                $job->date = date("m/d/Y", strtotime($job->clock_out));
            }elseif($job->is_accepted == 0){
                $job->jobStatus = "JOB PENDING";
                $job->date = date("m/d/Y", strtotime($job->pstart_time));
            }elseif($job->is_accepted == 1){
                $job->jobStatus = "JOB ACCEPTED";
                $job->date = date("m/d/Y", strtotime($job->pstart_time));
            }else{
                $job->jobStatus = "JOB REJECTED";
                $job->date = date("m/d/Y", strtotime($job->pstart_time));
            }
            unset($job->is_accepted);
            unset($job->is_finished);
        }

        $data['events'] = $jobs;
        $this->load->view('admin/calendars/calendar',$data);
    }//.... end of index()  ....//
    
    /**
     * function for returning a specific job details.....
     */
    public function get_job_details($job_id = '')
    {
        $columns = array("jobs.job_id","pstart_time","pend_time","clock_in","location","clock_out","babysitter_id","clock_out","is_finished","is_accepted","CONCAT(babysitter.fname,' ',babysitter.lname) as parent");

        $joins = array(
            (0)=> array(
                'table'=> "babysitter",
                'condition'=> 'jobs.parent_id = babysitter.bs_id',
                'jointype'=>  'inner'
            )
        );

        $jobs = $this->common_model->get_join_record("jobs",$columns,$joins,array("jobs.job_id"=> $job_id));
        foreach($jobs as $job){
            if($job->is_finished == 1){
                $job->jobStatus = "COMPLETED";
                $job->start_date = $job->clock_in;
                $job->end_date = $job->clock_out;
            }elseif($job->is_accepted == 0){
                $job->jobStatus = "PENDING";
                $job->start_date = $job->pstart_time;
                $job->end_date = $job->pend_time;
            }elseif($job->is_accepted == 1){
                $job->jobStatus = "ACCEPTED";
                $job->start_date = $job->pstart_time;
                $job->end_date = $job->pend_time;
            }else{
                $job->jobStatus = "REJECTED";
                $job->start_date = $job->pstart_time;
                $job->end_date = $job->pend_time;
            }//..... end of if-else() .....//

            $babysitter = $this->common_model->get_record("babysitter",array("CONCAT(fname,' ',lname) as babysitter"), array('bs_id'=> $job->babysitter_id));
            $job-> babysitter = (isset($babysitter))?$babysitter[0]->babysitter:NULL;

            unset($job->is_accepted);
            unset($job->is_finished);
            unset($job->babysitter_id);
            unset($job->pend_time);
            unset($job->pstart_time);
            unset($job->clock_in);
            unset($job->clock_out);
        }

        header('content-type:application/json');
        print json_encode($jobs);
    }//.... end of get_job_details() .....//

}//.... end of class...//