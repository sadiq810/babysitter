<?php

class ParentController extends MY_Controller {

	private $table = "babysitter";
	/**
	 * ParentController constructor.
	 */
    public function __construct()
    {
        parent::__construct();
		$this->load->model("user_model");
    }//.... end of constructor() ....//

	/**
	 * function for loading form for creating parent record....
	 */
	public function load_parent_form($parent_id = '')
	{
		$where = array('bs_id' => $parent_id);
		$column = array('*');
		$data["record"] = $this->common_model->get_record($this->table ,$column ,$where);
		$this->load->view('admin/createparent/addparent',$data);
	}//.... end of load_parent_form() ....//

	/**
	 * function for deleting a parent
	 */
	public function delete($parent_id = "")
	{
		$where = array('bs_id' => $parent_id);
		$columns = array('status' => 0);
		$status = $this->user_model->delete_record($this->table, $columns, $where);
		redirect("parent/list");
	}//.... end of delete() ....//

	/**
	 * function for approving parent....
	 */
	public function approve_parent($parent_id = '')
	{
		$where = array('bs_id' => $parent_id);
		$columns = array('approve_status' => 1);
		$status = $this->common_model->update_record($this->table, $columns, $where);
		echo $status;
	}//.... end of approve_parent() ....//

	/**
	 * function for saving parent data to database....
	 */
	public function save_record()
	{
		$parent_id 	= $this->input->post("parent_id");
		$fname 		= $this->input->post("fname");
		$lname 		= $this->input->post("lname");
		$email 		= $this->input->post("email");
		$username 	= $this->input->post("username");
		$password 	= $this->input->post("password");
		$phone 		= $this->input->post("phone");
		$address 	= $this->input->post("address");
		$card_number= $this->input->post("card_number");
		$pin_code	= $this->input->post("pin_code");
		$expire_date= $this->input->post("expire_date");

		$data = array(
				'fname'    => $fname,
				'lname'    => $lname,
				'email'    => $email,
				'username' => $username,
				'phone'	   => $phone,
				'address'  => $address,
				'card_number'=> $card_number,
				'pin_code'	=> $pin_code,
				'expiration_date'=> date('Y-m-d',strtotime($expire_date))
		);

		if(isset($_FILES['img'])){
			$path = time().$_FILES['img']['name'];
			$path = 'uploads/'.$path;
			if(! move_uploaded_file($_FILES['img']['tmp_name'],$path))
			{
				echo 'error';
				return false;
			}else
			{
				$data['img'] = $path;
			}
		};


		$where = array('username' => $username, 'status'=>1);
		$where2 = array('email' => $email, 'status'=>1);
		if($parent_id){
			$where['bs_id !='] = $parent_id;
			$where2['bs_id !='] = $parent_id;
		}
		$match = $this->user_model->checkExist_user($this->table,'username', $where);
		$email = $this->user_model->checkExist_user("babysitter",'email', $where2);
		if(!empty($match))
		{
			echo 'exist';
			exit();
		}elseif(!empty($email)){
			echo 'emailExist';
			exit(1);
		}//.... end of if-else() ....//

		if($parent_id){
			$data['modified_at'] = date('Y-m-d');
			$data['modified_by'] = $this->session->userdata('user_id');
			$Status =  $this->common_model->update_record($this->table,$data,array('bs_id'=> $parent_id));
		}else{
			$data['password'] = md5($password);
			$data['created_at'] = date('Y-m-d');
			$data['created_by'] = $this->session->userdata('user_id');
			$Status =  $this->common_model->insert_record($this->table,$data);
			$insertID = $Status;
			if($insertID){
				$this->common_model->insert_record("notifications",array("title"=>"New User Registered!","nfor"=>"admin","nsource"=> "p","from"=> $insertID,"description"=>"New User is registered, Please check his/her details information and Activate Account!",'created_at'=> date("Y-m-d H:i:s")));
			}//.... end of if() ....//
		}
		echo $Status;
	}//.... end of save_record() ....//

	/**
	 * Function for loading list of parents' view.....
	 */
	public function list_parent()
	{
		$this->load->view('admin/createparent/parentslist');
	}//.... end of list_parent() ....//
	
	/**
	 * function for loading all parents to grid....
	 */
	public function get_parents()
	{
		$this->datatables->select('bs_id,fname,lname,email,username,phone,address,approve_status')
				->from($this->table)->where(array('status !='=> 0))
				->add_column('edit', '<a href="'.base_url().'parent/edit/$1" class="btn btn-xs btn-warning"><i class=\'fa fa-pencil-square-o\'></i></a>', 'bs_id')
				->add_column('Action', '<a href="'.base_url().'parent/delete/$1" class="btn btn-xs btn-danger"><i class=\'fa fa-minus\'></i></a>', 'bs_id')
				->add_column('approve', '<a href="'.base_url().'parent/approve/$1" class="btn btn-xs btn-success btnApprove"><i class=\'fa fa-check\'></i></a>', 'bs_id')
				->add_column('view','<a href="'.base_url().'parent/view/$1" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>','bs_id');

		/*add_column('Action','<a class="btn btn-danger delete" href="#">Delete</a>','bs_id');*/
		echo $this->datatables->generate();
	}//.... end of get_parents() ....//

	/**
	 * function for viewing details of a parent....
	 */
	public function parent_details($parent_id = '')
	{
		$data['babysitter'] = $this->common_model->get_record($this->table,array("bs_id","fname","lname","address","phone","lat","long","card_number","expiration_date","initial_balance",  " hourly_rate as sitter_cost","about","img","likes","dislikes",'working_days',"from_time","to_time"), array("bs_id"=> $parent_id));
		if(!$data['babysitter'][0]->lat){
			$latLong = $this->get_lat_long($data['babysitter'][0]->address);
			$data['babysitter'][0]->lat = $latLong[0];
			$data['babysitter'][0]->long = $latLong[1];
			$this->common_model->update_record($this->table,array('lat'=> $latLong[0],'long'=> $latLong[1]),array('bs_id'=> $data['babysitter'][0]->bs_id));
		}//.... end of if() ....//

		$joins = array(
				(0)=> array(
						'table'=> 'jobs',
						'condition'=> 'jobs.parent_id = babysitter.bs_id',
						'jointype'=> 'inner'
				)
		);
		$where = array('jobs.parent_id'=> $parent_id,"is_finished"=> 1);
		$orderby = 'jobs.job_id desc';
		$data['clients'] = $this->common_model->get_join_record($this->table,array('fname',"lname","img"),$joins, $where,$take=12,$skip='',$count='',$groupby='',$orderby);
		//$data['jobs'] = $this->common_model->get_join_record("babysitter",array('reviews','fname',"lname","pstart_time","pend_time","total_cost","total_hours","is_paid"), $joins,array('parent_id'=> $parent_id,"is_finished"=> 1));
		$data['totalInvest'] = $this->common_model->get_join_record("jobs",array("sum(total_cost) as totalInvest"), $joins='',array('parent_id'=> $parent_id,"is_finished"=> 1));
		$data['paid'] = $this->common_model->get_join_record("jobs",array("sum(total_cost) as paid"), $joins='',array('parent_id'=> $parent_id,"is_finished"=> 1, "is_paid"=> 1));
		$data['payable'] = $this->common_model->get_join_record("jobs",array("sum(total_cost) as payable"), $joins='',array('parent_id'=> $parent_id,"is_finished"=> 1, "is_paid"=> 0));
		$data['totalHours'] = $this->common_model->get_join_record("jobs",array("sum(total_hours) as tHours"), $joins='',array('parent_id'=> $parent_id,"is_finished"=> 1));
		$data['totalJobPosted'] = $this->common_model->get_record("jobs",array("job_id"),array('parent_id'=> $parent_id),$orderby='' ,$groupby='', $limit='', $skip ='', $count = true);
		$data['CompletedJobs'] = $this->common_model->get_record("jobs",array("job_id"), array('parent_id'=> $parent_id,'is_finished'=>1),$orderby='' ,$groupby='', $limit='', $skip ='', $count = TRUE);
		$data['PEJobs'] = $this->common_model->get_record("jobs",array("job_id"), array('parent_id'=> $parent_id,'is_finished'=>0, 'is_accepted'=>0),$orderby='' ,$groupby='', $limit='', $skip ='', $count = TRUE);
		$data['RJobs'] = $this->common_model->get_record("jobs",array("job_id"), array('parent_id'=> $parent_id,'is_finished'=>0, 'is_accepted'=>2),$orderby='' ,$groupby='', $limit='', $skip ='', $count = TRUE);
		$data['parent_id'] = $parent_id;
		$this->load->view("admin/createparent/parent_details_view",$data);
	}//.... end of parent_details() ....//
	
	/**
	 * Function for getting list of total jobs, posted by a specific parent....
	 */
	public function get_total_jobs($parent_id = '')
	{
		$this->datatables->select('CONCAT(babysitter.fname," ",babysitter.lname) as babysitter,pstart_time,pend_time,clock_in,clock_out,hourly_cost,total_hours,total_cost,is_paid,is_finished,is_accepted')
				->from("jobs")->join("babysitter","jobs.babysitter_id = babysitter.bs_id","inner")->where(array("jobs.parent_id"=> $parent_id));

		$data = json_decode($this->datatables->generate());
		foreach($data->data as $key=> $obj){
			if($obj[8] == 1 && $obj[9] == 1) {
				$data->data[$key][8] = "Done";
			}elseif($obj[8] == 0 && $obj[9] == 1){
				$data->data[$key][8] = "Pending";
			}else{
				$data->data[$key][8] = "";
			}//... end of if-else ....//

			if($obj[9] == 1){
				$data->data[$key][9] = "COMPLETED";
			}elseif($obj[10] == 0){
				$data->data[$key][9] = "PENDING";
			}elseif($obj[10] == 1){
				$data->data[$key][9] = "ACCEPTED";
			}else{
				$data->data[$key][9] = "REJECTED";
			}
			unset($data->data[$key][10]);
		}//.... end of foreach() ....//

		echo json_encode($data);
	}//.... end of get_total_jobs() .....//
	
	/**
	 * function for getting list of completed jobs....
	 */
	public function get_completed_jobs($parent_id)
	{
		$this->datatables->select('CONCAT(babysitter.fname," ",babysitter.lname) as babysitter,clock_in,clock_out,hourly_cost,total_hours,total_cost,reviews,is_paid,is_finished,is_accepted')
				->from("jobs")->join("babysitter","jobs.babysitter_id = babysitter.bs_id","inner")->where(array("jobs.parent_id"=> $parent_id,"jobs.is_finished"=> 1));

		$data = json_decode($this->datatables->generate());
		foreach($data->data as $key=> $obj){
			if($obj[7] == 1 && $obj[8] == 1) {
				$data->data[$key][7] = "Done";
			}elseif($obj[7] == 0 && $obj[8] == 1){
				$data->data[$key][7] = "Pending";
			}else{
				$data->data[$key][7] = "";
			}//... end of if-else ....//

			if($obj[8] == 1){
				$data->data[$key][8] = "COMPLETED";
			}elseif($obj[9] == 0){
				$data->data[$key][8] = "PENDING";
			}elseif($obj[9] == 1){
				$data->data[$key][8] = "ACCEPTED";
			}else{
				$data->data[$key][8] = "REJECTED";
			}
			unset($data->data[$key][9]);
		}//.... end of foreach() ....//

		echo json_encode($data);
	}//.... end of get_completed_jobs() ....//

	/**
	 * Function for getting list of pending or expired jobs.....
	 */
	public function get_pendingExpired_jobs($parent_id = '')
	{
		$this->datatables->select('CONCAT(babysitter.fname," ",babysitter.lname) as babysitter,pstart_time,pend_time,hourly_cost,is_accepted')
				->from("jobs")->join("babysitter","jobs.babysitter_id = babysitter.bs_id","inner")->where(array("jobs.parent_id"=> $parent_id,"jobs.is_accepted "=> 0));

		$data = json_decode($this->datatables->generate());
		foreach($data->data as $key=> $obj)
		{
			if($obj[4] == 0 AND date("Y-m-d", strtotime($obj[2])) >= date('Y-m-d')){
				$data->data[$key][4] = "PENDING";
			}else{
				$data->data[$key][4] = "EXPIRED";
			}
		}//.... end of foreach() ....//

		echo json_encode($data);
	}//.... end of get_pendingExpired_jobs() .....//
	
	/**
	 * Function for getting list of rejected jobs......
	 */
	public function get_rejected_jobs($parent_id = '')
	{
		$this->datatables->select('CONCAT(babysitter.fname," ",babysitter.lname) as babysitter,pstart_time,pend_time,hourly_cost,is_accepted,feedback')
				->from("jobs")->join("babysitter","jobs.babysitter_id = babysitter.bs_id","inner")->where(array("jobs.parent_id"=> $parent_id,"jobs.is_accepted "=> 2));

		$data = json_decode($this->datatables->generate());
		foreach($data->data as $key=> $obj)
		{
			if($obj[4] == 2){
				$data->data[$key][4] = "REJECTED";
			}else{
				$data->data[$key][4] = "EXPIRED";
			}
		}//.... end of foreach() ....//

		echo json_encode($data);
	}//.... end of get_rejected_jobs() .....//
}//.... end of class....//