<?php

class Admin extends MY_Controller
{
	private $table = "sys_users";

	/**
	 * Admin constructor.
	 */
    public function __construct()
    {
        parent::__construct();
		$this->load->model("user_model");
    }//..... end of __construct() ....//

	/**
	 * function for loading admin creation form....
	 */
	public function load_admin_form()
	{
		$this->load->view('admin/admins/createadmin');
	}//.... end of load_admin_form() ....//

	/**
	 * function for listing all admin users....
	 */
	function list_admins()
	{
		$where = array('status' => 1);
		$column = array('*');
		$data["record"] = $this->common_model->get_record($this->table ,$column ,$where);
		$this->load->view('admin/admins/adminlist', $data);
	}//.... end of list_admins()....//
	
	/**
	 * function for saving new admin data to the database.....
	 */
	public function save_admin()
	{
		$user_id 	= $this->input->post("user_id");
		$fname 		= $this->input->post("fname");
		$lname 		= $this->input->post("lname");
		$email 		= $this->input->post("email");
		$username 	= $this->input->post("username");
		$password 	= $this->input->post("password");
		$phone 		= $this->input->post("phone");
		$address 	= $this->input->post("address");

		$data = array(
			'fname'    => $fname,
			'lname'    => $lname,
			'email'    => $email,
			'username' => $username,
			'phone'	   => $phone,
			'address'  => $address
		);

		if(isset($_FILES['img'])){
			$path = time().$_FILES['img']['name'];
			$path = 'uploads/'.$path;
			if(! move_uploaded_file($_FILES['img']['tmp_name'],$path))
			{
				echo 'error';
				return false;
			}else
			{
				$data['img'] = $path;
			}
		};

		$where = array('username' => $username, 'status'=>1);
		if($user_id){
			$where['user_id !='] = $user_id;
		}
		$match = $this->user_model->checkExist_user($this->table,'username', $where);
		if(!empty($match))
		{
			echo 'exist';
			exit();
		}

		if($user_id){
			$data['modified_at'] = date('Y-m-d');
			$data['modified_by'] = $this->session->userdata('user_id');
			$Status =  $this->user_model->update_record($this->table,$data,array('user_id'=> $user_id));
		}else{
			$data['password'] = md5($password);
			$data['created_at'] = date('Y-m-d');
			$data['created_by'] = $this->session->userdata('user_id');
			$Status =  $this->user_model->insert_record($this->table,$data);
		}
		echo $Status;
	}//.... End of save_admin() ....//

	/**
	 * function for editing specific user....
	 */
	public function edit($user_id = '')
	{
		$where = array('user_id' => $user_id);
		$column = array('*');
		$data["record"] = $this->common_model->get_record($this->table ,$column ,$where);
		$this->load->view("admin/admins/createadmin",$data);
	}//.... end of edit() .....//

	/**
	 * function for deleting user...
	 */
	public function delete($user_id = '')
	{
		$where = array('user_id' => $user_id);
		$columns = array('status' => 0);
		$status = $this->user_model->delete_record($this->table, $columns, $where);
		redirect("list_admin");
	}//.... end of delete() ....//
}//.... end of class...//