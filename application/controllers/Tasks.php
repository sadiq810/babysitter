<?php

class Tasks extends MY_Controller {

    /**
     * Tasks constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }//.... end of __construct() .....//

    /**
     * Function for loading task view.....//
     */
	public function tasks()
	{
		$this->load->view("admin/tasksandnotes/tasks");
	}//.... end of tasks() ....//
    
    /**
     * Function for getting all tasks....
     */
    public function get_tasks_list()
    {
       $this->datatables->select('CONCAT(babysitter.fname," ",babysitter.lname) as parent,location,pstart_time,pend_time,babysitter_id,hourly_cost,is_finished,is_accepted')
            ->from("jobs")->join("babysitter","jobs.parent_id = babysitter.bs_id","inner")->where(array());

        /*add_column('Action','<a class="btn btn-danger delete" href="#">Delete</a>','bs_id');*/
        $data = json_decode($this->datatables->generate());
        foreach($data->data as $key=> $obj){
            $babysitter = $this->common_model->get_record("babysitter",array("CONCAT(fname,' ',lname) as babysitter"), array('bs_id'=> $obj[4]));
            if(!empty($babysitter) && isset($babysitter[0]->babysitter)){
                $data->data[$key][4] = $babysitter[0]->babysitter;
            }else{
                $data->data[$key][4] = "";
            }//.... end of if() .....//

            if($obj[6] == 1){
                $data->data[$key][6] = "COMPLETED";
            }elseif($obj[7] == 0){
                $data->data[$key][6] = "PENDING";
            }elseif($obj[7] == 1){
                $data->data[$key][6] = "ACCEPTED";
            }else{
                $data->data[$key][6] = "REJECTED";
            }
            unset($data->data[$key][7]);
        }//.... end of foreach() ....//
        //echo $this->datatables->generate();
        echo json_encode($data);
    }//.... end of get_tasks_list() .....//

}//.... end of the class....//