<?php $this->load->view("includes/header");?>
    <div class="row">
        <div class="col-md-12">
            <!--Top header start-->
            <h3 class="ls-top-header">Parent Detail Page</h3>
            <!--Top header end -->

            <!--Top breadcrumb start -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Parent</a></li>
                <li class="active"><?php echo $babysitter[0]->fname." ".$babysitter[0]->lname; ?> Details</li>
            </ol>
            <!--Top breadcrumb start -->
        </div>
    </div>
    <!-- Main Content Element  Start-->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <!--User Details Start-->
            <div class="ls-user-details">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <!--User Name-->
                        <div class="ls-user-name">
                            <h1><?php echo $babysitter[0]->fname." ".$babysitter[0]->lname; ?></h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--User Details start-->
                    <div class="user-detail col-md-4 col-sm-5">
                        <div class="ls-user-position">
                            <h4>Credit Card No: &nbsp;<?php
                                $subStrNo = (isset($babysitter[0]->card_number))? substr($babysitter[0]->card_number, -4):'';
                                $length = (isset($babysitter[0]->card_number) && strlen($babysitter[0]->card_number))?strlen($babysitter[0]->card_number):12;
                                echo str_pad($subStrNo,$length,"*",STR_PAD_LEFT);
                                ?></h4>
                            <p>&nbsp;&nbsp;</p>
                        </div>
                        <!--Address-->
                        <address class="col-sm-11">
                            <i class="fa fa-map-marker"></i>
                            <!--795 Folsom Ave, Suite 600<br>
                            San Francisco, CA 94107<br>-->
                            <?php echo $babysitter[0]->address; ?><br>
                            <abbr title="Phone"><i class="fa fa-mobile"></i> P:</abbr> <?php echo $babysitter[0]->phone; ?>
                        </address>
                        <!--User Image-->
                        <div class="user-pic">
                            <?php if($babysitter[0]->img){?>
                                <img alt="image" src="<?php echo base_url().$babysitter[0]->img; ?>">
                            <?php }else{?>
                                <img alt="image" src="<?php echo base_url(); ?>assets/images/demo/avatar-115.png">
                            <?php }?>
                        </div>
                    </div>
                    <!--User Info Start-->
                    <div class="ls-user-info col-md-8 col-sm-7">
                        <div class="ls-user-text">
                            <p> <span style="float: left; margin-left: 36%">INITIAL DEPOSIT : <?php echo ($babysitter[0]->initial_balance)?$babysitter[0]->initial_balance:0;?> $</span>  TOTAL INVESTMENT : <?php echo (isset($totalInvest[0]->totalInvest))?number_format((float)$totalInvest[0]->totalInvest, 2, '.', ''):0;?> $.</p>
                            <p> <span style="float: left; margin-left: 36%">TOTAL PAID : <?php echo (isset($paid[0]->paid))?number_format((float)$paid[0]->paid, 3, '.', ''):0;?> $</span> PAYABLE AMOUNT : <?php echo (isset($payable[0]->payable))?number_format((float)$payable[0]->payable, 3, '.', ''):0;?> $</p>
                        </div>
                        <div class="ls-user-text2">
                            <h4><span style="float: left; margin-left: 36%"> TOTAL JOBS POSTED : <?php echo (isset($totalJobPosted))?(int)$totalJobPosted:0;?> </span> PENDING/EXPIRED JOBS: <?php echo $PEJobs;?></h4>
                            <p> <span style="float: left; margin-left: 36%"> COMPLETED JOBS : <?php echo (isset($CompletedJobs))?(int)$CompletedJobs:0;?> </span>  REJECTED JOBS: <?php echo $RJobs; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="user-profile-tab">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs  nav-justified icon-tab">
                    <li class="active">
                        <a data-toggle="tab" class="user-google-location" href="#about-me">
                            <i class="fa fa-gears"></i> <span>About Me</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tab-border">
                    <div id="about-me" class="tab-pane fade active in">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="user-map-locator" id="user-locator" style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;">
                                    <div id="googleMap" style="width:100%;height:200px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-content tab-border">
                    <div id="about-me" class="tab-pane fade active in">
                        <div class="row">
                            <div class="sale-widget">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs icon-tab icon-tab-home nav-justified">
                                    <li class="active"><a data-toggle="tab" href="#allJobs"><i class="fa fa-calendar-o"></i> <span>All Posted Jobs</span></a></li>
                                    <li class=""><a data-toggle="tab" href="#completedJobs"><i class="fa fa-dollar"></i> <span>Completed Jobs</span></a></li>
                                    <li class=""><a data-identifier="heroDonut" data-toggle="tab" href="#PEJobs"><i class="fa fa-shopping-cart"></i> <span>Pending/Expired Jobs</span></a></li>
                                    <li class=""><a data-identifier="heroDonut" data-toggle="tab" href="#rejectedJobs"><i class="fa fa-shopping-cart"></i> <span>Rejected Jobs</span></a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div id="allJobs" class="tab-pane fade active in">
                                        <h4>Total Jobs</h4>
                                        <div class="monthlySale">
                                            <table class="table table-bordered table-striped table-hover dataTable no-footer" id="totalJobs">
                                                <thead>
                                                <tr>
                                                    <th>Babysitter</th>
                                                    <th>Job Start Time</th>
                                                    <th>Job End Time</th>
                                                    <th>Clock In</th>
                                                    <th>Clock Out</th>
                                                    <th>Hourly Cost</th>
                                                    <th>Total Hours</th>
                                                    <th>Total Cost</th>
                                                    <th>Payment Status</th>
                                                    <th>Job Status</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="completedJobs" class="tab-pane fade">
                                        <h4>Completed Jobs</h4>
                                        <div class="monthlySale">
                                            <table class="table table-bordered table-striped table-hover dataTable no-footer" id="completedJobsTable">
                                                <thead>
                                                <tr>
                                                    <th>Babysitter</th>
                                                    <th>Clock In</th>
                                                    <th>Clock Out</th>
                                                    <th>Hourly Cost</th>
                                                    <th>Total Hours</th>
                                                    <th>Total Cost</th>
                                                    <th>Review</th>
                                                    <th>Payment Status</th>
                                                    <th>Job Status</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="PEJobs" class="tab-pane fade">
                                        <h4>Pending/Expired Jobs</h4>
                                        <div class="monthlySale">
                                            <table class="table table-bordered table-striped table-hover dataTable no-footer" id="PEJobsTable">
                                                <thead>
                                                <tr>
                                                    <th>Babysitter</th>
                                                    <th>Job Start Time</th>
                                                    <th>Job End Time</th>
                                                    <th>Hourly Cost</th>
                                                    <th>Job Status</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="rejectedJobs" class="tab-pane fade">
                                        <h4>Rejected Jobs</h4>
                                        <div class="monthlySale">
                                            <table class="table table-bordered table-striped table-hover dataTable no-footer" id="rejectedJobsTable">
                                                <thead>
                                                <tr>
                                                    <th>Babysitter</th>
                                                    <th>Job Start Time</th>
                                                    <th>Job End Time</th>
                                                    <th>Hourly Cost</th>
                                                    <th>Job Status</th>
                                                    <th>FeedBack</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <!-- Tab End -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Main Content Element  End-->
    <script type="application/javascript">
        var bounds = new google.maps.LatLngBounds();
        var map;
        var marker;
        var infowindow = [];
        var clients = [];
        var img = "<?php echo $babysitter[0]->img; ?>";
        var lat = "<?php echo $babysitter[0]->lat; ?>";
        var long = "<?php echo $babysitter[0]->long; ?>";
        var babysitterID = <?php echo $babysitter[0]->bs_id; ?>;

        function initialize()
        {
            var mapProp = {
                center:new google.maps.LatLng(lat, long),
                zoom:15,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            infowindow[babysitterID] = new google.maps.InfoWindow({
                content: "<strong><?php echo $babysitter[0]->fname.' '.$babysitter[0]->lname;?></strong>"
            });

            var image = "http://maps.google.com/mapfiles/marker_orange.png";
            if(img != "")
                image = {url: "<?php echo base_url()?>"+ img ,scaledSize: new google.maps.Size(32, 32) };
            marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(lat, long),
                title: "<?php echo $babysitter[0]->fname.' '.$babysitter[0]->lname; ?>",
                icon: image

            });

            var label = new Label({ map: map });
            label.bindTo('position', marker, 'position');
            label.bindTo('text', marker, 'title');
            marker.empid = babysitterID;
            bounds.extend(marker.position);
            clients[babysitterID] = marker;

            google.maps.event.addListener(clients[babysitterID], 'click', function() {
                infowindow[this.empid].open( map,clients[this.empid] );
            });

            map.fitBounds(bounds);
        }//.... end of initialize() .....//
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <script type="text/javascript" language="javascript" class="init">
        $(document).ready(function() {
            $('#totalJobs').dataTable( {
                "processing": true,
                "serverSide": true,
                //.... Enable/Disable Info
                "paging":     true,
                "ordering":   true,
                "info":       true,
                //.... Saving state of the table,if you move to other sites and than return to this page,than the saved state will be loaded....//
                "stateSave": true,
                //..... Alternatives for pagination....//
                "pagingType": "simple_numbers",//numbers,simple,simple_numbers,full,full_numbers
                //..... Language Options....//
                "language": {
                    "lengthMenu": "Display _MENU_ Records Per Page",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing Page _PAGE_ of _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                //.... Page Length Options.....//
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],//first array define length and second for displaying

                "ajax": {
                    "url": "<?php echo base_url().'index.php/parentController/get_total_jobs/'.$parent_id;?>",
                    "type": "POST"
                },

                "columnDefs" : [
                    /*{
                        'sortable'  : false,
                        'searchable': false,
                        'orderable' :false,
                        'targets' : 8
                    },*/
                    //.....Column Rendering.....//
                    /*{
                        "render": function ( data, type, row ) {
                            if(row[7] == 0){
                                return data +' '+ row[9]+' '+row[10];
                            }else{
                                return data +' '+ row[9]+' '+row[11];
                            }
                        },
                        "targets": 8
                    },
                    {
                        "render": function ( data, type, row ) {
                            if(row[7] == 0){
                                return "UnApproved";
                            }else{
                                return "Approved";
                            }
                            //return data +' '+ row[9];
                        },
                        "targets": 7
                    },*/

                ]
            });//.... End of dataTables for totalJobs ....//

            /**
             * Datatable for Completed Jobs.....
             */
            $('#completedJobsTable').dataTable( {
                "processing": true,
                "serverSide": true,
                //.... Enable/Disable Info
                "paging":     true,
                "ordering":   true,
                "info":       true,
                //.... Saving state of the table,if you move to other sites and than return to this page,than the saved state will be loaded....//
                //"stateSave": true,
                //..... Alternatives for pagination....//
                "pagingType": "simple_numbers",//numbers,simple,simple_numbers,full,full_numbers
                //..... Language Options....//
                "language": {
                    "lengthMenu": "Display _MENU_ Records Per Page",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing Page _PAGE_ of _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                //.... Page Length Options.....//
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],//first array define length and second for displaying

                "ajax": {
                    "url": "<?php echo base_url().'index.php/parentController/get_completed_jobs/'.$parent_id;?>",
                    "type": "POST"
                }
            });//.... End of dataTables...
            $("#completedJobsTable").css("width","100%");


            /**
             * DataTable for Pending or Expired Jobs.....
             */
            $('#PEJobsTable').dataTable( {
                "processing": true,
                "serverSide": true,
                //.... Enable/Disable Info
                "paging":     true,
                "ordering":   true,
                "info":       true,
                //.... Saving state of the table,if you move to other sites and than return to this page,than the saved state will be loaded....//
                //"stateSave": true,
                //..... Alternatives for pagination....//
                "pagingType": "simple_numbers",//numbers,simple,simple_numbers,full,full_numbers
                //..... Language Options....//
                "language": {
                    "lengthMenu": "Display _MENU_ Records Per Page",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing Page _PAGE_ of _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                //.... Page Length Options.....//
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],//first array define length and second for displaying

                "ajax": {
                    "url": "<?php echo base_url().'index.php/parentController/get_pendingExpired_jobs/'.$parent_id;?>",
                    "type": "POST"
                }
            });//.... End of dataTables for Pending or Expired jobs ....//
            $("#PEJobsTable").css("width","100%");

            /**
             * DataTable for listing Rejected Jobs.....
             */
            $('#rejectedJobsTable').dataTable( {
                "processing": true,
                "serverSide": true,
                //.... Enable/Disable Info
                "paging":     true,
                "ordering":   true,
                "info":       true,
                //.... Saving state of the table,if you move to other sites and than return to this page,than the saved state will be loaded....//
                //"stateSave": true,
                //..... Alternatives for pagination....//
                "pagingType": "simple_numbers",//numbers,simple,simple_numbers,full,full_numbers
                //..... Language Options....//
                "language": {
                    "lengthMenu": "Display _MENU_ Records Per Page",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing Page _PAGE_ of _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                //.... Page Length Options.....//
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],//first array define length and second for displaying

                "ajax": {
                    "url": "<?php echo base_url().'index.php/parentController/get_rejected_jobs/'.$parent_id;?>",
                    "type": "POST"
                }
            });//.... End of dataTables for Pending or Expired jobs ....//
            $("#rejectedJobsTable").css("width","100%");

        });//.... End of ready....//
    </script>

<?php $this->load->view("includes/footer");?>