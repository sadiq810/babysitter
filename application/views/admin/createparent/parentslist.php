<?php $this->load->view("includes/header");?>
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Parents List</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">List</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
                    <!-- Main Content Element  Start-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Parents' List</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped table-hover dataTable no-footer" id="parentList">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Username</th>
                                                <th>Phone</th>
                                                <th>Address</th>
                                                <th>Approval</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->
    <script type="text/javascript" language="javascript" class="init">
        $(document).ready(function() {
            $('#parentList').dataTable( {
                "processing": true,
                "serverSide": true,
                //.... Enable/Disable Info
                "paging":     true,
                "ordering":   true,
                "info":       true,
                //.... Saving state of the table,if you move to other sites and than return to this page,than the saved state will be loaded....//
                "stateSave": true,
                //..... Alternatives for pagination....//
                "pagingType": "simple_numbers",//numbers,simple,simple_numbers,full,full_numbers
                //..... Language Options....//
                "language": {
                    "lengthMenu": "Display _MENU_ Records Per Page",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing Page _PAGE_ of _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                //.... Page Length Options.....//
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],//first array define length and second for displaying

                "ajax": {
                    "url": "<?php echo base_url().'index.php/parentController/get_parents';?>",
                    "type": "POST"
                },

                "columnDefs" : [
                    {
                        'sortable'  : false,
                        'searchable': false,
                        'orderable' :false,
                        //'orderData':[0,1], //.... Used to order data by columns,if values in 0 columns identical than orderby column no 1.
                        //'visible':true,
                        'targets' : 8
                    },
                    //.....Column Rendering.....//
                    {
                        "render": function ( data, type, row ) {
                            if(row[7] == 0){
                                return data +' '+ row[9]+' '+row[10];
                            }else{
                                return data +' '+ row[9]+' '+row[11];
                            }
                        },
                        "targets": 8
                    },
                    {
                        "render": function ( data, type, row ) {
                            if(row[7] == 0){
                                return "UnApproved";
                            }else{
                                return "Approved";
                            }
                            //return data +' '+ row[9];
                        },
                        "targets": 7
                    },

                ]
            } );//.... End of dataTables...

            $("body").on('click',".btnApprove",function(e){
                e.preventDefault();
                var url = $(this).attr("href");
                $.ajax({
                    url:url,
                    type:'post',
                    success:function(response){
                        $('#parentList').DataTable().ajax.reload();
                    }
                });
                e.stopImmediatePropagation();
            });
        });//.... End of ready....//

    </script>
<?php $this->load->view("includes/footer");?>