<?php $this->load->view("includes/header");?>
<?php
$parent_id = "";
$fname = "";
$lname = "";
$email = "";
$username = "";
$phone = "";
$address = "";
$card_number = "";
$expire_date = date("m-d-Y");
$pin_code = "";
if(isset($record) && !empty($record)){
    $parent_id       = (isset($record[0]->bs_id))?$record[0]->bs_id:"";
    $fname       = (isset($record[0]->fname))?$record[0]->fname:"";
    $lname       = (isset($record[0]->lname))?$record[0]->lname:"";
    $email       = (isset($record[0]->email))?$record[0]->email:"";
    $username    = (isset($record[0]->username))?$record[0]->username:"";
    $phone       = (isset($record[0]->phone))?$record[0]->phone:"";
    $address     = (isset($record[0]->address))?$record[0]->address:"";
    $card_number = (isset($record[0]->card_number))?$record[0]->card_number:"";
    $expire_date = (isset($record[0]->expiration_date))?$record[0]->expiration_date:"";
    $pin_code    = (isset($record[0]->pin_code))?$record[0]->pin_code:"";
    $expire_date = date("Y-m-d",strtotime($expire_date));
}//.... end of if() ....//
?>
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Create a Parent Profile</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li><a href="#">Parent Profile</a></li>
                                <li class="active">Create a Parent Profile</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
                    <!-- Main Content Element  Start-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></h3>
                                </div>
                                <div class="panel-body">
                                    <form action="<?php echo base_url().'index.php/parentController/save_record'?>" class="form-horizontal ls_form bv-form" method="post" id="defaultForm">
                                        <input type="hidden" name="parent_id" value="<?php echo $parent_id; ?>">
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">First name</label>
                                            <div class="col-lg-6">
                                                <input type="text" value="<?php echo $fname; ?>" placeholder="Enter first name.." name="fname" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Last name</label>
                                            <div class="col-lg-6">
                                                <input type="text" value="<?php echo $lname; ?>" required placeholder="Enter last name.." name="lname" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Email</label>
                                            <div class="col-lg-6">
                                                <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" placeholder="Enter email.." required>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Username</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="username" value="<?php echo $username; ?>" class="form-control" placeholder="Enter username.." required>
                                            </div>
                                        </div>
                                        <?php if(isset($record) && empty($record)):?>
                                            <div class="form-group has-feedback">
                                                <label class="col-lg-3 control-label">Password</label>
                                                <div class="col-lg-6">
                                                    <input type="password" name="password" class="form-control" placeholder="Enter password.." required>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Phone</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="phone" value="<?php echo $phone; ?>" class="form-control" placeholder="Enter phone number.." required>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Address</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="address" value="<?php echo $address; ?>" class="form-control" placeholder="Enter address..." required></div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Credit Card Number</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="card_number" value="<?php echo $card_number; ?>" class="form-control" placeholder="Enter Credit Card Number..." required></div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">PIN Code</label>
                                            <div class="col-lg-6">
                                                <input type="number" maxlength="4" min="4" name="pin_code" value="<?php echo $pin_code; ?>" class="form-control" placeholder="Enter Credit Card PIN Code..." required></div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Expiration Date</label>
                                            <div class="col-md-6">
                                                <input type="text" name="expire_date" class="form-control" id="ExdatePicker">
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Profile Image</label>
                                            <div class="col-lg-6">
                                                <input type="file" name="img" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-9 col-lg-offset-3">
                                                <input type="submit" value="Submit" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>

    <!--------------------- Modal Window Starts here ------------------------------------------------------->
    <div class="modal fade" id="myModalDanger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header label-red white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModaDanger">Error!</h4>
                </div>
                <div class="modal-body">
                    <div id="message"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn ls-red-btn btn-xs" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!------------------ End of Modal Window ------------------------------------>

                    <script>
                        $(document).ready(function(){
                            $("#defaultForm").on("submit",function(e){
                                e.preventDefault();
                                $(this).ajaxSubmit({
                                    success:function(response){
                                        if(response == 'exist'){
                                            $("#message").html("");
                                            $("#message").html("Username is Already Taken, Please Select another one.");
                                            $("#myModalDanger").modal("show");
                                            return false;
                                        }else if(response == 'emailExist'){
                                            $("#message").html("");
                                            $("#message").html("Email is already exist, please select different one!");
                                            $("#myModalDanger").modal("show");
                                            return false;
                                        }else{
                                            window.location.href = '<?php echo base_url()."parent/list";?>';
                                        }
                                    }//..... end of success() ....//
                                });//... end of ajaxSubmit() ....//
                            });//.... end of submit() ....//

                            $("#ExdatePicker").datetimepicker({
                                timepicker:false,
                                datepicker:true,
                                mask:false,
                                value:'<?php echo $expire_date;?>',
                                format:"Y-m-d"
                            });

                        });//.... end of ready....//
                    </script>
<?php $this->load->view("includes/footer");?>