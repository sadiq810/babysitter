<?php $this->load->view("includes/header");?>
<?php
$ci = &get_instance();
$totalMembers       = $ci->get_all_members();
$totalParents       = $ci->get_all_parents();
$totalBabysitters   = $ci->get_all_babysitters();
$totalJobs          = $ci->get_all_jobs();
$totalRejectedJobs  = $ci->get_all_rejectedJobs();
$totalCompletedJobs = $ci->get_all_completedJobs();
$totalPendingJobs   = $ci->get_all_pendingJobs();
$totalProgressJobs  = $ci->get_all_progressJobs();
$totalIncome        = $ci->get_total_income();
$newJobs            = $ci->get_newJobs();
$newUsers           = $ci->get_newUsers();
$totalIncomeInThisMonth= $ci->get_income_current_month();
$completedJobsInThisMonth= $ci->get_current_month_completedJobs();
$sixMonthSaleReport= $ci->get_six_months_saleReport();

$getPopulateBabysitters = $ci->get_popular_babysitters();
?>
<div class="row">
    <div class="col-md-12">
        <!--Top header start-->
        <h3 class="ls-top-header">Dashboard</h3>
        <!--Top header end -->

        <!--Top breadcrumb start -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i></a></li>
            <li class="active">Dashboard</li>
        </ol>
        <!--Top breadcrumb start -->
    </div>
</div>
<!-- Main Content Element  Start-->
<div class="row">
    <div class="col-md-9">
        <div class="memberBox">
            <div class="memberBox-header">
                <h5>Logged In Member</h5>
            </div>
            <div id="realTimeChart" class="flotChartRealTime widgetRealTime">

            </div>
            <div class="memberBox-details">
                <ul>
                    <li>
                        <div class="memberBox-title">
                            <i class="fa fa-users"></i>
                            <h4>Members</h4>
                        </div>
                        <div class="memberBox-value up"><i class="fa fa-user"></i> <span><?php echo $totalMembers; ?></span></div>
                    </li>
                    <li>
                        <div class="memberBox-title">
                            <i class="fa fa-eye"></i>
                            <h4>Parents</h4>
                        </div>
                        <div class="memberBox-value down"><i class="fa fa-flag"></i> <span><?php echo $totalParents; ?></span></div>
                    </li>
                    <li>
                        <div class="memberBox-title">
                            <i class="fa fa-eye"></i>
                            <h4>BabySitters</h4>
                        </div>
                        <div class="memberBox-value down"><i class="fa fa-flag"></i> <span><?php echo $totalBabysitters; ?></span></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="current-status-widget">
            <ul>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="sale-view"><?php echo $totalJobs; ?></h5>
                        <p class="lightGreen"><i class="fa fa-arrow-up lightGreen"></i> Total Jobs</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-red white">
                            <!--<i class="fa fa-download"></i>-->
                            <i class="fa fa-truck"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="download-show"><?php echo $totalRejectedJobs; ?></h5>
                        <p class="light-blue"><i class="fa fa-arrow-down light-blue"></i> Rejected Jobs</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-lightBlue white">
                            <i class="fa fa-truck"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="deliver-show"><?php echo $totalCompletedJobs; ?></h5>
                        <p class="light-blue"><i class="fa fa-arrow-up light-blue"></i> Completed Jobs</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-users"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="user-show"><?php echo $totalPendingJobs; ?></h5>
                        <p class="lightGreen"><i class="fa fa-arrow-up lightGreen"></i> Pending Jobs</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-success white">
                            <i class="fa fa-github"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="product-up"><?php echo $totalProgressJobs; ?></h5>
                        <p class="text-success"><i class="fa fa-arrow-up text-success"></i> Progress Jobs</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-dollar"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="income-show"><?php echo  number_format((float)$totalIncome, 3, '.', ''); ?> </h5>
                        <p class="lightGreen"><i class="fa fa-arrow-up lightGreen"></i> Total Income</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>
        </div>

    </div>
</div>

<div class="row home-row-top">
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-1" class="chart-pie-widget" data-percent="<?php echo (int)$newJobs*100/$totalJobs; ?>">
                <span class="pie-widget-count-1 pie-widget-count"></span>
            </div>
            <p>
                New Jobs
            </p>
            <h5><i class="fa fa-bomb"></i> <?php echo $newJobs; ?> </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-2" class="chart-pie-widget" data-percent="<?php echo (int) $newUsers*100/$totalMembers; ?>">
                <span class="pie-widget-count-2 pie-widget-count"></span>
            </div>
            <p>
                New Users
            </p>
            <h5><i class="fa fa-child"></i> <?php echo $newUsers; ?> </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-3" class="chart-pie-widget" data-percent="<?php echo (int) $totalIncomeInThisMonth*100/$totalIncome; ?>">
                <span class="pie-widget-count-3 pie-widget-count"></span>
            </div>
            <p>
                Total income
            </p>
            <h5><i class="fa fa-dollar"></i> <?php echo number_format((float)$totalIncomeInThisMonth,3,'.','');?> </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-4" class="chart-pie-widget" data-percent="<?php echo (int) $completedJobsInThisMonth[0]*100/$completedJobsInThisMonth[1]; ?>">
                <span class="pie-widget-count-4 pie-widget-count"></span>
            </div>
            <p>
                Completed Jobs
            </p>
            <h5><i class="fa fa-file-excel-o"></i> <?php echo $completedJobsInThisMonth[0];?></h5>

        </div>
    </div>
</div>

<div class="row home-row-top">
    <div class="col-md-6">
        <div class="sale-widget">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs icon-tab icon-tab-home nav-justified">
                <li><a href="#monthly" data-toggle="tab"><i class="fa fa-calendar-o"></i> <span>Monthly</span></a></li>
                <li class="active"><a href="#yearly" data-toggle="tab"><i class="fa fa-dollar"></i> <span>Yearly</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade" id="monthly">
                    <h4>Monthly Income Report</h4>
                    <p>In previous months of <?php echo date("Y");?></p>
                    <div class="monthlySale">
                        <ul>
                            <?php
                            $arr = array("ls-light-blue-progress","progress-bar-info","progress-bar-success","progress-bar-warning","progress-bar-danger","");
                            foreach ($sixMonthSaleReport as $key=> $item) {
                            ?>
                                <li>
                                    <div class="progress vertical bottom">
                                        <div class="progress-bar <?php echo $arr[$key]; ?> six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="<?php echo $item->percentage; ?>"></div>
                                    </div>
                                    <div class="monthName">
                                        <?php echo $item->month; ?>
                                    </div>
                                </li>
                            <?php }?>
                        </ul>

                    </div>
                </div>

                <div class="tab-pane fade in active" id="yearly">
                    <div id="seriesToggleWidget" class="seriesToggleWidget"></div>
                    <ul id="choicesWidget"></ul>
                </div>
                <div class="tab-pane fade" id="product">
                    <div id="flotPieChart" class="flotPieChartWidget"></div>
                </div>

            </div>
            <!-- Tab End -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="v-map-widget">
            <div class="v-map-overlay">
                <ul>
                    <li><span class="user-status is-online"></span> Online</li>
                    <!--<li><span class="user-status is-idle"></span> Idle</li>-->
                    <li><span class="user-status is-busy"></span> Busy</li>
                    <li><span class="user-status is-offline"></span> Offline</li>
                </ul>
            </div>
            <h3 class="ls-header">User Status</h3>
            <div id="world_map" class="world_map_home_widget"></div>
        </div>
    </div>

</div>
<div class="row home-row-top">
    <div class="col-md-12">
        <h3 class="ls-header">Popular Babysitters</h3>
        <!--Table Wrapper Start-->
        <div class="table-responsive ls-table">
            <table class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Projects</th>
                    <th>Likes</th>
                    <th>Dislikes</th>
                    <th>Attitude</th>
                    <th>Punctuality</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($getPopulateBabysitters as $key=>$sitter):?>
                    <tr>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $sitter->name; ?></td>
                        <td><?php echo $sitter->email; ?></td>
                        <td><?php echo $sitter->phone; ?></td>
                        <td><?php echo $sitter->project_done; ?></td>
                        <td class="ls-table-progress">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuetransitiongoal="<?php echo ($sitter->likes/($sitter->likes+$sitter->dislikes))*100; ?>"></div>
                            </div>
                        </td>
                        <td class="ls-table-progress">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuetransitiongoal="<?php echo ($sitter->dislikes/($sitter->likes+$sitter->dislikes))*100; ?>"></div>
                            </div>
                        </td>
                        <td class="ls-table-progress">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuetransitiongoal="<?php echo ($sitter->attitude/$sitter->project_done)*10; ?>"></div>
                            </div>
                        </td>
                        <td class="ls-table-progress">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuetransitiongoal="<?php echo ($sitter->punctuality/$sitter->project_done)*10; ?>"></div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <!--<tr>
                    <td>2</td>
                    <td>PSD</td>
                    <td>Lorem ipsum dolor sit amet</td>
                    <td class="ls-table-progress">
                        <div class="progress progress-striped active">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuetransitiongoal="90"></div>
                        </div>
                    </td>
                    <td><span class="label label-light-green">On Way</span></td>
                    <td>
                        <button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-xs ls-light-green-btn"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-xs ls-red-btn"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>PSD Theme</td>
                    <td>Lorem ipsum dolor sit amet</td>
                    <td class="ls-table-progress">
                        <div class="progress progress-striped active">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuetransitiongoal="80"></div>
                        </div>
                    </td>
                    <td><span class="label label-warning">Pending</span></td>
                    <td>
                        <button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-xs ls-light-green-btn"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-xs ls-red-btn"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Wordpress Theme</td>
                    <td>Lorem ipsum dolor sit amet</td>

                    <td class="ls-table-progress">
                        <div class="progress progress-striped active">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuetransitiongoal="80"></div>
                        </div>
                    </td>
                    <td><span class="label label-red">Error</span></td>
                    <td>
                        <button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-xs ls-light-green-btn"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-xs ls-red-btn"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>HTML Template</td>
                    <td>Lorem ipsum dolor sit amet</td>

                    <td class="ls-table-progress">
                        <div class="progress progress-striped active">
                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuetransitiongoal="70"></div>
                        </div>
                    </td>
                    <td>
                        <span class="label label-light-green">On Way</span>
                    </td>
                    <td>
                        <button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-xs ls-light-green-btn"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-xs ls-red-btn"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Plugin</td>
                    <td>Lorem ipsum dolor sit amet</td>

                    <td class="ls-table-progress">
                        <div class="progress progress-striped active">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuetransitiongoal="90"></div>
                        </div>
                    </td>
                    <td><span class="label label-success">Successfull</span></td>
                    <td>
                        <button class="btn btn-xs btn-info"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-xs ls-light-green-btn"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-xs ls-red-btn"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>-->
                </tbody>

            </table>
        </div>
        <!--Table Wrapper Finish-->
    </div>
</div>



<!-- Main Content Element  End-->
<?php $this->load->view("includes/footer");?>