<?php $this->load->view('includes/header'); ?>
    <div class="row">
        <div class="col-md-12">
            <!--Top header start-->
            <h3 class="ls-top-header">Tasks</h3>
            <!--Top header end -->


            <!--Top breadcrumb start -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li class="active">Task</li>
            </ol>
            <!--Top breadcrumb start -->
        </div>
    </div>


    <!-- Main Content Element  Start-->
    <div class="widget-content nopadding">
        <table class="table table-bordered table-striped table-hover dataTable no-footer" id="tasksLists">
            <thead>
            <tr>
                <th class="text-center">Title</th>
                <th class="text-center">Address</th>
                <th class="text-center">Start Date</th>
                <th class="text-center">End Date</th>
                <th class="text-center">Assigned To</th>
                <th class="text-center">Hourly Cost</th>
                <th class="header">Status</th>
            </tr>
            </thead>
        </table>

    <script>
        $(document).ready(function() {
            $('#tasksLists').dataTable( {
                "processing": true,
                "serverSide": true,
                //.... Enable/Disable Info
                "paging":     true,
                "ordering":   true,
                "info":       true,
                //.... Saving state of the table,if you move to other sites and than return to this page,than the saved state will be loaded....//
                "stateSave": true,
                //..... Alternatives for pagination....//
                "pagingType": "simple_numbers",//numbers,simple,simple_numbers,full,full_numbers
                //..... Language Options....//
                "language": {
                    "lengthMenu": "Display _MENU_ Records Per Page",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing Page _PAGE_ of _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                //.... Page Length Options.....//
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],//first array define length and second for displaying

                "ajax": {
                    "url": "<?php echo base_url().'index.php/tasks/get_tasks_list';?>",
                    "type": "POST"
                },

                "columnDefs" : [
                    {
                        "render": function ( data, type, row ) {
                            if(row[6] == "COMPLETED"){
                                return "<span style='color: green;font-weight: bold;'>"+row[6]+"</span>";
                            }/*else if(row[6] == "PENDING"){
                                "<span style='color:yellow;font-weight: bold;'>"+row[6]+"</span>";
                            }*//*else if(row[6] == "REJECTED"){
                                "<span style='color:darkred;font-weight: bold;'>"+row[6]+"</span>";
                            }*/else{
                                return row[6];
                            }
                        },
                        "targets": 6
                    }
                    /*{
                        "render": function ( data, type, row ) {
                            if(row[7] == 1 && row[8] != "b"){
                                return row[9]+' '+row[10]+' '+row[11];
                            }else{
                                return row[9]+' '+row[10]+' '+row[12];
                            }
                        },
                        "targets": 8
                    }*/

                ]
            } );//.... End of dataTables...

        });//.... End of ready....//

    </script>
    <!-- Main Content Element  End-->
<?php $this->load->view('includes/footer'); ?>