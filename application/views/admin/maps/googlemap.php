<?php $this->load->view("includes/header");?>
    <div class="row">
        <div class="col-md-12">
            <!--Top header start-->
            <!--<h3 class="ls-top-header">Show Task on Google Map</h3>-->
            <!--Top header end -->

            <!--Top breadcrumb start -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Maps</a></li>
                <li class="active">Google Map</li>
            </ol>
            <!--Top breadcrumb start -->
        </div>
    </div>
    <!-- Main Content Element  Start-->


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-dark">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-map-marker"></i> Available Jobs On Google map</h3>
                    <ul class="panel-control">
                        <li><a href="javascript:void(0)" class="minus"><i class="fa fa-minus"></i></a></li>
                        <li><a href="javascript:void(0)" class="refresh"><i class="fa fa-refresh"></i></a></li>
                        <li><a href="javascript:void(0)" class="close-panel"><i class="fa fa-times"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body no-padding">
                    <div class="row">
                        <div class="col-md-12">
                            <!--ls Google Map Feature  Start-->
                            <div class="ls-google-map-wrapper">
                                <div id="googleMap" style="width: 100%;height: 500px;"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Content Element  End-->
    <script type="application/javascript">
        var bounds = new google.maps.LatLngBounds();
         var map;
         var marker;
         var infowindow = [];
         var clients = [];
         var lat = 37.7821587;
         var long = -122.4004835;
         var babysitterID = 1;

        function setMapProperties(lat, long)
        {
            var mapProp = {
                center:new google.maps.LatLng(lat, long),
                zoom:15,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
        }//.... end of function() ....//

         function initialize()
         {
             $.ajax({
                 type:"post",
                 url:"<?php echo base_url()."index.php/maps/get_allJobs";?>",
                 success:function(response){
                     var data = JSON.parse(response);
                     if(data.length==0){
                         setMapProperties(29.8108233, -95.1717434);
                         return false;
                     }
                     $.each(data, function (index, value){
                         if(index == 0){
                             setMapProperties(value.lat, value.long);
                         }//.... end of if() ....//

                         infowindow[value.job_id] = new google.maps.InfoWindow({
                             content: "<strong>Announced By:"+value.title+"</strong>" +
                                 "<p><b>Location:</b>"+value.location+"</p>"+
                             "<p><b>Start Date:</b>"+value.start_date+".  <b>End Date:</b>"+value.end_date+"</p>"
                         });

                         marker = new google.maps.Marker({
                             map: map,
                             position: new google.maps.LatLng(value.lat, value.long),
                             title: value.title,
                             icon: "http://maps.google.com/mapfiles/marker_orange.png"

                         });

                         var label = new Label({ map: map });
                         label.bindTo('position', marker, 'position');
                         label.bindTo('text', marker, 'title');
                         marker.empid = value.job_id;
                         bounds.extend(marker.position);
                         clients[value.job_id] = marker;

                         google.maps.event.addListener(clients[value.job_id], 'click', function() {
                             infowindow[this.empid].open( map,clients[this.empid] );
                         });
                     });//.... end of foreach() ....//
                     map.fitBounds(bounds);
                 }//.... end of success.
             });//... end of ajax() ....//
         }//.... end of initialize() .....//
         google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<?php $this->load->view("includes/footer");?>