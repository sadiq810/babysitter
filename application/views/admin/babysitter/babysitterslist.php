<?php $this->load->view('includes/header'); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Babysitters List</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">List</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
                    <!-- Main Content Element  Start-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Baby Sitters List</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped table-hover dataTable no-footer" id="babysittersList">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Username</th>
                                                <th>Phone</th>
                                                <th>Address</th>
                                                <th>Approval</th>
                                                <th>Actions</th>
                                                &nbsp;
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->
    <script>
        $(document).ready(function() {
            $('#babysittersList').dataTable( {
                "processing": true,
                "serverSide": true,
                //.... Enable/Disable Info
                "paging":     true,
                "ordering":   true,
                "info":       true,
                //.... Saving state of the table,if you move to other sites and than return to this page,than the saved state will be loaded....//
                "stateSave": true,
                //..... Alternatives for pagination....//
                "pagingType": "simple_numbers",//numbers,simple,simple_numbers,full,full_numbers
                //..... Language Options....//
                "language": {
                    "lengthMenu": "Display _MENU_ Records Per Page",
                    "zeroRecords": "Nothing found - sorry",
                    "info": "Showing Page _PAGE_ of _PAGES_",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                //.... Page Length Options.....//
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],//first array define length and second for displaying

                "ajax": {
                    "url": "<?php echo base_url().'index.php/babysitter/get_babysitters';?>",
                    "type": "POST"
                },

                "columnDefs" : [
                    {
                        'sortable'  : false,
                        'searchable': false,
                        'orderable' :false,
                        'targets' : 8
                    },
                    {
                        "render": function ( data, type, row ) {
                            if(row[7] == 1 && row[8] != "b"){
                                return "UnApproved";
                            }else{
                                return "Approved";
                            }
                        },
                        "targets": 7
                    },
                    {
                        "render": function ( data, type, row ) {
                            if(row[7] == 1 && row[8] != "b"){
                                return row[9]+' '+row[10]+' '+row[11];
                            }else{
                                return row[9]+' '+row[10]+' '+row[12];
                            }
                        },
                        "targets": 8
                    }

                ]
            } );//.... End of dataTables...


            $("body").on('click',".btnApprove",function(e){
                e.preventDefault();
                var url = $(this).attr("href");
                $.ajax({
                    url:url,
                    type:'post',
                    success:function(response){
                        $('#babysittersList').DataTable().ajax.reload();
                    }
                });
                e.stopImmediatePropagation();
            });
        });//.... End of ready....//

    </script>
<?php $this->load->view('includes/footer'); ?>