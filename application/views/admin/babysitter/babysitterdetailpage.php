<?php $this->load->view("includes/header");?>
    <div class="row">
        <div class="col-md-12">
            <!--Top header start-->
            <h3 class="ls-top-header">Baby Sitter Detail Page</h3>
            <!--Top header end -->

            <!--Top breadcrumb start -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li><a href="#">Baby Sitter</a></li>
                <li class="active"><?php echo $babysitter[0]->fname." ".$babysitter[0]->lname; ?> Details</li>
            </ol>
            <!--Top breadcrumb start -->
        </div>
    </div>
    <!-- Main Content Element  Start-->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <!--User Details Start-->
            <div class="ls-user-details">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <!--User Name-->
                        <div class="ls-user-name">
                            <h1><?php echo $babysitter[0]->fname." ".$babysitter[0]->lname; ?></h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--User Details start-->
                    <div class="user-detail col-md-4 col-sm-5">
                        <div class="ls-user-position">
                            <h4>Sitter Cost</h4>
                            <p><?php echo $babysitter[0]->sitter_cost; ?>$ / h</p>
                        </div>
                        <!--Address-->
                        <address class="col-sm-11">
                            <i class="fa fa-map-marker"></i>
                            <!--795 Folsom Ave, Suite 600<br>
                            San Francisco, CA 94107<br>-->
                            <?php echo $babysitter[0]->address; ?><br>
                            <abbr title="Phone"><i class="fa fa-mobile"></i> P:</abbr> <?php echo $babysitter[0]->phone; ?>
                        </address>
                        <!--User Image-->
                        <div class="user-pic">
                            <?php if($babysitter[0]->img){?>
                                <img alt="image" src="<?php echo base_url().$babysitter[0]->img; ?>">
                            <?php }else{?>
                                <img alt="image" src="<?php echo base_url(); ?>assets/images/demo/avatar-115.png">
                            <?php }?>
                        </div>
                    </div>
                    <!--User Info Start-->
                    <div class="ls-user-info col-md-8 col-sm-7">
                        <div class="ls-user-text">
                            <p> <span style="float: left; margin-left: 36%">Total Hours Completed: <?php echo (isset($totalHours[0]->tHours))?number_format((float)$totalHours[0]->tHours, 3, '.', ''):0;?> Hrs</span>  Total Earnings: <?php echo (isset($totalEarnings[0]->totalEarnings))?number_format((float)$totalEarnings[0]->totalEarnings, 2, '.', ''):0;?> $.</p>
                            <p>Balance: <?php echo (isset($balance[0]->balance))?number_format((float)$balance[0]->balance, 2, '.', ''):0;?> $</p>
                        </div>
                        <div class="ls-user-text2">
                            <h4><span style="float: left; margin-left: 50%">Punctuality : <?php echo (isset($punctuality))?(int)$punctuality:0;?> </span> Job done: <?php echo $CompletedJobs;?></h4>
                            <p> <span style="float: left; margin-left: 50%">Attitude : <?php echo (isset($attitude))?(int)$attitude:0;?> </span>  <?php echo $babysitter[0]->likes; ?> <i class="fa fa-thumbs-up"></i>  <?php echo $babysitter[0]->dislikes; ?> <i class="fa fa-thumbs-down"></i></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="user-profile-tab">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs  nav-justified icon-tab">
                    <li class="active">
                        <a data-toggle="tab" class="user-google-location" href="#about-me">
                            <i class="fa fa-gears"></i> <span>About Me</span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content tab-border">
                    <div id="about-me" class="tab-pane fade active in">
                        <div class="row">
                            <div class="col-md-7">
                                <h3 class="ls-header"><?php echo $babysitter[0]->fname." ".$babysitter[0]->lname;?></h3>
                                <p>
                                    <span class="about-me-text">
                                        <?php echo $babysitter[0]->about;?>
                                    </span>
                                </p>
                                <p>
                                    <h3> Working Days:    <span style="float: right; margin-right: 27px;">Languages:</span></h3>
                                <span><?php echo str_replace("-",", ", $babysitter[0]->working_days);?></span>
                                <span style="float:right;"><?php echo str_replace("-",", ", $babysitter[0]->languages);?></span>
                                </p>
                                <p>From  <?php echo $babysitter[0]->from_time." -   To  ". $babysitter[0]->to_time; ?></p>
                            </div>
                            <div class="col-md-5">
                                <div class="user-map-locator" id="user-locator" style="position: relative; background-color: rgb(229, 227, 223); overflow: hidden;">
                                    <div id="googleMap" style="width:100%;height:200px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row ls-bottom-gap">
        <div class="col-md-4">
            <!--User Activities Wrapper Start-->
            <div class="ls-user-activities">
                <h3 class="ls-header">Recent Jobs</h3>
                <hr>
                <!--Nano Scroll Wrap Start-->
                <div class="feed-box-profile">
                    <div class="nano has-scrollbar">
                        <div class="nano-content" tabindex="0" style="right: -17px;">
                            <div class="feed-box">
                                <ul class="ls-feed">
                                    <?php foreach($jobs as $j):?>
                                    <!--List Start-->
                                        <li>
                                            <?php echo "<strong>".$j->fname." ",$j->lname."</strong> ( ".$j->pstart_time." - ".date('H:i',strtotime($j->pend_time))." )"; ?>
                                            <span class="date"></span>
                                            <p><i>Total Hours</i> : <?php echo $j->total_hours; ?> , <i>Total Earning</i> : <?php echo $j->total_cost;?> $, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <i>Payment Status</i> : <?php echo ($j->is_paid == 0)?"PENDING":"PAID";?></p>
                                        </li>
                                    <!--List Finish-->
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <div class="nano-pane" style="display: block; opacity: 1; visibility: visible;">
                            <div class="nano-slider" style="height: 67px; transform: translate(0px, 0px);"></div>
                        </div>
                    </div>
                </div>
                <!--Nano Scroll Wrap Finish-->
            </div>
            <!--User Activities Wrapper Finish-->
        </div>
        <div class="col-md-4">
            <!--User Task Wrapper Start-->
            <div class="ls-user-task">
                <h3 class="ls-header">Recent Reviews</h3>
                <hr>
                <!--Task List Start-->
                <ul id="slippylist">
                    <?php foreach($jobs as $k=>$j):
                        if(! $j->reviews) continue;
                        ?>
                        <li>
                            <i class="fa fa-thumbs-up"></i>
                            <span>
                            <?php echo $j->reviews; ?>
                            </span>
                        </li>
                    <?php if($k>6) break;
                    endforeach; ?>
                </ul>
            </div>
            <!--User Task Wrapper Finish-->
        </div>

        <div class="col-md-4">
            <div class="ls-project ls-prettyPhoto">
                <h3 class="ls-header">Recent Clients</h3>
                <hr>
                <ul id="ls-project-lightGallery">
                    <?php foreach($clients as $key=>$c):?>
                        <li data-src="<?php if($c->img){echo base_url().$c->img;}else{ echo base_url()."assets/images/demo/dummy-460x290.jpg";}?>">
                            <div class="ls-view ls-hover-effect">
                                <img src="<?php if($c->img){echo base_url().$c->img;}else{ echo base_url()."assets/images/demo/dummy-120x100.jpg";}?>"
                                     class="img-thumbnail">
                                <!--<div class="ls-mask">
                                    <a href="javascript:void(0);" class="info">
                                        Read More</a>
                                </div>-->
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- Main Content Element  End-->
    <script type="application/javascript">
        var bounds = new google.maps.LatLngBounds();
        var map;
        var marker;
        var infowindow = [];
        var clients = [];
        var img = "<?php echo $babysitter[0]->img; ?>";
        var lat = "<?php echo $babysitter[0]->lat; ?>";
        var long = "<?php echo $babysitter[0]->long; ?>";
        var babysitterID = <?php echo $babysitter[0]->bs_id; ?>;

        function initialize()
        {
            var mapProp = {
                center:new google.maps.LatLng(lat, long),
                zoom:15,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };

            map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
                        infowindow[babysitterID] = new google.maps.InfoWindow({
                            content: "<strong><?php echo $babysitter[0]->fname.' '.$babysitter[0]->lname;?></strong>"
                        });

                    var image = "http://maps.google.com/mapfiles/marker_orange.png";
                    if(img != "")
                        image = {url: "<?php echo base_url()?>"+ img ,scaledSize: new google.maps.Size(32, 32) };
                    marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(lat, long),
                        title: "<?php echo $babysitter[0]->fname.' '.$babysitter[0]->lname; ?>",
                        icon: image

                    });

                    var label = new Label({ map: map });
                    label.bindTo('position', marker, 'position');
                    label.bindTo('text', marker, 'title');
                    marker.empid = babysitterID;
                    bounds.extend(marker.position);
                    clients[babysitterID] = marker;

                    google.maps.event.addListener(clients[babysitterID], 'click', function() {
                        infowindow[this.empid].open( map,clients[this.empid] );
                    });

                map.fitBounds(bounds);
        }//.... end of initialize() .....//
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<?php $this->load->view("includes/footer");?>