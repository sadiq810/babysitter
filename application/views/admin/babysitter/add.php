<?php $this->load->view("includes/header");?>
    <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
   <!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
<?php
$bs_id = "";
$fname = "";
$lname = "";
$email = "";
$username = "";
$phone = "";
$address = "";
$hrate = "";
$card_number = "";
$expire_date = date("m-d-Y");
$aboutme = "";
$from_time ="";
$to_time = "";
$working_days = "";
$pin_code = "";
if(isset($record)){
    $bs_id       = (isset($record[0]->bs_id))?$record[0]->bs_id:"";
    $fname       = (isset($record[0]->fname))?$record[0]->fname:"";
    $lname       = (isset($record[0]->lname))?$record[0]->lname:"";
    $email       = (isset($record[0]->email))?$record[0]->email:"";
    $username    = (isset($record[0]->username))?$record[0]->username:"";
    $phone       = (isset($record[0]->phone))?$record[0]->phone:"";
    $address     = (isset($record[0]->address))?$record[0]->address:"";
    $hrate       = (isset($record[0]->hourly_rate))?$record[0]->hourly_rate:"";
    $card_number = (isset($record[0]->card_number))?$record[0]->card_number:"";
    $aboutme     = (isset($record[0]->about))?$record[0]->about:"";
    $pin_code     = (isset($record[0]->pin_code))?$record[0]->pin_code:"";
    $from_time   = (isset($record[0]->from_time))?date("H:i",strtotime($record[0]->from_time)):"";
    $to_time     = (isset($record[0]->to_time))?date("H:i",strtotime($record[0]->to_time)):"";
    $expire_date = (isset($record[0]->expiration_date))?$record[0]->expiration_date:"";
    $expire_date = date("Y-m-d",strtotime($expire_date));
    $working_days = (isset($record[0]->working_days))?explode("-",$record[0]->working_days):"";
}//.... end of if() ....//
?>
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">create a Baby Sitter</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li><a href="#">Baby Sitter</a></li>
                                <li class="active">create a Baby Sitter</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
                    <!-- Main Content Element  Start-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></h3>
                                </div>
                                <div class="panel-body">
                                    <form action="<?php echo base_url().'index.php/babysitter/save_record'?>" class="form-horizontal ls_form bv-form" method="post" id="defaultForm">
                                        <input type="hidden" name="bs_id" value="<?php echo $bs_id; ?>">
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">First name</label>
                                            <div class="col-lg-6">
                                                <input type="text" value="<?php echo $fname; ?>" placeholder="Enter first name.." name="fname" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Last name</label>
                                            <div class="col-lg-6">
                                                <input type="text" value="<?php echo $lname; ?>" required placeholder="Enter last name.." name="lname" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Email</label>
                                            <div class="col-lg-6">
                                                <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" placeholder="Enter email.." required>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Username</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="username" value="<?php echo $username; ?>" class="form-control" placeholder="Enter username.." required>
                                            </div>
                                        </div>
                                        <?php if(! isset($record)):?>
                                            <div class="form-group has-feedback">
                                                <label class="col-lg-3 control-label">Password</label>
                                                <div class="col-lg-6">
                                                    <input type="password" name="password" class="form-control" placeholder="Enter password.." required>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Phone</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="phone" value="<?php echo $phone; ?>" class="form-control" placeholder="Enter phone number.." required>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Hourly Rate</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="hrate" value="<?php echo $hrate; ?>" class="form-control" placeholder="Enter hourly rate.." required>
                                            </div>
                                            <span class="col-lg-1" style="text-align: center; font-weight: bolder">$ / Hour</span>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Available On</label>

                                            <div class="col-md-1 ls-group-input">
                                                <label class="checkbox">
                                                    <input type="checkbox" name="availableOn[]" value="Sunday" id="Sunday" <?php if(is_array($working_days)){echo (in_array("Sunday", $working_days))?"checked":"";} ?>> Sunday
                                                </label>
                                            </div>
                                            <div class="col-md-1 ls-group-input">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="Monday" name="availableOn[]" id="Monday" <?php if(is_array($working_days)){echo (in_array("Monday", $working_days))?"checked":"";} ?>> Monday
                                                </label>
                                            </div>
                                            <div class="col-md-1 ls-group-input">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="Tuesday" name="availableOn[]" id="Tuesday" <?php if(is_array($working_days)){echo (in_array("Tuesday", $working_days))?"checked":"";} ?>> Tuesday
                                                </label>
                                            </div>
                                            <div class="col-md-1 ls-group-input">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="Wednesday" name="availableOn[]" id="Wednesday" <?php if(is_array($working_days)){echo (in_array("Wednesday", $working_days))?"checked":"";} ?>> Wednesday
                                                </label>
                                            </div>
                                            <div class="col-md-1 ls-group-input">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="Thursday" name="availableOn[]" id="Thursday" <?php if(is_array($working_days)){echo (in_array("Thursday", $working_days))?"checked":"";} ?>> Thursday
                                                </label>
                                            </div>
                                            <div class="col-md-1 ls-group-input">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="Friday" name="availableOn[]" id="Friday" <?php if(is_array($working_days)){echo (in_array("Friday", $working_days))?"checked":"";} ?>> Friday
                                                </label>
                                            </div>
                                            <div class="col-md-1 ls-group-input">
                                                <label class="checkbox">
                                                    <input type="checkbox" value="Saturday" name="availableOn[]" id="Saturday" <?php if(is_array($working_days)){echo (in_array("Saturday", $working_days))?"checked":"";} ?>> Saturday
                                                </label>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 col-sm-3 col-xs-12 control-label">Time of Availability</label>
                                            <div class="col-md-3 ls-group-input">
                                                <label class="checkbox">
                                                    From: <input type="text" name="from" class="form-control timePickerFrom">
                                                </label>
                                            </div>
                                            <div class="col-md-3 ls-group-input">
                                                <label class="checkbox">
                                                    To:
                                                </label>
                                                <input type="text" name="to" class="form-control timePickerTo">
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Address</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="address" value="<?php echo $address; ?>" class="form-control" placeholder="Enter address..." required></div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Credit Card Number</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="card_number" value="<?php echo $card_number; ?>" class="form-control" placeholder="Enter Credit Card Number..." required></div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Expiration Date</label>
                                            <div class="col-md-6">
                                                <input type="text" name="expire_date" class="form-control" id="ExdatePicker">
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">PIN Code</label>
                                            <div class="col-lg-6">
                                                <input type="number" maxlength="4" min="4" name="pin_code" value="<?php echo $pin_code; ?>" class="form-control" placeholder="Enter Credit Card PIN Code..." required></div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">About Me</label>
                                            <div class="col-md-6">
                                                <textarea name="aboutme" id="about" cols="30" rows="10" class="form-control"  placeholder="Enter details about yourself"><?php echo $aboutme; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Profile Image</label>
                                            <div class="col-lg-6">
                                                <input type="file" name="img" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-9 col-lg-offset-3">
                                                <input type="submit" value="Submit" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

<!--------------------- Modal Window Starts here ------------------------------------------------------->
    <div class="modal fade" id="myModalDanger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header label-red white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModaDanger">Error!</h4>
                </div>
                <div class="modal-body">
                    <div id="message"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn ls-red-btn btn-xs" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!------------------ End of Modal Window ------------------------------------>
    <script>
        $(document).ready(function(){
            $("#defaultForm").on("submit",function(e){
                e.preventDefault();
                $(this).ajaxSubmit({
                    success:function(response){
                        if(response == 'exist'){
                            $("#message").html("");
                            $("#message").html("Username is Already Taken, Please Select another one.");
                            $("#myModalDanger").modal("show");
                            return false;
                        }else if(response == 'emailExist'){
                            $("#message").html("");
                            $("#message").html("Email is already exist, please select different one!");
                            $("#myModalDanger").modal("show");
                            return false;
                        }else{
                            window.location.href = '<?php echo base_url()."babysitters";?>';
                        }//.... end of if-else.....//
                        //console.log(response);
                    }//..... end of success() ....//
                });//... end of ajaxSubmit() ....//
            });//.... end of submit() ....//

            $("#ExdatePicker").datetimepicker({
                timepicker:false,
                datepicker:true,
                mask:false,
                value:'<?php echo $expire_date;?>',
                format:"Y-m-d"
            });
            $(".timePickerFrom").datetimepicker({
                datepicker: false,
                value: '<?php echo $from_time;?>',
                format:'H:i'
            });
            $(".timePickerTo").datetimepicker({
                datepicker: false,
                format: 'H:i',
                onShow:function( ct ){
                    this.setOptions({
                        minTime:$('.timePickerFrom').val()?$('.timePickerFrom').val():false
                    })
                },
                value:'<?php echo $to_time;?>'
            });

        });//.... end of ready....//
    </script>
<?php $this->load->view("includes/footer");?>