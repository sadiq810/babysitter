<?php $this->load->view('includes/header'); ?>
<?php
    $user_id = "";
    $fname = "";
    $lname = "";
    $email = "";
    $username = "";
    $phone = "";
    $address = "";
    if(isset($record)){
        $user_id    = (isset($record[0]->user_id))?$record[0]->user_id:"";
        $fname      = (isset($record[0]->fname))?$record[0]->fname:"";
        $lname      = (isset($record[0]->lname))?$record[0]->lname:"";
        $email      = (isset($record[0]->email))?$record[0]->email:"";
        $username   = (isset($record[0]->username))?$record[0]->username:"";
        $phone      = (isset($record[0]->phone))?$record[0]->phone:"";
        $address    = (isset($record[0]->address))?$record[0]->address:"";
    }//.... end of if() ....//
?>
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Create a Admin</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li><a href="#">Admin</a></li>
                                <li class="active">Create a Admin</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
                    <!-- Main Content Element  Start-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"></h3>
                                </div>
                                <div class="panel-body">
                                    <form action="<?php echo base_url().'index.php/admin/save_admin'?>" class="form-horizontal ls_form bv-form" method="post" id="defaultForm">
                                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">First name</label>
                                            <div class="col-lg-6">
                                                <input type="text" value="<?php echo $fname; ?>" placeholder="Enter first name.." name="fname" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Last name</label>
                                            <div class="col-lg-6">
                                                <input type="text" value="<?php echo $lname; ?>" required placeholder="Enter last name.." name="lname" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Email</label>
                                            <div class="col-lg-6">
                                                <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" placeholder="Enter email.." required>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Username</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="username" value="<?php echo $username; ?>" class="form-control" placeholder="Enter username.." required>
                                            </div>
                                        </div>
                                        <?php if(! isset($record)):?>
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Password</label>
                                            <div class="col-lg-6">
                                                <input type="password" name="password" class="form-control" placeholder="Enter password.." required>
                                            </div>
                                        </div>
                                        <?php endif; ?>

                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Phone</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="phone" value="<?php echo $phone; ?>" class="form-control" placeholder="Enter phone number.." required>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Address</label>
                                            <div class="col-lg-6">
                                                <input type="text" name="address" value="<?php echo $address; ?>" class="form-control" placeholder="Enter address..." required></div>
                                        </div>
                                        
                                        <div class="form-group has-feedback">
                                            <label class="col-lg-3 control-label">Profile Image</label>
                                            <div class="col-lg-6">
                                                <input type="file" name="img" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-9 col-lg-offset-3">
                                                <input type="submit" value="Submit" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                        $(document).ready(function(){
                            $("#defaultForm").on("submit",function(e){
                                e.preventDefault();
                                $(this).ajaxSubmit({
                                    success:function(response){
                                        if(response == 'exist'){
                                            alert("Username is already exist please select different one!");
                                            return false;
                                        }else if(response == 1){
                                            window.location = '<?php echo base_url()."list_admin";?>';
                                        }
                                        console.log(response);
                                    }//..... end of success() ....//
                                });//... end of ajaxSubmit() ....//
                            });
                        });//.... end of ready....//
                    </script>
<?php $this->load->view('includes/footer'); ?>