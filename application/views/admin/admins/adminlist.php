<?php $this->load->view('includes/header'); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Admin List</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Admins</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
                    <!-- Main Content Element  Start-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Simple List</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Username</th>
                                                <th>Phone</th>
                                                <th>Address</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($record as $key=> $value): ?>
                                                    <tr>
                                                        <td><?php echo $key+1; ?></td>
                                                        <td><?php echo $value->fname; ?></td>
                                                        <td><?php echo $value->lname; ?></td>
                                                        <td><?php echo $value->email; ?></td>
                                                        <td><?php echo $value->username; ?></td>
                                                        <td><?php echo $value->phone; ?></td>
                                                        <td><?php echo $value->address; ?></td>
                                                        <td class="text-center">
                                                            <!--<button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>-->
                                                            <button class="btn btn-xs btn-warning" onclick="window.location.href = '<?php echo base_url().'admin/edit/'.$value->user_id;?>';"><i class="fa fa-pencil-square-o"></i></button>
                                                            <button class="btn btn-xs btn-danger" onclick="window.location.href = '<?php echo base_url().'admin/edit/'.$value->user_id;?>';"><i class="fa fa-minus"></i></button>
                                                        </td>
                                                    </tr>
                                            <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->
<?php $this->load->view('includes/footer'); ?>