<?php $this->load->view("includes/header");?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins/fullcalendar/fullcalendar.css">

    <div class="row">
        <div class="col-md-12">
            <!--Top header start-->
            <h3 class="ls-top-header">Calender</h3>
            <!--Top header end -->

            <!--Top breadcrumb start -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i></a></li>
                <li class="active">Calender</li>
            </ol>
            <!--Top breadcrumb start -->
        </div>
    </div>
    <!-- Main Content Element  Start-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-calendar"></i> Jobs Calender</h3>
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div style="display:none" id="loading">loading...</div>
                            <div id="full-calendar"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header label-light-green white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabelSuccess">Notification Details</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped table-hover dataTable no-footer">
                        <thead>
                        <tr>
                            <th>Parent Name</th>
                            <th>Babysitter Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Address</th>
                            <th>Job Status</th>
                        </tr>
                        </thead>
                        <tbody id="tableBody"></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn ls-light-green-btn btn-xs" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
                    
<script>
window.onload = full_calendar_trigger_call;
	/****** fullcalendar Start *****/
var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();
var $trash = $("#trash");
var full_calendar;

function full_calendar_trigger_call(){
    'use strict';

    full_calendar = $('#full-calendar').fullCalendar({
        /*height: 650,*/
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        selectable: true,
        selectHelper: true,
        /*select: function (start, end, allDay) {
            bootbox.prompt("Event name?", function (result) {
                if (result === null) {

                } else {

                    full_calendar.fullCalendar('renderEvent',
                        {
                            title: result,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                        true // make the event "stick"
                    );
                }
                full_calendar.fullCalendar('unselect');
            });
        },*/
        editable: true,
        events: [
            <?php if(isset($events) && !empty($events)){
                foreach($events as $event){
            ?>
                 {
                     title: '<?php echo $event->jobStatus; ?>',
                     start: '<?php echo $event->date;?>',
                     url:   "<?php echo $event->job_id;?>"
                 },
            <?php } }?>/*,
            {
                title: 'Long Event',
                start: new Date(y, m, d - 5),
                end: new Date(y, m, d - 2)
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d - 3, 16, 0),
                allDay: false
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d + 4, 16, 0),
                allDay: false
            },
            {
                title: 'Meeting',
                start: new Date(y, m, d, 10, 30),
                allDay: false
            },
            {
                title: 'Lunch',
                start: new Date(y, m, d, 12, 0),
                end: new Date(y, m, d, 14, 0),
                allDay: false
            },
            {
                title: 'Birthday Party',
                start: new Date(y, m, d + 1, 19, 0),
                end: new Date(y, m, d + 1, 22, 30),
                allDay: false
            },
            {
                title: 'Click for Google',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: 'http://google.com/'
            }*/
        ],
        eventClick: function(event) {
            //console.log("clicked:"+event.url);
            show_popup_details(event.url);
            return false;
        },
        droppable: false, // this allows things to be dropped onto the calendar !!!
        /*drop: function (date, allDay) { // this function is called when something is dropped
            //console.log("Dropped on " + date + " with allDay=" + allDay);
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#full-calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        },*/
        //eventDragStart: eventDragStart,
        /*events: "json-events.php",*/
        loading: function (bool) {
            if (bool) $('#loading').show();
            else $('#loading').hide();
        },
    });
    full_calendar_control();
}
function full_calendar_control(){
    'use strict';

   /* $('button.addEvent').click(function () {
        bootbox.prompt("Draggable Event name?", function (result) {
            if (result != null && result != "") {
                var $html = "<div class='external-event'>" + result + "</div>";
                $("div.eventList").append($html);
                dragablaeElementSet();
            }
        });
    });*/
  /*  $trash.droppable({
        drop: function (event, ui) {
            /!*console.log(event);*!/
            /!*console.log($(ui));*!/
            $(ui.draggable).remove();
            $('#trash').removeClass('active');
        }
    });*/
}
function eventDragStart(event, jsEvent, ui, view) {
    'use strict';

    /*console.log(event);
     console.log(jsEvent);
     console.log(ui);
     console.log(view);*/
}
function dragablaeElementSet() {
    'use strict';

    $('#external-events div.external-event').each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
       /* $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0,  //  original position after the drag
            start: startDraging,
            stop: stopDraging
        });*/

        $(this).click(function(){
            console.log("clicked");
        });

    });
}
function startDraging() {
    'use strict';

    $('#trash').addClass('active');
}

function stopDraging() {
    'use strict';

    $('#trash').removeClass('active');
}

/****** fullcalendar End *****/

function show_popup_details(job_id){
    $("#tableBody").html("");
    $.ajax({
        type:"post",
        url:"<?php echo base_url().'index.php/calender/get_job_details';?>/"+job_id,
        success:function(response){
            var parent = response[0].parent;
            var babysitter = response[0].babysitter;
            var status = response[0].jobStatus;
            var sdate = response[0].start_date;
            var edate = response[0].end_date;
            var address = response[0].location;

            $("#tableBody").append('<tr><td>'+parent+'</td><td>'+babysitter+'</td><td>'+sdate+'</td><td>'+edate+'</td><td>'+address+'</td><td>'+status+'</td></tr>');
            $("#myModalSuccess").modal("show");
        }//.... end of success() ....//
    });//.... end of ajax() ....//
}//.... end of show_popup_details() ...//

</script>
<?php $this->load->view("includes/footer");?>