
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Table</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Table</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
                    <!-- Main Content Element  Start-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Simple Table</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Mark</td>
                                                <td>Otto</td>
                                                <td>@mdo</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Mark</td>
                                                <td>Otto</td>
                                                <td>@TwBootstrap</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Jacob</td>
                                                <td>Thornton</td>
                                                <td>@fat</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Larry the Bird</td>
                                                <td>Larry the Bird</td>
                                                <td>@twitter</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>Jacob</td>
                                                <td>Thornton</td>
                                                <td>@fat</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>Larry the Bird</td>
                                                <td>Larry the Bird</td>
                                                <td>@twitter</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Action Table</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product</th>
                                                <th>Info</th>
                                                <th>Tag</th>
                                                <th class="text-center">Progress</th>
                                                <th>Report</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>PSD Design</td>
                                                <td>Lorem ipsum dolor sit amet</td>
                                                <td>
                                                    <span class="label label-default">Wordpress</span>
                                                    <span class="label label-info">Portfolio</span>
                                                    <span class="label label-success">Corporate</span>
                                                </td>
                                                <td class="ls-table-progress">
                                                    <div class="progress progress-striped active">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuetransitiongoal="20" aria-valuenow="20" style="width: 20%;">20%</div>
                                                    </div>
                                                </td>
                                                <td>Pending</td>
                                                <td class="text-center">
                                                    <button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>
                                                    <button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                                                    <button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>PSD</td>
                                                <td>Lorem ipsum dolor sit amet</td>
                                                <td>
                                                    <span class="label label-success">Corporate</span>
                                                    <span class="label label-info">Portfolio</span>
                                                    <span class="label label-warning">Business</span>
                                                    <span class="label label-danger">Admin</span>
                                                </td>
                                                <td class="ls-table-progress">
                                                    <div class="progress progress-striped active">
                                                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuetransitiongoal="30" aria-valuenow="30" style="width: 30%;">30%</div>
                                                    </div>
                                                </td>
                                                <td>Pending</td>
                                                <td class="text-center">
                                                    <button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>
                                                    <button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                                                    <button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>PSD Theme</td>
                                                <td>Lorem ipsum dolor sit amet</td>
                                                <td>
                                                    <span class="label label-info">Portfolio</span>
                                                    <span class="label label-warning">Business</span>
                                                </td>
                                                <td class="ls-table-progress">
                                                    <div class="progress progress-striped active">
                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuetransitiongoal="40" aria-valuenow="40" style="width: 40%;">40%</div>
                                                    </div>
                                                </td>
                                                <td>Pending</td>
                                                <td class="text-center">
                                                    <button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>
                                                    <button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                                                    <button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Wordpress Theme</td>
                                                <td>Lorem ipsum dolor sit amet</td>
                                                <td>
                                                    <span class="label label-warning">Buesiness</span>
                                                    <span class="label label-danger">Admin</span>
                                                </td>
                                                <td class="ls-table-progress">
                                                    <div class="progress progress-striped active">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuetransitiongoal="50" aria-valuenow="50" style="width: 50%;">50%</div>
                                                    </div>
                                                </td>
                                                <td>Pending</td>
                                                <td class="text-center">
                                                    <button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>
                                                    <button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                                                    <button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>HTML Template</td>
                                                <td>Lorem ipsum dolor sit amet</td>
                                                <td>
                                                    <span class="label label-success">Corporate</span>
                                                    <span class="label label-info">portfolio</span>
                                                    <span class="label label-warning">Business</span>
                                                </td>
                                                <td class="ls-table-progress">
                                                    <div class="progress progress-striped active">
                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuetransitiongoal="30" aria-valuenow="30" style="width: 30%;">30%</div>
                                                    </div>
                                                </td>
                                                <td>Pending</td>
                                                <td class="text-center">
                                                    <button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>
                                                    <button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                                                    <button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>Plugin</td>
                                                <td>Lorem ipsum dolor sit amet</td>
                                                <td>
                                                    <span class="label label-default">Wordpress</span>
                                                    <span class="label label-primary">Plugin</span>
                                                    <span class="label label-success">Corporate</span>
                                                    <span class="label label-info">Portfolio</span>
                                                    <span class="label label-warning">Business</span>
                                                    <span class="label label-danger">Admin</span>
                                                </td><td class="ls-table-progress">
                                                <div class="progress progress-striped active">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuetransitiongoal="90" aria-valuenow="90" style="width: 90%;">90%</div>
                                                </div>
                                            </td>
                                                <td>Sucessfull</td>
                                                <td class="text-center">
                                                    <button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>
                                                    <button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                                                    <button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
                                    <div class="text-center">
                                        <div class="ls-button-group demo-btn ls-table-pagination">
                                            <ul class="pagination ls-pagination">
                                                <li><a href="#"><i class="fa fa-arrow-left"></i> </a></li>
                                                <li><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li><a href="#"><i class="fa fa-arrow-right"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Row Drag n Drop Table</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-bottomless" id="ls-row">
                                            <thead>
                                            <tr style="cursor: move;">
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr style="cursor: move;">
                                                <td style="background-color: rgb(239, 241, 241);">1</td>
                                                <td>Mark</td>
                                                <td style="background-color: rgb(239, 241, 241);">Otto</td>
                                                <td>@mdo</td>
                                            </tr>
                                            <tr style="cursor: move;">
                                                <td style="background-color: rgb(239, 241, 241);">2</td>
                                                <td>Mark</td>
                                                <td style="background-color: rgb(239, 241, 241);">Otto</td>
                                                <td>@TwBootstrap</td>
                                            </tr>
                                            <tr style="cursor: move;">
                                                <td style="background-color: rgb(239, 241, 241);">3</td>
                                                <td>Jacob</td>
                                                <td style="background-color: rgb(239, 241, 241);">Thornton</td>
                                                <td>@fat</td>
                                            </tr>
                                            <tr style="cursor: move;">
                                                <td style="background-color: rgb(239, 241, 241);">4</td>
                                                <td>Larry the Bird</td>
                                                <td style="background-color: rgb(239, 241, 241);">Thornton</td>
                                                <td>@twitter</td>
                                            </tr>
                                            <tr style="cursor: move;">
                                                <td style="background-color: rgb(239, 241, 241);">5</td>
                                                <td>Jacob</td>
                                                <td style="background-color: rgb(239, 241, 241);">Thornton</td>
                                                <td>@fat</td>
                                            </tr>
                                            <tr style="cursor: move;">
                                                <td style="background-color: rgb(239, 241, 241);">6</td>
                                                <td>Larry the Bird</td>
                                                <td style="background-color: rgb(239, 241, 241);">Thornton</td>
                                                <td>@twitter</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Fickle Column Drag n Drop Table</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered sar-table table-bottomless" id="ls-column">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td style="background-color: rgb(239, 241, 241);">Mark</td>
                                                <td>Otto</td>
                                                <td style="background-color: rgb(239, 241, 241);">@mdo</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td style="background-color: rgb(239, 241, 241);">Mark</td>
                                                <td>Otto</td>
                                                <td style="background-color: rgb(239, 241, 241);">@TwBootstrap</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td style="background-color: rgb(239, 241, 241);">Jacob</td>
                                                <td>Thornton</td>
                                                <td style="background-color: rgb(239, 241, 241);">@fat</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td style="background-color: rgb(239, 241, 241);">Larry the Bird</td>
                                                <td>Thornton</td>
                                                <td style="background-color: rgb(239, 241, 241);">@twitter</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td style="background-color: rgb(239, 241, 241);">Jacob</td>
                                                <td>Thornton</td>
                                                <td style="background-color: rgb(239, 241, 241);">@fat</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td style="background-color: rgb(239, 241, 241);">Larry the Bird</td>
                                                <td>Thornton</td>
                                                <td style="background-color: rgb(239, 241, 241);">@twitter</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Animated Sortable Table</h3>
                                </div>
                                <div class="panel-body">
                                    <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-striped table-bottomless ls-animated-table" style="position: relative; height: 295px; width: 1087px;">
                                            <thead>
                                            <tr style="width: 1087px; height: 36px; top: 0px; position: absolute;">
                                                <th style="min-width: 123px; cursor: pointer;">#<div class="sortArrow"><div class="sortArrowAscending"></div><div class="sortArrowDescending"></div></div></th>
                                                <th style="min-width: 314px; cursor: pointer;">First Name<div class="sortArrow"><div class="sortArrowAscending"></div><div class="sortArrowDescending"></div></div></th>
                                                <th style="min-width: 311px; cursor: pointer;">Last Name<div class="sortArrow"><div class="sortArrowAscending"></div><div class="sortArrowDescending"></div></div></th>
                                                <th style="min-width: 339px; cursor: pointer;">Username<div class="sortArrow"><div class="sortArrowAscending"></div><div class="sortArrowDescending"></div></div></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr style="width: 1087px; height: 37px; top: 36px; position: absolute;">
                                                <td style="min-width: 123px;">1</td>
                                                <td style="min-width: 314px;">Mark</td>
                                                <td style="min-width: 311px;">Otto</td>
                                                <td style="min-width: 339px;">@mdo</td>
                                            </tr>
                                            <tr style="width: 1087px; height: 37px; top: 73px; position: absolute;">
                                                <td style="min-width: 123px;">2</td>
                                                <td style="min-width: 314px;">Mark</td>
                                                <td style="min-width: 311px;">Otto</td>
                                                <td style="min-width: 339px;">@TwBootstrap</td>
                                            </tr>
                                            <tr style="width: 1087px; height: 37px; top: 110px; position: absolute;">
                                                <td style="min-width: 123px;">3</td>
                                                <td style="min-width: 314px;">Jacob</td>
                                                <td style="min-width: 311px;">Thornton</td>
                                                <td style="min-width: 339px;">@fat</td>
                                            </tr>
                                            <tr style="width: 1087px; height: 37px; top: 147px; position: absolute;">
                                                <td style="min-width: 123px;">4</td>
                                                <td style="min-width: 314px;">Larry the Bird</td>
                                                <td style="min-width: 311px;">@twitter</td>
                                                <td style="min-width: 339px;">@twitter</td>
                                            </tr>
                                            <tr style="width: 1087px; height: 37px; top: 184px; position: absolute;">
                                                <td style="min-width: 123px;">5</td>
                                                <td style="min-width: 314px;">Larry the Bird</td>
                                                <td style="min-width: 311px;">@twitter</td>
                                                <td style="min-width: 339px;">@twitter</td>
                                            </tr>
                                            <tr style="width: 1087px; height: 37px; top: 221px; position: absolute;">
                                                <td style="min-width: 123px;">6</td>
                                                <td style="min-width: 314px;">Larry the Bird</td>
                                                <td style="min-width: 311px;">@twitter</td>
                                                <td style="min-width: 339px;">@twitter</td>
                                            </tr>
                                            <tr style="width: 1087px; height: 37px; top: 258px; position: absolute;">
                                                <td style="min-width: 123px;">7</td>
                                                <td style="min-width: 314px;">Larry the Bird</td>
                                                <td style="min-width: 311px;">@twitter</td>
                                                <td style="min-width: 339px;">@twitter</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Advance &amp; Inline Editable Table</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="ls-editable-table table-responsive ls-table">
                                        <div id="ls-editable-table_wrapper" class="dataTables_wrapper" role="grid"><div class="dataTables_length" id="ls-editable-table_length"><label>Show <select name="ls-editable-table_length" aria-controls="ls-editable-table" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="ls-editable-table_filter" class="dataTables_filter"><label>Search:<input type="search" class="" aria-controls="ls-editable-table"></label></div><table class="table table-bordered table-striped table-bottomless dataTable no-footer" id="ls-editable-table" aria-describedby="ls-editable-table_info">
                                            <thead>
                                                <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="ls-editable-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column ascending" style="width: 204px;">Rendering engine</th><th class="sorting" tabindex="0" aria-controls="ls-editable-table" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 284px;">Browser</th><th class="sorting" tabindex="0" aria-controls="ls-editable-table" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 239px;">Platform(s)</th><th class="sorting" tabindex="0" aria-controls="ls-editable-table" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 128px;">Engine version</th><th class="sorting" tabindex="0" aria-controls="ls-editable-table" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 141px;">CSS grade</th></tr>
                                            </thead>
                                            <tbody>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            <tr class="odd">
                                                <td class="sorting_1"> A Trident(read only cell)</td>
                                                <td class=" ">Internet Explorer 4.0</td>
                                                <td class=" ">Win 95+</td>
                                                <td class=" ">4</td>
                                                <td class=" ">X</td>
                                            </tr><tr class="even">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Firefox 1.5</td>
                                                <td class=" ">Win 98+ / OSX.2+</td>
                                                <td class=" ">1.8</td>
                                                <td class=" ">A</td>
                                            </tr><tr class="odd">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Firefox 2.0</td>
                                                <td class=" ">Win 98+ / OSX.2+</td>
                                                <td class=" ">1.8</td>
                                                <td class=" ">A</td>
                                            </tr><tr class="even">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Firefox 3.0</td>
                                                <td class=" ">Win 2k+ / OSX.3+</td>
                                                <td class=" ">1.9</td>
                                                <td class=" ">A</td>
                                            </tr><tr class="odd">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Camino 1.0</td>
                                                <td class=" ">OSX.2+</td>
                                                <td class=" ">1.8</td>
                                                <td class=" ">A</td>
                                            </tr><tr class="even">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Camino 1.5</td>
                                                <td class=" ">OSX.3+</td>
                                                <td class=" ">1.8</td>
                                                <td class=" ">A</td>
                                            </tr><tr class="odd">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Netscape 7.2</td>
                                                <td class=" ">Win 95+ / Mac OS 8.6-9.2</td>
                                                <td class=" ">1.7</td>
                                                <td class=" ">A</td>
                                            </tr><tr class="even">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Netscape Browser 8</td>
                                                <td class=" ">Win 98SE+</td>
                                                <td class=" ">1.7</td>
                                                <td class=" ">A</td>
                                            </tr><tr class="odd">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Netscape Navigator 9</td>
                                                <td class=" ">Win 98+ / OSX.2+</td>
                                                <td class=" ">1.8</td>
                                                <td class=" ">A</td>
                                            </tr><tr class="even">
                                                <td class="sorting_1">Gecko</td>
                                                <td class=" ">Mozilla 1.0</td>
                                                <td class=" ">Win 95+ / OSX.1+</td>
                                                <td class=" ">1</td>
                                                <td class=" ">A</td>
                                            </tr></tbody>
                                        </table><div class="dataTables_info" id="ls-editable-table_info" role="alert" aria-live="polite" aria-relevant="all">Showing 1 to 10 of 57 entries</div><div class="dataTables_paginate paging_full_numbers" id="ls-editable-table_paginate"><a class="paginate_button first disabled" aria-controls="ls-editable-table" tabindex="0" id="ls-editable-table_first">First</a><a class="paginate_button previous disabled" aria-controls="ls-editable-table" tabindex="0" id="ls-editable-table_previous">Previous</a><span><a class="paginate_button current" aria-controls="ls-editable-table" tabindex="0">1</a><a class="paginate_button " aria-controls="ls-editable-table" tabindex="0">2</a><a class="paginate_button " aria-controls="ls-editable-table" tabindex="0">3</a><a class="paginate_button " aria-controls="ls-editable-table" tabindex="0">4</a><a class="paginate_button " aria-controls="ls-editable-table" tabindex="0">5</a><a class="paginate_button " aria-controls="ls-editable-table" tabindex="0">6</a></span><a class="paginate_button next" aria-controls="ls-editable-table" tabindex="0" id="ls-editable-table_next">Next</a><a class="paginate_button last" aria-controls="ls-editable-table" tabindex="0" id="ls-editable-table_last">Last</a></div></div>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->

