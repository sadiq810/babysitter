<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <!-- iOS webapp icons -->
    <!-- <link rel="apple-touch-icon-precomposed" href="<?php /*echo base_url(); */?>assets/images/ios/fickle-logo-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php /*echo base_url(); */?>assets/images/ios/fickle-logo-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php /*echo base_url(); */?>assets/images/ios/fickle-logo-114.png" />-->

    <!--- TODO: Add a favicon --->
    <!--<link rel="shortcut icon" href="<?php /*echo base_url(); */?>assets/images/ico/fab.ico">-->

    <title>BabySitter - Dashboard</title>

    <!--Page loading plugin Start -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins/pace.css">
    <script src="<?php echo base_url(); ?>assets/js/pace.min.js"></script>
    <!--Page loading plugin End   -->

    <!-- Plugin Css Put Here -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins/jquery-jvectormap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins/jquery.datetimepicker.css">

    <!--AmaranJS Css Start-->
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/jquery.amaran.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/theme/all-themes.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/theme/awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/theme/default.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/theme/blur.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/theme/user.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/theme/rounded.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/theme/readmore.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/amaranjs/theme/metro.css" rel="stylesheet">
    <!--AmaranJS Css End -->

    <!-- Plugin Css End -->
    <!-- Custom styles Style -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <!-- Custom styles Style End-->

    <!-- Responsive Style For-->
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">
    <!-- Responsive Style For-->

    <!-- Custom styles for this template -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/lib/jquery-1.11.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dataTable/jquery.dataTables.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/dataTable/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script src="<?php echo base_url(); ?>assets/js/label.js"></script>
    <script>
        var baseURL = "<?php echo base_url(); ?>";
    </script>
</head>

<body class="">
<!--Navigation Top Bar Start-->
<nav class="navigation">
<div class="container-fluid">
<!--Logo text start-->
<div class="header-logo">
    <a href="<?php echo base_url(); ?>">
        <h1>Babysitter</h1>
    </a>
</div>
<!--Logo text End-->
<div class="top-navigation">
<!--Collapse navigation menu icon start -->
<div class="menu-control hidden-xs">
    <a href="javascript:void(0)">
        <i class="fa fa-bars"></i>
    </a>
</div>
<!--<div class="search-box">
    <ul>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                <span class="fa fa-search"></span>
            </a>
            <div class="dropdown-menu  top-dropDown-1">
                <h4>Search</h4>
                <form>
                    <input type="search" placeholder="what you want to see ?">
                </form>
            </div>

        </li>
    </ul>
</div>-->

<!--Collapse navigation menu icon end -->
<!--Top Navigation Start-->
    <?php
    $ci = &get_instance();
    $notifications = $ci->getNotificationsForHeader();
    $tasks = $ci->getTasksForHeader();
    ?>
<ul>
    <li class="dropdown">
        <!--All task drop down start-->
        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
            <span class="fa fa-tasks"></span>
            <span class="badge badge-lightBlue"><?php echo count($tasks);?></span>
        </a>
        <div class="dropdown-menu right top-dropDown-1">
            <h4>All Task</h4>
            <ul class="goal-item">
                <?php
                $count = 0;
                foreach($tasks as $k=> $task): if($count >=10)break; ?>
                    <li>
                        <a href="javascript:void(0)">
                            <div class="goal-user-image">
                                <img class="rounded" src="<?php echo (isset($task->img) && $task->img)?base_url().'/'.$task->img:base_url().'assets/images/demo/avatar-80.png';?>" alt="user image" />
                            </div>
                            <div class="goal-content">
                                <?php echo (isset($task->msg))?$task->msg:''; ?>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar <?php echo (isset($task->jobStatus) && $task->jobStatus == "REJECTED")?'ls-red-progress':(isset($task->jobStatus) && $task->jobStatus == "COMPLETED")?'ls-light-green-progress':'ls-light-blue-progress'; ?> six-sec-ease-in-out" aria-valuetransitiongoal="100"></div>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php $count++; endforeach; ?>
               <!-- <li>
                    <a href="javascript:void(0)">
                        <div class="goal-user-image">
                            <img class="rounded" src="<?php /*echo base_url();*/?>assets/images/demo/avatar-80.png" alt="user image" />
                        </div>
                        <div class="goal-content">
                            PSD Designe
                            <div class="progress progress-striped active">
                                <div class="progress-bar ls-red-progress six-sec-ease-in-out" aria-valuetransitiongoal="40"></div>
                            </div>
                        </div>
                    </a>
                </li>-->
                <!--<li>
                    <a href="javascript:void(0)">
                        <div class="goal-user-image">
                            <img class="rounded" src="<?php /*echo base_url();*/?>assets/images/demo/avatar-80.png" alt="user image" />
                        </div>
                        <div class="goal-content">
                            Wordpress PLugin
                            <div class="progress progress-striped active">
                                <div class="progress-bar ls-light-green-progress six-sec-ease-in-out" aria-valuetransitiongoal="60"></div>
                            </div>
                        </div>
                    </a>
                </li>-->
                <li class="only-link">
                    <a href="<?php echo base_url().'tasks';?>">View All</a>
                </li>
            </ul>
        </div>
        <!--All task drop down end-->
    </li>
    <li class="dropdown">
        <!--Notification drop down start-->
        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
            <span class="fa fa-bell-o"></span>
            <span class="badge badge-red"><?php echo count($notifications); ?></span>
        </a>

        <div class="dropdown-menu right top-notification">
            <h4>Notification</h4>
            <ul class="ls-feed">
                <?php
                foreach ($notifications as $key => $notification) {
                    if($key >=10) break;
                ?>
                    <li>
                        <a href="javascript:void(0)">
                                        <span class="label label-red">
                                            <i class="fa fa-bell"></i>
                                        </span>
                            <?php echo $notification->title; ?>
                            <span class="date"><?php echo date("Y-m-d",strtotime($notification->created_at)); ?></span>
                        </a>
                    </li>
                <?php } ?>
                <!--<li>
                    <a href="javascript:void(0)">
                                        <span class="label label-light-green">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </span>
                        Finance Report for year 2013
                        <span class="date">30 min</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                                        <span class="label label-lightBlue">
                                            <i class="fa fa-shopping-cart"></i>
                                        </span>
                        New order received with
                        <span class="date">45 min</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                                        <span class="label label-lightBlue">
                                            <i class="fa fa-user"></i>
                                        </span>
                        5 pending membership
                        <span class="date">50 min</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                                        <span class="label label-red">
                                            <i class="fa fa-bell"></i>
                                        </span>
                        Server hardware upgraded
                        <span class="date">1 hr</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                                        <span class="label label-blue">
                                            <i class="fa fa-briefcase"></i>
                                        </span>
                        IPO Report for
                        <span class="lightGreen">2014</span>
                        <span class="date">5 hrs</span>
                    </a>
                </li>-->
                <li class="only-link">
                    <a href="<?php echo base_url()."notifications"?>">View All</a>
                </li>
            </ul>
        </div>
    </li>
    <!--<li class="dropdown">

        <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
            <span class="fa fa-envelope-o"></span>
            <span class="badge badge-red">3</span>
        </a>

        <div class="dropdown-menu right email-notification">
            <h4>Email</h4>
            <ul class="email-top">
                <li>
                    <a href="javascript:void(0)">
                        <div class="email-top-image">
                            <img class="rounded" src="assets/images/demo/avatar-80.png" alt="user image" />
                        </div>
                        <div class="email-top-content">
                            John Doe <div>Sample Mail 1</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <div class="email-top-image">
                            <img class="rounded" src="assets/images/demo/avatar-80.png" alt="user image" />
                        </div>
                        <div class="email-top-content">
                            John Doe <div>Sample Mail 2</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <div class="email-top-image">
                            <img class="rounded" src="assets/images/demo/avatar-80.png" alt="user image" />
                        </div>
                        <div class="email-top-content">
                            John Doe <div> Sample Mail 4</div>
                        </div>
                    </a>
                </li>
                <li class="only-link">
                    <a href="mail.html">View All</a>
                </li>
            </ul>
        </div>

    </li>-->
   <!-- <li class="hidden-xs">
        <a class="right-sidebar" href="javascript:void(0)">
            <i class="fa fa-comment-o"></i>
        </a>
    </li>
    <li class="hidden-xs">
        <a class="right-sidebar-setting" href="javascript:void(0)">
            <i class="fa fa-cogs"></i>
        </a>
    </li>
    <li>
        <a href="lock-screen.html">
            <i class="fa fa-lock"></i>
        </a>
    </li>-->
    <li>
        <a href="<?php echo base_url().'index.php/Login_controller/logout';?>">
            <i class="fa fa-power-off"></i>
        </a>
    </li>

</ul>
<!--Top Navigation End-->
</div>
</div>
</nav>
<!--Navigation Top Bar End-->

<section id="main-container">

<!--Left navigation section start-->
<section id="left-navigation">
<!--Left navigation user details start-->
<!--<div class="user-image">
    <img src="assets/images/demo/avatar-80.png" alt=""/>
    <div class="user-online-status"><span class="user-status is-online  "></span> </div>
</div>-->
<!--<ul class="social-icon">
    <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
    <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
    <li><a href="javascript:void(0)"><i class="fa fa-github"></i></a></li>
    <li><a href="javascript:void(0)"><i class="fa fa-bitbucket"></i></a></li>
</ul>-->
<!--Left navigation user details end-->

<!--Phone Navigation Menu icon start-->
<div class="phone-nav-box visible-xs">
    <a class="phone-logo" href="<?php echo base_url();?>" title="">
        <h1>Babysitter</h1>
    </a>
    <a class="phone-nav-control" href="javascript:void(0)">
        <span class="fa fa-bars"></span>
    </a>
    <div class="clearfix"></div>
</div>
<!--Phone Navigation Menu icon start-->

<!--Left navigation start-->
<ul class="mainNav">
<li <?php if(!$this->uri->segment(1)){echo 'class="active"';}?>>
    <a <?php if(!$this->uri->segment(1)){echo 'class="active"';}?> href="<?php echo base_url(); ?>">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>

<li <?php if($this->uri->segment(1) == 'list_admin' || $this->uri->segment(1) == 'create_admin'){echo 'class="active"';}?>>
    <a href="#">
        <i class="fa fa-anchor"></i> <span>Admin</span> <span class="badge badge-red"></span>
    </a>
    <ul>
        <li><a <?php if($this->uri->segment(1) == 'create_admin'){echo 'class="active"';}?> href="<?php echo base_url(); ?>create_admin">Create a Admin</a></li>
        <li><a <?php if($this->uri->segment(1) == 'list_admin'){echo 'class="active"';}?> href="<?php echo base_url(); ?>list_admin">Admin List</a></li>
    </ul>
</li>

<li <?php if($this->uri->segment(1) == 'babysitters' || $this->uri->segment(1) == 'babysitter'){echo 'class="active"';}?>>
    <a href="#">
        <i class="fa fa-child"></i> <span>Baby Sitter</span> <span class="badge badge-red"></span>
    </a>
    <ul>
        <li><a <?php if($this->uri->segment(2) == 'add'){echo 'class="active"';}?> href="<?php echo base_url(); ?>babysitter/add">Create a Sitter</a></li>
        <li><a <?php if($this->uri->segment(1) == 'babysitters' && !$this->uri->segment(2)){echo 'class="active"';}?> href="<?php echo base_url(); ?>babysitters">Baby Sitters List</a></li>
    </ul>
</li>


<li <?php if($this->uri->segment(1) == 'parent'){echo 'class="active"';}?>>
    <a href="#">
        <i class="fa fa-meh-o"></i> <span>Parent Profile</span> <span class="badge badge-red"></span>
    </a>
    <ul>
        <li><a <?php if($this->uri->segment(2) == 'create'){echo 'class="active"';}?> href="<?php echo base_url(); ?>parent/create">Create Parent</a></li>
        <li><a <?php if($this->uri->segment(2) == 'list'){echo 'class="active"';}?> href="<?php echo base_url(); ?>parent/list">Parents List</a></li>
    </ul>
</li>

<li <?php if($this->uri->segment(1) == 'maps'){echo 'class="active"';}?>>
    <a <?php if($this->uri->segment(1) == 'maps'){echo 'class="active"';}?> href="<?php echo base_url(); ?>maps">
        <i class="fa fa-paw"></i> <span>Google Map</span> <span class="badge badge-red"></span>
    </a>
</li>

<li <?php if($this->uri->segment(1) == 'tasks'){echo 'class="active"';}?>>
    <a <?php if($this->uri->segment(1) == 'tasks'){echo 'class="active"';}?> href="<?php echo base_url(); ?>tasks">
        <i class="fa fa-pencil"></i> <span>Tasks</span> <span class="badge badge-red"></span>
    </a>
</li>

<li <?php if($this->uri->segment(1) == 'calendar'){echo 'class="active"';}?>>
    <a <?php if($this->uri->segment(1) == 'calendar'){echo 'class="active"';}?> href="<?php echo base_url(); ?>calendar">
        <i class="fa fa-calendar"></i> <span>Calendar</span> <span class="badge badge-red"></span>
    </a>
</li>
</ul>
</li>
</ul>
<!--Left navigation end-->
</section>
<!--Left navigation section end-->

<!--Page main section start-->
<section id="min-wrapper">
<div id="main-content">
<div class="container-fluid">