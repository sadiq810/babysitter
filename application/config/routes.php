<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Dashboard';
$route['notifications'] = "dashboard/load_all_notifications";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin'] = 'user/index';
$route['admin/validate_user'] = 'user/validate_user';

$route['calendar'] = 'Calender/index';

$route['create_admin'] = 'Admin/load_admin_form';
$route['list_admin'] = 'Admin/list_admins';
$route['admin/edit'] = 'Admin/edit/$1';
$route['admin/delete'] = 'Admin/delete/$1';


$route['babysitter/add']             = 'Babysitter/load_form';
$route['babysitter/edit']            = 'Babysitter/edit/$1';
$route['babysitters/delete']         = 'Babysitter/delete/$1';
$route['babysitters']                = 'Babysitter/list_record';
$route['babysitters/approve/(:num)'] ='Babysitter/approve_babbysitter/$1';
$route['babysitters/view/(:num)']    ='Babysitter/view_details/$1';

$route['parent/create'] = 'ParentController/load_parent_form/$1';
$route['parent/list'] = 'ParentController/list_parent';
$route['parent/edit/(:num)'] ='ParentController/load_parent_form/$1';
$route['parent/delete/(:num)'] ='ParentController/delete/$1';
$route['parent/approve/(:num)'] ='ParentController/approve_parent/$1';
$route['parent/view/(:num)'] ='ParentController/parent_details/$1';

$route['maps'] = 'Maps/google_map';
$route['tasks'] = 'Tasks/tasks';
$route['api'] = "ApiController";






