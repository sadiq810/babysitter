<?php
/**
 * Created by PhpStorm.
 * User: Spider
 * Date: 10/20/2014
 * Time: 11:44 PM
 */
class Login_model extends MY_Model{

    /**
     * constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
    }//--- End of __construct() ---//

    /**
     * function for validating user login credentials
     */
    public function validate_credentials($user,$pass,$table = '')
    {
        $where = array(
            'username' =>$user,
            'password' => $pass,
            'status' => 1
        );
        $data = $this->get_record($table,'*',$where,'','','1');

        return $data;
    }//--- End of function validate_credentials() ---//
}