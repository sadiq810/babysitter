<?php
/**
 * Created by PhpStorm.
 * User: Spider
 * Date: 10/21/14
 * Time: 9:57 PM
 */
class User_Model extends MY_Model{

    public function __construct(){
        parent::__construct();
    }

    /**
     * @param $table
     * @param string $where
     * @param string $field
     * @param string $value
     * @return mixed
     * function for searching usre in the database....
     */
    public function search_user($table,$where = '',$field ='',$value ='')
    {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where($where);
        $this->db->like($field,$value,'both');
        $data = $this->db->get();
        return $data->result();
    }//--- End of function search_user() ---//

    /**
     * @param $table
     * @param $field
     * @param $value
     * function for counting records by value...
     */
    public function countRecByValue($table, $field, $value)
    {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where(array('IsDeleted'=>0,'IsActive'=>1));
        $this->db->like($field, $value, 'both');
        $data = $this->db->get();
        return $data->num_rows();
    }//--- End of function countRecByValue ---//

    /**
     * function for checking the user name if already exist....
     */
    public function checkExist_user($table, $field ,$where ='')
    {
        $this->db->select($field);
        $this->db->where($where);
        $record = $this->db->get($table);
        return $record->result();
    }

}//---- End of class