<?php
/**
 * Created by PhpStorm.
 * User: Spider
 * Date: 10/20/2014
 * Time: 11:27 PM
 */
class MY_Model extends CI_Model{

    /**
     * constructor of the class.
     */
    public function __construct()
    {
        parent::__construct();
    }//--- End of constructor ---//

    /**
     * function for inserting records to database
     */
    public function insert_record($table,$column)
    {
        //$this->db->trans_begin();

        $this->db->insert($table,$column);
        if($this->db->affected_rows()){//$this->db->trans_status() === FALSE){
           // $this->db->trans_rollback();
            return TRUE;
        }
        else{
            //$this->db->trans_commit();
            return FALSE;
        }

    }//--- End of function insert_record() ---//

    /**
     * function for updating records in database
     */
    public function update_record($table , $columns , $where)
    {
        $this->db->trans_begin();
        $this->db->where($where);
        $this->db->update($table,$columns);
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else
        {
         $this->db->trans_commit();
            return TRUE;
        }
    }//--- End of function update_record ---//

    /**
     * function for reading record form database
     */
    public function get_record($table ,$column ,$where='' ,$orderby='' ,$groupby='', $limit='', $skip ='', $count ='',$field='',$value='')
    {
        $this->db->select($column);
        $this->db->from($table);
        if($where !='')
            $this->db->where($where);
        if($orderby != '')
            $this->db->order_by($orderby);
        if($groupby != '')
            $this->db->group_by($groupby);
        if($skip != '' && $limit != '')
            $this->db->limit($limit,$skip);
        elseif($limit != '') {
            $this->db->limit($limit);
        }
        if($field !== ''){
            $this->db->like($field,$value);
        }

        $listData = $this->db->get();
        if($count){
            return $listData->num_rows();
        }
        else{
            return $listData->result();
        }

    }//--- End of function get_record() ---//

    /**
     * function for getting filter record form the database
     *///($table, $columns, $where, $orderby, $groupby , $take, $skip , $logic, $field, $operator, $value);
    public function get_filter_record($table, $column, $where ='', $orderby ='', $groupby ='', $take ='', $skip ='', $logic ='', $field ='', $value ='')
    {
        $this->db->select($column);
        $this->db->from($table);

        if($where != ''){
            $this->db->where($where);
        }

        if($orderby != '')
        {
            $this->db->order_by($orderby);
        }

        if($groupby != ''){
            $this->db->group_by($groupby);
        }

        $this->db->limit($take, $skip);
        $this->db->like($field,$value);
        $detail = $this->db->get();
        return $detail->result();
    }//--- End of function get_filter_record() ---//

    /***
     * function for deleting record from database
     */
    public function delete_record($table,$column ,$where,$hDelete ='')
    {
        $this->db->trans_begin();
        $this->db->where($where);
        if($hDelete != ''){
                $this->db->delete($table,$where);
        }else{
            $this->db->update($table,$column);
        }
        if($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else{
            $this->db->trans_commit();
            return TRUE;
        }
    }//--- End of function delete_record() ---//

    public function get_join_record($table, $columns,$joins = '', $where='',$take='',$skip='',$count='',$orderby = '')
    {
        $this->db->select($columns);
        $this->db->from($table);

        if($orderby != '')
            $this->db->order_by($orderby);
       /* if($groupby != '')
            $this->db->group_by($groupby);
        if($skip != '' && $take != '')
            $this->db->limit($take,$skip);
        else*/
        if($take != '')
            $this->db->limit($take,$skip);

        if($joins !== ''){
            foreach($joins as $k => $v){
                $this->db->join($v['table'],$v['condition'],$v['jointype']);
            }
        }
        if($where !='')
            $this->db->where($where);

        $listData = $this->db->get();

        if($count){
            return $listData->num_rows();
        }
        else{
            return $listData->result();
        }
    }//--- End of get_join_record() ---//

    /***
     * function for getting filtered join record from database
     */
    public function get_filter_join_record($table, $columns,$join='' ,$where ='', $take='', $skip='', $field='',  $value='',$count = '')
    {
        $this->db->select($columns);
        $this->db->from($table);

        if($join !== ''){
            foreach($join as $k=>$v){
               $this->db->join($v['table'],$v['condition'],$v['jointype']);
            }
        }

        if($where !== '')
            $this->db->where($where);
        if($take !=='')
            $this->db->limit($take,$skip);

        $this->db->like($field,$value);
        $data = $this->db->get();
        if($count != '')
        {
            return $data->num_rows();
        }
        else{
            return $data->result();
        }


    }//--- End of get_filter_join_record() ---//

    /**
     * @param $table
     * @param string $where
     * @param string $field
     * @param string $value
     * @return mixed
     * function for searching customer in the database....
     */
    public function search_record($table,$where = '',$field ='',$value ='')
    {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where($where);
        $this->db->like($field,$value,'both');
        $data = $this->db->get();
        return $data->result();
    }//--- End of function search_mobileModel() ---//


    /**
     * @param $table
     * @param $field
     * @param $value
     * function for counting records by value...
     */
    public function countRecByValue($table, $field, $value)
    {
        $this->db->select($field);
        $this->db->from($table);
        $this->db->where(array('IsDeleted'=>0,'IsActive'=>1));
        $this->db->like($field, $value, 'both');
        $data = $this->db->get();
        return $data->num_rows();
    }//--- End of function countRecByValue ---//


}//---End of Class ---//