<?php
/**
 * Created by PhpStorm.
 * User: Spider
 * Date: 10/20/2014
 * Time: 10:01 PM
 */
class MY_Controller extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('common_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('Datatables');

        $method = $this->router->fetch_method();
        $cls    = $this->router->fetch_class();
        $cus_url = $cls."/".$method;
        $arr = array('login_controller/validate_user','login_controller/login','login_controller/index','login_controller/email_forgot_password');
        if(!(in_array($cus_url,$arr) || $this->session->userdata('is_logged_in') === TRUE)){
            redirect('login_controller/index');
        }

    }//--- end of constructor() ---//

    function get_userID(){
        return $this->session->userdata('user_id');
    }

    public function explode_date($date){
        $date = explode("GMT",$date);
        return date("Y-m-d",strtotime($date[0]));
    }//---  end of function explode_date  ---//
    
    /**
     * Function for getting notifications for header of the page......
     */
    public function getNotificationsForHeader()
    {
        $this->db->order_by("notification_id","DESC");
        $notifications = $this->common_model->get_record('notifications',array("notification_id","title","description","nsource","from","created_at"), array("nfor"=> "admin",'is_read'=> 0,"status"=> 1));
        return $notifications;
    }//.... end of getNotificationsForHeader() ....//

    /**
     * Function for getting list of tasks for header...
     */
    public function getTasksForHeader()
    {
        $columns = array("jobs.job_id","pstart_time","pend_time","img","clock_in","clock_out","clock_out","is_finished","is_accepted","CONCAT(babysitter.fname,' ',babysitter.lname) as babysitter");

        $joins = array(
            (0)=> array(
                'table'=> "babysitter",
                'condition'=> 'jobs.babysitter_id = babysitter.bs_id',
                'jointype'=>  'inner'
            )
        );
        $this->db->order_by("job_id","DESC");
        $jobs = $this->common_model->get_join_record("jobs",$columns,$joins,array());

        foreach($jobs as $key=>$job){
            if($job->is_finished == 1){
                if(date("Y-m-d") != date("Y-m-d",strtotime($job->clock_out))){
                    unset($jobs[$key]);
                    continue;
                }
                $job->jobStatus = "COMPLETED";
                $job->msg = "Job Completed by ".$job->babysitter;
                $job->start_date = $job->clock_in;
                $job->end_date = $job->clock_out;
            }elseif($job->is_accepted == 0){
                if(date("Y-m-d") != date("Y-m-d",strtotime($job->pstart_time)) || date("Y-m-d",strtotime($job->pend_time)) < date("Y-m-d") ){
                    unset($jobs[$key]);
                    continue;
                }
                $job->jobStatus = "PENDING";
                $job->msg = "Job is Pending to ".$job->babysitter;
                $job->start_date = $job->pstart_time;
                $job->end_date = $job->pend_time;
            }elseif($job->is_accepted == 1){
                if(date("Y-m-d") != date("Y-m-d",strtotime($job->pstart_time)) || date("Y-m-d",strtotime($job->pend_time)) < date("Y-m-d") ){
                    unset($jobs[$key]);
                    continue;
                }
                $job->jobStatus = "ACCEPTED";
                $job->msg = "Job Accepted by ".$job->babysitter;
                $job->start_date = $job->pstart_time;
                $job->end_date = $job->pend_time;
            }else{
                if(date("Y-m-d") != date("Y-m-d",strtotime($job->pstart_time)) || date("Y-m-d",strtotime($job->pend_time)) < date("Y-m-d") ){
                    unset($jobs[$key]);
                    continue;
                }
                $job->jobStatus = "REJECTED";
                $job->msg = "Job Rejected by ".$job->babysitter;
                $job->start_date = $job->pstart_time;
                $job->end_date = $job->pend_time;
            }//..... end of if-else() .....//

        }//.... end of foreach() ....//

        return $jobs;
    }//.... end of getTasksForHeader() ....//

    /**
     * Function for getting Latitude and Longtitude of the address...
     */
    public function get_lat_long($address)
    {
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key=AIzaSyCj-CYOUDIH7bWsgEHRuWI2vbwn4R9jhkM&sensor=false');
        $geo = json_decode($geo, true);
        $latitude = (isset($geo['results'][0]['geometry']['location']['lat']))?$geo['results'][0]['geometry']['location']['lat']:null;
        $longitude = (isset($geo['results'][0]['geometry']['location']['lng']))?$geo['results'][0]['geometry']['location']['lng']:null;
        return array($latitude,$longitude);
    }//.... End of function() ....//

    /**
     * Function for creating thumbs of the images....
     */
    public function create_thumbs($path, $thumbsPath)
    {
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $path,
            'new_image' => $thumbsPath,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 200,
            'height' => 200
        );
        $this->load->library('image_lib', $config_manip);
        if($this->image_lib->resize()){
            $this->image_lib->clear();
            return true;
        }else{
            return false;
        }//.... end of if-else() ....//
    }//.... end of create_thumbs() ....//

}//--- end of class